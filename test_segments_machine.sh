#!/bin/bash
# Started by Robert Bruntz, 2020.02.04.
# This is a script to remotely test a number of functions on a segments* machine, especially after some updates have been made to the machine.
# The script will produce results that must be interpreted by the user at first, but then gradually automated over time.
# Tasks to be performed by the script:
	#   * Read in command-line variables and set internal variables
#   * Print out what is going to be done and ask for confirmation or give opportunity to cancel
#   * Run ping tests on the machine - internal network and external network [should take < 5 sec for 2 network connections] (note that failed pings are caught and a summary line is printed, but queries will still be run either way)
#   * Query the science-mode flags for all IFOs, using the internal network and the external network, over a fixed range of GPS times (so results are known) [should take < 2 min for 5 IFOs x 2 network connections]
#   * Query alphabetically-sorted first and last flags for each IFO (?) (internal network only)
#   * Query the science-mode flags for all IFOs for a recent time range and report the end of the last 'known' segment (internal network only)
#   * Publish a new flag to the DB (internal network only)
#   * Query the flag that was just published to the DB (internal network only)
# Tasks to be performed on the local machine (i.e., not from here):
#   * (fill this in later)
# To do:
#   * [done] Check the latest known science mode segment queries, to see if there are any actual results, before grepping and printing results
#   * [done] Check that the segment files exist before trying to publish; if not, don't count it as a passed or failed test (or even as a run test)
#   * [done] Check that publishing was successful before querying to check publishing results
#   * [done] Automate testing of output from queries, to see if they match expected values
#   * [done] Add sleep time as a command-line arg
#   * [done] Add the option to replace any variable with "-" to keep the default, whatever that might be?
#   * Turn the 'latest time' queries (latest_time_bool) into a function, rather than having 5 (?) separate queries
#   * Include Ryan's 'ligolw cats' query test (at least as an option)
#   * Write 'known' and 'active' segment files from this script, then publish from the written files? (so not dependent on external files; those files are only ~15 lines total)
#   *


# default values
server=segments-backup   # name will be prepended to ".ldas.cit" (internal network) or ".ligo.org" (external network) at appropriate points, possibly with "http://" or "https://" (respectively) prepended to that
path_prefix=""   # allows the test suite to be run from the Python binaries (scripts) in a different dir (useful for testing scripts in a different dir, maybe on a different machine)
#path_prefix="/home/robert.bruntz/git_work/dqsegdb_2021.11.30/dqsegdb/bin/"
  # Note: to use path_prefix, you probably have to do the following:
  #   * export PATH=[path to the dqsegdb/bin dir]:[everything PATH was set to before]   # this part might not even be necessary; one test showed it wasn't
  #   * export PYTHONPATH=[path to the dqsegdb dir]:[everything PYTHONPATH was set to before - probably nothing]   # note that it must point to the repo dir, so that Python scripts can find module dqsegdb, etc. (error is "ModuleNotFoundError: No module named 'dqsegdb' ")
  #   * ligo-proxy-init albert.einstein   # might only be needed for certain tests; you'll know if you get "RuntimeError: Could not find a RFC 3820 compliant proxy credential. Please run 'grid-proxy-init -rfc' and try again."
#
segments_web_bool=1   # choose whether to print the message to check segments-web, to make sure the web page is accessible
binary_bool=1   # choose whether to run a test of the dqsegdb binaries (scripts)
ping_bool=1   # choose whether to ping the machine (on both the internal and external networks)
science_flag_bool=1   # choose whether to query the science flag for each IFO, on both the internal and external networks
latest_time_bool=1   # choose whether to get the end time for the latest 'known' science-mode segment for each IFO (internal network only)
publish_test_bool=1   # choose whether to publish a unique, new flag to the DB (with the name based on the current GPS time) (using 'insert'), then verify that the publishing worked; note: not allowed (by this script) on segments actual (.ligo.org or .ldas.cit)
other_tests_bool=1   # run a set of misc tests
dq_query_test_bool=1   # run ligolw_dq_query_dqsegdb, which will probably take ~12 min.
publish_threaded_test_bool=1   # run a publish test using ligolw_publish_threaded_dqxml_dqsegdb, using a throwaway set of publisher script, state file, and dqxml files, then verify (from the log file) that publishing worked; note: not allowed (by this script) on segments actual (.ligo.org or .ldas.cit)
#
stop_if_bin_fail=0   # binaries (scripts) are checked for run-ability; if failed, could stop running tests there; might want to continue, since even tests that fail print the commands to run them, which might be useful
science_mode_test_start=1264550000   # this is the time that the 'latest known science-mode time check' will start at; use a time that is known to be before the newest known segment, but preferrably recent, to reduce server workload
sleep_time=20   # delay between printing publishing command that will execute and actually executing it, so user can stop it if they need to
#
run_test_count=0
passed_test_count=0
failed_test_count=0
skipped_test_count=0
exit_code=0


# Read in command-line variables and set internal variables
if [ $# -eq 0 ]; then
  echo "### Usage: '    (script name)              (server)     (segments-web)  (test binaries)  (ping server)  (science-mode flag check)  (latest known science-mode time check)  (publish test flag)  (sleep time for publish; default = 20)  (other tests)  (dq query test)  (publish threaded test)"
  echo "### No args are required; defaults will be used.  Use a dash (-) as a placeholder, to use default, but fill in a later arg."
  echo "### Example: ' ./test_segments_machine.sh  segments-dev  0              0                1              0                          1                                       1 '"
  echo "                                           (to use segments-dev for a ping-test, get latest known science mode times, and publish and verify a test flag)"
  echo "### Example: ' ./test_segments_machine.sh  -             -                  -              -                          -                                       1                    3 '"
  echo "                                           (to use segments-backup with defaults for all tests (= on) and 3 second delay before running the publish test)"
  echo "### Example: ' /home/detchar/dqsegdb/test_segments_machine.sh segments-backup 1 1 1 1 1 1 5 1 1 1   # (5 sec delay)' "
  exit 1
fi
# note: the " && [ $1 != "-" ]" (etc.) parts allow users to enter a dash as a placeholder for a command-line argument, to use the default, but still fill in a later one with a non-default value
#   e.g., ' ./test_segments_machine.sh  - 0 1 1 1 1 5 ' = use segments-backup (default), set other values
if [ $# -gt 0 ] && [ $1 != "-" ]; then server=$1; fi
if [ $# -gt 1 ] && [ $2 != "-" ]; then segments_web_bool=$2; fi
if [ $# -gt 2 ] && [ $3 != "-" ]; then binary_bool=$3; fi
if [ $# -gt 3 ] && [ $4 != "-" ]; then ping_bool=$4; fi
if [ $# -gt 4 ] && [ $5 != "-" ]; then science_flag_bool=$5; fi
if [ $# -gt 5 ] && [ $6 != "-" ]; then latest_time_bool=$6; fi
if [ $# -gt 6 ] && [ $7 != "-" ]; then publish_test_bool=$7; fi
if [ $# -gt 7 ] && [ $8 != "-" ]; then sleep_time=$8; fi
if [ $# -gt 8 ] && [ $9 != "-" ]; then other_tests_bool=$9; fi
if [ $# -gt 9 ] && [ $10 != "-" ]; then dq_query_test_bool=${10}; fi
if [ $# -gt 10 ] && [ $11 != "-" ]; then publish_threaded_test_bool=${11}; fi

if [ "$path_prefix" != "" ] && [ "$PYTHONPATH" == "" ]
then
  echo "### WARNING ### path_prefix is set (to '$path_prefix' ), but PYTHONPATH is empty.  This is probably not what you want."
  echo "            ### You probably want to set PYTHONPATH with something like ' export PYTHONPATH=$path_prefix:\$PYTHONPATH '."
  echo "            ### Waiting for 10 seconds, to give you time to exit this script to fix it.  (Ctrl-C works here.)"
  sleep 10
fi

# print out what is going to be done and ask for confirmation or give opportunity to cancel
start_time=$(date)
gps_start_time=$((`date +%s` - 315964782))
echo "###"
echo "### INFO ### Run time: $(date)  - GPS time: ~$gps_start_time"
#if [ "$server" != "segments" ] && [ "$server" != "segments-backup" ] && [ "$server" != "segments-dev" ] && [ "$server" != "segments-dev2" ] && [ "$server" != "segments-dev3" ]   # note that -dev3 is for testing errors; it isn't (currently) a real machine
#then
#  echo "### ERROR ### This 'server' value ($server) is not valid.  The only valid options are: segments, segments-backup, segments-dev, and segments-dev2.  If there is a new valid server, please modify this script."
#  exit 2
#fi
if [ "$server" == "segments" ]; then if [ "$publish_test_bool" -eq 1 ]; then echo "### NOTICE ### Publishing to server 'segments' is not allowed.  'publish_test_bool' is being set to 0."; publish_test_bool=0; fi; fi
if [ "$server" == "segments" ]; then if [ "$publish_threaded_test_bool" -eq 1 ]; then echo "### NOTICE ### Publishing to server 'segments' is not allowed.  'publish_threaded_test_bool' is being set to 0."; publish_threaded_test_bool=0; fi; fi
echo "### INFO ### server = $server; segments-web test = $segments_web_bool; binary tests = $binary_bool; ping server = $ping_bool; science-mode flag check = $science_flag_bool; latest known science-mode time check = $latest_time_bool;\
 publish test flag = $publish_test_bool; sleep before publish = $sleep_time; other tests = $other_tests_bool; dq_query test = $dq_query_test_bool; threaded publish test = $publish_threaded_test_bool"
if [ "$path_prefix" != "" ]; then echo "### INFO ### path_prefix = $path_prefix"; fi


function cancel_all_tests {
  # this is a function to cancel all tests (which probably means all remaining tests, since some may have already run when it's called); defined here so that it's easy to update if new tests are added; probably only called in the binary test block
  segments_web_bool=0;  binary_bool=0;         ping_bool=0;
  science_flag_bool=0;  latest_time_bool=0;    publish_test_bool=0;
  other_tests_bool=0;   dq_query_test_bool=0;  publish_threaded_test_bool=0
}


# check segments-web, to make sure that it's running
if [ "$segments_web_bool" -eq 1 ]; then
  echo "###"
  echo "### NOTICE ### Check  segments-web.ligo.org  to make sure that it loads properly"
  echo "### WARNING ### This is not a robust check yet."
  # ping test
    echo "### Ping test of segments-web.ligo.org:  'ping -c 1 segments-web.ligo.org &> /dev/null' "
    ping -c 1 segments-web.ligo.org &> /dev/null
    exit_code=$?
    let "run_test_count++"
    if [ "$exit_code" -eq 0 ]
    then
      echo "Ping test passed"; let "passed_test_count+=1"
    else
      echo "### ERROR ### Ping test failed"; let "failed_test_count+=1"
    fi
  # web site test
    echo "### Web site test: this test is not yet implemented."
fi


# check Python scripts (binaries), to make sure they even run, by running "--help" for each command
if [ "$binary_bool" -eq 1 ]; then
  echo "###"
  echo "### Testing ligolw commands "
  ligolw_cmd_passed_count=0
  ligolw_cmd_failed_count=0
  function test_ligolw_cmd () {
    # run the command with --help option and capture output and exit code; if exit code is fail, command failed;
    #   if exit code is OK, check output; if it doesn't have "--help" in it, command failed; if it does, command passed
    echo "### Running: '${path_prefix}$ligolw_cmd --help &> /tmp/help_${ligolw_cmd}.txt"
    ${path_prefix}$ligolw_cmd --help &> /tmp/help_${ligolw_cmd}.txt
    exit_code=$?
    let "run_test_count++"
    if [ "$exit_code" -ne 0 ]; then
      echo "### ERROR ### Cannot run command  $ligolw_cmd .  Last line of /tmp/help_${ligolw_cmd}.txt:     `tail -1 /tmp/help_${ligolw_cmd}.txt`"; let "failed_test_count+=1"; let "ligolw_cmd_failed_count+=1"
    else
      cmd_check_count=`grep "\--help" /tmp/help_${ligolw_cmd}.txt | wc --lines`
      if [ "$cmd_check_count" -gt 0 ]; then
        echo "Command  $ligolw_cmd  passed.  Output in /tmp/help_${ligolw_cmd}.txt ."; let "passed_test_count+=1"; let "ligolw_cmd_passed_count+=1"
        if [ `cat /tmp/help_${ligolw_cmd}.txt | wc --lines` -ne "$help_line_count" ]; then echo "### WARNING ### Help output for $ligolw_cmd was `cat /tmp/help_${ligolw_cmd}.txt | wc --lines` lines but was expected to be $help_line_count lines.  This should be investigated."; fi
      else
        echo "Command  $ligolw_cmd  failed - no instance of '--help' in help output.  More info in /tmp/help_${ligolw_cmd}.txt ."; let "failed_test_count+=1"; let "ligolw_cmd_failed_count+=1"
      fi
    fi
  }
  # run the tests
  ligolw_cmd=ligolw_dq_query_dqsegdb;               help_line_count=25;  test_ligolw_cmd
  ligolw_cmd=ligolw_publish_threaded_dqxml_dqsegdb; help_line_count=38;  test_ligolw_cmd
  ligolw_cmd=ligolw_segment_insert_dqsegdb;         help_line_count=59;  test_ligolw_cmd
  ligolw_cmd=ligolw_segment_query_dqsegdb;          help_line_count=78;  test_ligolw_cmd
  ligolw_cmd=ligolw_segments_from_cats_dqsegdb;     help_line_count=47;  test_ligolw_cmd
  # check if the user wants to stop if the binary tests fail; if so, check if all 5 tests failed; if so, zero out bools for all other tests, which should skip execution to the summary section; note: need to add bools to zero out here as new tests are added
  echo "Passed  $ligolw_cmd_passed_count  tests out of 5 binary tests;  $ligolw_cmd_failed_count  tests were failed."
  if [ "$stop_if_bin_fail" -eq 1 ]; then
    if [ $((ligolw_cmd_passed_count + ligolw_cmd_failed_count)) -ne 5 ]; then echo "### WARNING ### ligolw command passed count (${ligolw_cmd_passed_count}) and failed count (${ligolw_cmd_failed_count}) should sum to 5, but the don't.  You should investigate this."; fi
    if [ "$ligolw_cmd_failed_count" -eq 5 ]; then cancel_all_tests; echo "### NOTICE ### All binary tests failed, so all remaining tests have been cancelled."; fi
    if [ "$ligolw_cmd_failed_count" -gt 0 ] && [ "$ligolw_cmd_failed_count" -lt 5 ]; then echo "### NOTICE ### $ligolw_cmd_failed_count binary tests failed, out of 5.  Other tests will continue, but there might be trouble."; fi
  fi
  #echo $ligolw_cmd_passed_count, $ligolw_cmd_failed_count
fi


# run ping tests on the machine - internal network and external network
# successful ping returns error code = 0; unsuccessful = 1
if [ "$ping_bool" -eq 1 ]; then
  echo "###"
  echo "### Internal network ping test:  '${path_prefix}ligolw_segment_query_dqsegdb --ping --segment-url http://${server}.ldas.cit'  (should report Server version number):"
  output_file=/tmp/ldas_ping_${gps_start_time}.txt
  let "run_test_count++"
  ${path_prefix}ligolw_segment_query_dqsegdb --ping --segment-url http://${server}.ldas.cit  &>  $output_file
  #if [ `cat $output_file | wc --lines` -eq 1 ]   # fails if there are any kind of syntax warnings
  if [ `grep -i version $output_file | wc --lines` -eq 1 ]
    then cat $output_file; let "passed_test_count+=1"
    else echo "### ERROR ### ligolw ping of $server returned an error.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file; let "failed_test_count+=1"
  fi
  #echo $?
  echo "### External network ping test:  '${path_prefix}ligolw_segment_query_dqsegdb --ping --segment-url https://${server}.ligo.org'  (should report Server version number):"
  output_file=/tmp/ligo_ping_${gps_start_time}.txt
  let "run_test_count++"
  ${path_prefix}ligolw_segment_query_dqsegdb --ping --segment-url https://${server}.ligo.org  &>  $output_file
  #if [ `cat $output_file | wc --lines` -eq 1 ]	  # fails if there are any kind	of syntax warnings
  if [ `grep -i version $output_file | wc --lines` -eq 1 ]
    then tail -1 $output_file; let "passed_test_count+=1"
    else echo "### ERROR ### ligolw ping of $server returned an error.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file; let "failed_test_count+=1"
  fi
  #echo $?
fi


# define the query_flag function
#   variables that are defined in the main script and used in the fn: run_test_count, passed_test_count, failed_test_count
#   variables that are defined specifically for each call of the fn: segment_url, ifo1, flag, gps_start, gps_end, output_file, expected_known_count, expected_active_count, expected_known_md5sum, expected_active_md5sum
#   variables that are defined in the function: actual_known_count, actual_active_count, actual_known_md5sum, actual_active_md5sum (defined as local variables, so they don't carry over from one call to another, which could cause false-positive results from bugs)
function query_flag () {
  echo "             ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
  let "run_test_count++"
  /usr/bin/time -f %E -o /tmp/time.txt --  ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
  echo "Query run time:  `cat /tmp/time.txt`"
  if [ `cat $output_file | wc --lines` -gt 70 ]; then
    local actual_known_count=`cat $output_file  |  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id | wc --lines`
    local actual_active_count=`cat $output_file  |  grep segment:segment_id: | wc --lines`
    local actual_known_md5sum=`cat $output_file   |  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id  |  md5sum  |  awk '{print $1}'`   # this output of md5sum will print '[hash] -', so awk will only save the hash
    local actual_active_md5sum=`cat $output_file  |  grep segment:segment_id:  |  md5sum  |  awk '{print $1}'`   # this output of md5sum will print '[hash] -', so awk will only save the hash
    echo "Counts of (known, active) segments should be ($expected_known_count, $expected_active_count) lines (in that order); returned values are ($actual_known_count, $actual_active_count)"
    if [ "$actual_known_count" == "$expected_known_count" ] && [ "$actual_active_count" == "$expected_active_count" ]
    then
      if [ "$actual_known_md5sum" == "$expected_known_md5sum" ] && [ "$actual_active_md5sum" == "$expected_active_md5sum" ]
      then
        echo "MATCH: Known and active segments counts and contents MATCH expectations."
        let "passed_test_count++"
      else
        echo "DIFFERENCE: Known and active segments contents DO NOT MATCH expectations."
        echo "Counts of known and active segments matched expectations, but md5sum values were expected to be ($actual_known_md5sum and $actual_active_md5sum) but returned values were ($expected_known_md5sum and $expected_active_md5sum)."
        echo "(Maybe try running output through '|  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id  |  md5sum' for known and '|  grep segment:segment_id:  |  md5sum' for active segments.)"
        let "failed_test_count++"
      fi
    else
      echo "DIFFERENCE: Known and active segments counts DO NOT MATCH expectations ($expected_known_count, $expected_active_count) vs ($actual_known_count, $actual_active_count).  (Maybe examine contents of $output_file )"
      let "failed_test_count++"
    fi
  else
    echo "### ERROR ### Query of $server failed.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file
    let "failed_test_count++"
  fi
}   # function query_flag


# query the science-mode flags for all IFOs, using the internal network and the external network, over a fixed range of GPS times (so results are known)
# find 'known' lines: 'grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id'
# find 'active' lines: 'grep segment:segment_id:'

if [ "$science_flag_bool" -eq 1 ]; then
  echo "###"
  echo "### Running a fast science mode flag query for each IFO over internal and external network, over a pre-determined time span, to get response time.  User must interpret results.  (< 0.4 sec is good; ~< 0.5 sec is normal.)"
  # changed the query times to match the times for the science-mode flag, for speed comparison
  #segment_url=http://${server}.ldas.cit; ifo1=H1; flag=DMT-ANALYSIS_READY_STATIC_6AUG2019; gps_start=1070000000; gps_end=1250000000; output_file=/tmp/h1_ldas_fast_query_${gps_start_time}.txt
  segment_url=http://${server}.ldas.cit; ifo1=H1; flag=DMT-ANALYSIS_READY_STATIC_6AUG2019; gps_start=1100000000; gps_end=1200000000; output_file=/tmp/h1_ldas_fast_query_${gps_start_time}.txt
  echo "          ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
  /usr/bin/time -f %E -o /tmp/time.txt --  ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
  echo "Query run time:  `cat /tmp/time.txt`"
  #segment_url=http://${server}.ligo.org; ifo1=H1; flag=DMT-ANALYSIS_READY_STATIC_6AUG2019; gps_start=1070000000; gps_end=1250000000; output_file=/tmp/h1_ligo_fast_query_${gps_start_time}.txt
  segment_url=https://${server}.ligo.org; ifo1=H1; flag=DMT-ANALYSIS_READY_STATIC_6AUG2019; gps_start=1100000000; gps_end=1200000000; output_file=/tmp/h1_ligo_fast_query_${gps_start_time}.txt
  echo "          ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
  /usr/bin/time -f %E -o /tmp/time.txt --  ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
  echo "Query run time:  `cat /tmp/time.txt`"
  echo "###"
  echo "### Querying science mode flag for each IFO over internal and external network, over a pre-determined time span, then comparing results to expected results"
# H1:
  expected_known_count=670
  expected_active_count=1270
  expected_known_md5sum=f29e3703062040bcb1021af427081815
  expected_active_md5sum=6d10b83eac731031d928de40ad05e89c
# H1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=H1; flag=DMT-ANALYSIS_READY; gps_start=1100000000; gps_end=1200000000
  output_file=/tmp/h1_ldas_query_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# H1, external network:
  segment_url=https://${server}.ligo.org; ifo1=H1; flag=DMT-ANALYSIS_READY; gps_start=1100000000; gps_end=1200000000
  output_file=/tmp/h1_ligo_query_${gps_start_time}.txt
  echo "### INFO ### External network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# clear varaiables that are used in the query flag fn, so that subsequent calls can't re-use them (and thus get inappropriate, possibly confusing, possibly deceptive, results)
  segment_url=x1, ifo1=x2, flag=x3, gps_start=-1, gps_end=-2, output_file=/tmp/shouldnt_exist.txt, expected_known_count=-1, expected_active_count=-1, expected_known_md5sum=-1, expected_active_md5sum=-1
# following lines preserved for (temporary) reference
#  echo "             ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
#  echo "### INFO ### Should be  670  known segment lines and  1270  active segment lines (in that order)"
#  let "run_test_count++"
#  /usr/bin/time -f %E -o /tmp/time.txt --  ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
#  cat /tmp/time.txt
#  if [ `cat $output_file | wc --lines` -gt 70 ]; then
#    cat $output_file  |  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id | wc --lines
#    cat $output_file  |  grep segment:segment_id: | wc --lines
#  else
#    echo "### ERROR ### Query of $server failed.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file
#  fi

# L1:
  expected_known_count=357
  expected_active_count=1675
  expected_known_md5sum=0ad964a6f5e67869ef1c51f60226dbd6
  expected_active_md5sum=aadc8621f0856ef5cd7814eec31026f2
# L1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=L1; flag=DMT-ANALYSIS_READY; gps_start=1100000000; gps_end=1200000000
  output_file=/tmp/l1_ldas_query_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# L1, external network:
  segment_url=https://${server}.ligo.org; ifo1=L1; flag=DMT-ANALYSIS_READY; gps_start=1100000000; gps_end=1200000000
  output_file=/tmp/l1_ligo_query_${gps_start_time}.txt
  echo "### INFO ### External network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# clear varaiables that are used in the query flag fn, so that subsequent calls can't re-use them (and thus get inappropriate, possibly confusing, possibly deceptive, results)
  segment_url=x1, ifo1=x2, flag=x3, gps_start=-1, gps_end=-2, output_file=/tmp/shouldnt_exist.txt, expected_known_count=-1, expected_active_count=-1, expected_known_md5sum=-1, expected_active_md5sum=-1

# V1:
  expected_known_count=4
  expected_active_count=170
  expected_known_md5sum=28912fd5782c233c2ff13bb407e360d3
  expected_active_md5sum=5f07fea91bbac210d843a5aa2dacaedd
# V1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=V1; flag=ITF_SCIENCE; gps_start=1100000000; gps_end=1200000000
  output_file=/tmp/v1_ldas_query_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# V1, external network:
  segment_url=https://${server}.ligo.org; ifo1=V1; flag=ITF_SCIENCE; gps_start=1100000000; gps_end=1200000000
  output_file=/tmp/v1_ligo_query_${gps_start_time}.txt
  echo "### INFO ### External network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# clear varaiables that are used in the query flag fn, so that subsequent calls can't re-use them (and thus get inappropriate, possibly confusing, possibly deceptive, results)
  segment_url=x1, ifo1=x2, flag=x3, gps_start=-1, gps_end=-2, output_file=/tmp/shouldnt_exist.txt, expected_known_count=-1, expected_active_count=-1, expected_known_md5sum=-1, expected_active_md5sum=-1

# K1:
  expected_known_count=2
  expected_active_count=690
  expected_known_md5sum=8b79abaf83f273eb778b7e4275958177
  expected_active_md5sum=a83df9846516ed3fb7402776a032361d
# K1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=K1; flag=GRD-LSC_LOCK_STATE_N_EQ_1000; gps_start=1200000000; gps_end=1264700000
  output_file=/tmp/k1_ldas_query_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# K1, external network:
  segment_url=https://${server}.ligo.org; ifo1=K1; flag=GRD-LSC_LOCK_STATE_N_EQ_1000; gps_start=1200000000; gps_end=1264700000
  output_file=/tmp/k1_ligo_query_${gps_start_time}.txt
  echo "### INFO ### External network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# clear varaiables that are used in the query flag fn, so that subsequent calls can't re-use them (and thus get inappropriate, possibly confusing, possibly deceptive, results)
  segment_url=x1, ifo1=x2, flag=x3, gps_start=-1, gps_end=-2, output_file=/tmp/shouldnt_exist.txt, expected_known_count=-1, expected_active_count=-1, expected_known_md5sum=-1, expected_active_md5sum=-1

# G1:
  expected_known_count=93
  expected_active_count=9258
  expected_known_md5sum=b135c249493d7fffc6e029168318b593
  expected_active_md5sum=8088aabb8325b35ff752954de20b2996
# G1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=G1; flag=GEO-UP; gps_start=1100000000; gps_end=1200000000
  output_file=/tmp/g1_ldas_query_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# G1, external network:
  segment_url=https://${server}.ligo.org; ifo1=G1; flag=GEO-UP; gps_start=1100000000; gps_end=1200000000
  output_file=/tmp/g1_ligo_query_${gps_start_time}.txt
  echo "### INFO ### External network query for $ifo1 science mode flag (unfiltered):"
  query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# clear varaiables that are used in the query flag fn, so that subsequent calls can't re-use them (and thus get inappropriate, possibly confusing, possibly deceptive, results)
  segment_url=x1, ifo1=x2, flag=x3, gps_start=-1, gps_end=-2, output_file=/tmp/shouldnt_exist.txt, expected_known_count=-1, expected_active_count=-1, expected_known_md5sum=-1, expected_active_md5sum=-1
fi


# query alphabetically-sorted first and last flags for each IFO (?) (internal network only)


# query the science-mode flags for all IFOs for a recent time range and report the end of the last 'known' segment (internal network only)
if [ "$latest_time_bool" -eq 1 ]; then
  echo "###"
  echo "### Running query of science mode flags for all IFOs, to get end of most recent 'known' segment"
  which ltconvert &> /dev/null; ltconvert_bool=$?
# H1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=H1; flag=DMT-ANALYSIS_READY; gps_start=$science_mode_test_start; gps_end=2000000000
  output_file=/tmp/h1_ldas_science_start_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  echo "             ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
  let "run_test_count++"
  /usr/bin/time -f %E -o /tmp/time.txt --  ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
  echo "Query run time:  `cat /tmp/time.txt`"
  if [ `cat $output_file | wc --lines` -gt 70 ]; then
    last_time=`cat $output_file  |  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id  |  tail -n 1  |  cut -c7-16`
    if [ `echo $last_time | wc --chars` -gt 1 ]; then
      let "passed_test_count++"
      echo "### Latest $ifo1 science mode flag ($flag) known time:"
    if [ "$ltconvert_bool" -eq 0 ]; then ltconvert $last_time; else echo $last_time; fi
    else
      let "failed_test_count++"
      echo "### MISSING:  Query of $server for $ifo1 science mode flag ($flag) returned results, but 0 'known' segments.  Output saved in $output_file."
    fi
  else
    let "failed_test_count++"
    echo "### ERROR ### Query of $server failed.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file
  fi
# L1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=L1; flag=DMT-ANALYSIS_READY; gps_start=$science_mode_test_start; gps_end=2000000000
  output_file=/tmp/l1_ldas_science_start_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  echo "             ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
  let "run_test_count++"
  /usr/bin/time -f %E -o /tmp/time.txt --  ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
  echo "Query run time:  `cat /tmp/time.txt`"
  if [ `cat $output_file | wc --lines` -gt 70 ]; then
    last_time=`cat $output_file  |  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id  |  tail -n 1  |  cut -c7-16`
    if [ `echo $last_time | wc --chars` -gt 1 ]; then
      let "passed_test_count++"
      echo "### Latest $ifo1 science mode flag ($flag) known time:"
    if [ "$ltconvert_bool" -eq 0 ]; then ltconvert $last_time; else echo $last_time; fi
    else
      let "failed_test_count++"
      echo "### MISSING:  Query of $server for $ifo1 science mode flag ($flag) returned results, but 0 'known' segments.  Output saved in $output_file."
    fi
  else
    let "failed_test_count++"
    echo "### ERROR ### Query of $server failed.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file
  fi
# V1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=V1; flag=ITF_SCIENCE; gps_start=$science_mode_test_start; gps_end=2000000000
  output_file=/tmp/v1_ldas_science_start_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  echo "             ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
  let "run_test_count++"
  /usr/bin/time -f %E -o /tmp/time.txt --  ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
  echo "Query run time:  `cat /tmp/time.txt`"
  if [ `cat $output_file | wc --lines` -gt 70 ]; then
    last_time=`cat $output_file  |  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id  |  tail -n 1  |  cut -c7-16`
    if [ `echo $last_time | wc --chars` -gt 1 ]; then
      let "passed_test_count++"
      echo "### Latest $ifo1 science mode flag ($flag) known time:"
    if [ "$ltconvert_bool" -eq 0 ]; then ltconvert $last_time; else echo $last_time; fi
    else
      let "failed_test_count++"
      echo "### MISSING:  Query of $server for $ifo1 science mode flag ($flag) returned results, but 0 'known' segments.  Output saved in $output_file."
    fi
  else
    let "failed_test_count++"
    echo "### ERROR ### Query of $server failed.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file
  fi
# K1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=K1; flag=GRD-LSC_LOCK_STATE_N_EQ_1000; gps_start=$science_mode_test_start; gps_end=2000000000
  output_file=/tmp/k1_ldas_science_start_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  echo "             ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
  let "run_test_count++"
  /usr/bin/time -f %E -o /tmp/time.txt --  ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
  echo "Query run time:  `cat /tmp/time.txt`"
  if [ `cat $output_file | wc --lines` -gt 70 ]; then
    last_time=`cat $output_file  |  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id  |  tail -n 1  |  cut -c7-16`
    if [ `echo $last_time | wc --chars` -gt 1 ]; then
      let "passed_test_count++"
      echo "### Latest $ifo1 science mode flag ($flag) known time:"
    if [ "$ltconvert_bool" -eq 0 ]; then ltconvert $last_time; else echo $last_time; fi
    else
      let "failed_test_count++"
      echo "### MISSING:  Query of $server for $ifo1 science mode flag ($flag) returned results, but 0 'known' segments.  Output saved in $output_file."
    fi
  else
    let "failed_test_count++"
    echo "### ERROR ### Query of $server failed.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file
  fi
# G1, internal network:
  segment_url=http://${server}.ldas.cit; ifo1=G1; flag=GEO-UP; gps_start=$science_mode_test_start; gps_end=2000000000
  output_file=/tmp/g1_ldas_science_start_${gps_start_time}.txt
  echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
  echo "             ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file"
  let "run_test_count++"
  /usr/bin/time -f %E -o /tmp/time.txt --  ${path_prefix}ligolw_segment_query_dqsegdb  --segment-url $segment_url  --query-segments  --include-segments ${ifo1}:${flag}:1  --gps-start-time $gps_start  --gps-end-time $gps_end  &>  $output_file
  echo "Query run time:  `cat /tmp/time.txt`"
  if [ `cat $output_file | wc --lines` -gt 70 ]; then
    last_time=`cat $output_file  |  grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id  |  tail -n 1  |  cut -c7-16`
    if [ `echo $last_time | wc --chars` -gt 1 ]; then
      let "passed_test_count++"
      echo "### Latest $ifo1 science mode flag ($flag) known time:"
    if [ "$ltconvert_bool" -eq 0 ]; then ltconvert $last_time; else echo $last_time; fi
    else
      let "failed_test_count++"
      echo "### MISSING:  Query of $server for $ifo1 science mode flag ($flag) returned results, but 0 'known' segments.  Output saved in $output_file."
    fi
  else
    let "failed_test_count++"
    echo "### ERROR ### Query of $server failed.  Final line of the output (saved in $output_file):"; tail -n 1 $output_file
  fi
fi


# publish a new flag to the DB (internal network only)
###here
if [ "$publish_test_bool" -eq 1 ]; then
  echo "###"
  echo "### Running publishing test"
  summary_file=/home/detchar/dqsegdb/known_segments_test_file.txt
  segment_file=/home/detchar/dqsegdb/active_segments_test_file.txt
  action=insert
  #action=append
  publish_server=http://${server}.ldas.cit
  ifo1=G1
  publish_flag=TEST_FLAG_GPS_TIME_${gps_start_time}
  comment="This is a test flag published by test_segments_machine.sh on detchar-cit in a run started at GPS time ${gps_start_time}."
  if [ "$server" == "segments" ]
  then
    echo "### ERROR ### Writing a test flag to server 'segments' is not allowed.  Exiting this if-then block."
    let "skipped_test_count+=2"
  else
    if [ -e $summary_file ] && [ -e $segment_file ]
    then
      echo "### WARNING ### The following publish command will be executed in $sleep_time seconds:"
      echo "            ### ${path_prefix}ligolw_segment_insert_dqsegdb  --$action  --segment-url=$publish_server  --ifos=$ifo1  --name=$publish_flag  --version=1  -S $summary_file  -G $segment_file  --explain=\"$comment\"    --comment=\"$comment\""
      sleep $sleep_time
      let "run_test_count++"
      ${path_prefix}ligolw_segment_insert_dqsegdb  --$action  --segment-url=$publish_server  --ifos=$ifo1  --name=$publish_flag  --version=1  -S $summary_file  -G $segment_file  --explain="$comment"    --comment="$comment"
      error_code=$?
      echo "Error code =  $error_code"
      if [ "$error_code" -eq 0 ]
      then
        let "passed_test_count++"
        echo "PASSED: Publishing worked."
# query the flag that was just published to the DB (internal network only)
# query 11602-11604
# G1, internal network:
        expected_known_count=15
        expected_active_count=10
        expected_known_md5sum=4d84615d441360b46a38080fe53fbd80
        expected_active_md5sum=125843367c61e16cfee1b0c5e3e1dd2a
        segment_url=${publish_server}; flag=$publish_flag; gps_start=1160200000; gps_end=1160400000
        output_file=/tmp/test_flag_query_${gps_start_time}.txt
        echo "### INFO ### Internal network query for $ifo1 science mode flag (unfiltered):"
        query_flag   # function call, using the above defined variables; queries the flag, gets info on the output, and reports how that output compares to expected values
# clear varaiables that are used in the query flag fn, so that subsequent calls can't re-use them (and thus get inappropriate, possibly confusing, possibly deceptive, results)
        segment_url=x1, ifo1=x2, flag=x3, gps_start=-1, gps_end=-2, output_file=/tmp/shouldnt_exist.txt, expected_known_count=-1, expected_active_count=-1, expected_known_md5sum=-1, expected_active_md5sum=-1
      else
        let "failed_test_count++"
        echo "FAILED: Publishing did not work."
        echo "### INFO ### Skipping query of published segments (because publishing failed)."
        let "skipped_test_count++"
      fi
    else
      echo "### ERROR ### Could not find one or both: known segments file ( $summary_file), active segments file ( $segment_file )"
      echo "          ### Skipping publishing test and query of published segments."
      let "skipped_test_count+=2"
    fi
  fi
fi


# other misc tests
if [ "$other_tests_bool" -eq 1 ]; then
  echo "###"
  echo "### Other misc tests"
  # other ligolw_segment_query_dqsegdb test
    echo "###"
    echo "### Other ligolw_segment_query_dqsegdb test (checks DB and ligolw_print)"
    output_file=/tmp/test_other_seg_query_${gps_start_time}.txt
    echo "### Test to run: 'IFO=L ; ${path_prefix}ligolw_segment_query_dqsegdb -t https://${server}.ligo.org -q -a \${IFO}1:DMT-ANALYSIS_READY:1 -s 1250294418 -e 1250726418 | ligolw_print -t segment -c start_time -c end_time -d \" \" &> $output_file' "
    IFO=L ; ${path_prefix}ligolw_segment_query_dqsegdb -t https://${server}.ligo.org -q -a ${IFO}1:DMT-ANALYSIS_READY:1 -s 1250294418 -e 1250726418 | ligolw_print -t segment -c start_time -c end_time -d " " &> $output_file
    let "run_test_count++"
    if [ `cat "$output_file" | wc --lines` -eq 13 ] && [ `cat "$output_file" | wc --words` -eq 26 ]; then
      echo "### Test passed (13 lines, 26 words in $output_file , as expected)."; let "passed_test_count++"
    else
      echo "### ERROR ### Test failed.  Output file  $output_file  should have 13 lines containing 26 'words', but instead has `cat $output_file | wc --lines` lines containing `cat $output_file | wc --words` words.  This should be investigated."
      echo "          ### Last line of  $output_file :     `tail -1 $output_file` "; let "failed_test_count++"
    fi
  # short cats test (ligolw_segments_from_cats_dqsegdb)
    echo "###"
    echo "### Short cats test (ligolw_segments_from_cats_dqsegdb)"
    cats_gps_start_time=931035615
    cats_gps_end_time=940000000
    # veto file should be where indicated; if not, download it and check it
    veto_file=/home/detchar/dqsegdb/H1L1V1-S6_CBC_HIGHMASS_D_OFFLINE-961545543-0.xml
    if [ ! -e "$veto_file" ]; then
      echo "### NOTICE ### Didn't find file $veto_file ; attempting to download it using 'wget https://raw.githubusercontent.com/ligovirgo/dqsegdb/master/test/H1L1V1-S6_CBC_HIGHMASS_D_OFFLINE-961545543-0.xml  -P /tmp/' "
      veto_file=/tmp/H1L1V1-S6_CBC_HIGHMASS_D_OFFLINE-961545543-0.xml
      wget https://raw.githubusercontent.com/ligovirgo/dqsegdb/master/test/H1L1V1-S6_CBC_HIGHMASS_D_OFFLINE-961545543-0.xml  -P /tmp/
      if [ -e "$veto_file" ]; then echo "### File downloaded"; else echo "### File could not be downloaded"; fi
    fi
    if [ -e "$veto_file" ] && [ `md5sum $veto_file | awk '{print $1}'` == "c4dbad271753f3fa6d5a4e1cf2d065fe" ]; then
      output_dir=/tmp/cats_short_test_output_${gps_start_time}
      mkdir $output_dir
      if [ ! -w "$output_dir" ]; then
        let "skipped_test_count++"
        echo "### ERROR ### Could not create output dir for test ($output_dir).  Ending test."
      else
        echo "### Running short cats test: '${path_prefix}ligolw_segments_from_cats_dqsegdb --separate-categories --individual-results --segment-url https://${server}.ligo.org --veto-file /home/detchar/dqsegdb/H1L1V1-S6_CBC_HIGHMASS_D_OFFLINE-961545543-0.xml -s $cats_gps_start_time -e $cats_gps_end_time -o $output_dir'"
        ${path_prefix}ligolw_segments_from_cats_dqsegdb --separate-categories --individual-results --segment-url https://${server}.ligo.org --veto-file /home/detchar/dqsegdb/H1L1V1-S6_CBC_HIGHMASS_D_OFFLINE-961545543-0.xml -s $cats_gps_start_time -e $cats_gps_end_time -o $output_dir
        let "run_test_count++"
        short_cats_file_count=`ls -1 $output_dir | wc --lines`
        if [ "$short_cats_file_count" -eq 15 ]; then
          echo "### Spot test of output:"
          echo "H1-VETOTIME_CAT1-931035615-8964385.xml should be 7367 lines; is `cat $output_dir/H1-VETOTIME_CAT1-931035615-8964385.xml | wc --lines` lines."
          echo "L1-VETOTIME_CAT3-931035615-8964385.xml should be 5768 lines; is `cat $output_dir/L1-VETOTIME_CAT3-931035615-8964385.xml | wc --lines` lines."
          echo "V1-VETOTIME_CAT5-931035615-8964385.xml should be 5688 lines; is `cat $output_dir/V1-VETOTIME_CAT5-931035615-8964385.xml | wc --lines` lines."
          if [ `cat $output_dir/H1-VETOTIME_CAT1-931035615-8964385.xml | wc --lines` -eq 7367 ] && [ `cat $output_dir/L1-VETOTIME_CAT3-931035615-8964385.xml | wc --lines` -eq 5768 ] && [ `cat $output_dir/V1-VETOTIME_CAT5-931035615-8964385.xml | wc --lines` -eq 5688 ]; then
            let "passed_test_count++"
            echo "Test passed: Short cats test output dir ($output_dir) has $short_cats_file_count files, as expected.  You might want to check those files, to make sure they're good, though.)"
            echo "First file in that dir:"
            ls -l1 $output_dir/* | head -n 1
          else
            let "failed_test_count++"
            echo "### ERROR ### Test failed: Some spot-checked files in output dir ($output_dir) were a different size from expectations.  This should be investigated."
          fi
        else
          let "failed_test_count++"
          echo "### ERROR ### Test failed: Short cats test output dir ($output_dir) has $short_cats_file_count files, but 15 files were expected.  This should be investigated."
        fi
      fi
    else
      let "failed_test_count++"
      if [ ! -e "$veto_file" ]; then
        echo "### ERROR ### Cannot run test without veto file."
      else
        echo "### ERROR ### Veto file does not match expected file.  md5sum is `md5sum $veto_file | awk '{print $1}'` , but expected value is c4dbad271753f3fa6d5a4e1cf2d065fe.  Test ended."
      fi
    fi
fi


# dq_query test
if [ "$dq_query_test_bool" -eq 1 ]; then
  echo "###"
  echo "### dq_query test"
  dq_query_gps_time=1200000000
  dq_query_wc=1737
  output_file=/tmp/dq_query_${gps_start_time}.txt
  let "run_test_count++"
  echo "### Test to be run: 'time ${path_prefix}ligolw_dq_query_dqsegdb -t https://${server}.ligo.org  $dq_query_gps_time  &>  $output_file' "
  time ${path_prefix}ligolw_dq_query_dqsegdb -t https://${server}.ligo.org  $dq_query_gps_time  &>  $output_file
  exit_code=$?
  dq_query_line_count=`cat $output_file | wc --lines`
  if [ "$exit_code" -eq 0 ] && [ "$dq_query_line_count" -ge "$dq_query_wc" ] && [ "$dq_query_line_count" -le $((dq_query_wc + 2)) ]; then
    let "passed_test_count++"
    echo "Test passed: Line count for $output_file is $dq_query_line_count, as expected."
  else
    let "failed_test_count++"
    echo "### ERROR ### Test failed."
    if [ "$exit_code" -ne 0 ]; then echo "          ### Exit code was $exit_code.  This should be investigated."; fi
    if [ "$dq_query_line_count" -ne "$dq_query_wc" ]; then echo "          ### Line count for output file was $dq_query_line_count, but was expected to be $dq_query_wc.  This should be investigated."; fi
  fi
# reference:
# dq_query_gps_time=1200000000
#  1737   3694  43621 ligolw_dq_query_dqsegdb_output.txt
# dq_query_gps_time=1210000000
#  1765   3715  44332 ligolw_dq_query_dqsegdb_output5.txt
fi


# threaded publish test
if [ "$publish_threaded_test_bool" -eq 1 ]; then
###here3
  echo "###"
  echo "### threaded publish test"
  # note: path_prefix should point to [something]/dqsegdb/bin
  input_dir=/home/detchar/dqsegdb/threaded_publish_test_files/
  output_dir=/tmp/threaded_publish_${gps_start_time}
  publisher_script=$output_dir/run_publishing_threaded_test.sh
  state_file=$output_dir/H-DQ_Segments_publishing_threaded_test.xml
  dqxml_dir=$output_dir/dqxml/H1/
  #publisher_server="https://${server}.ligo.org"
  publisher_server="http://${server}.ldas.cit"
  if [ "$server" == "segments" ]
  then
    echo "### ERROR ### Writing a test flag to server 'segments' is not allowed.  Exiting this if-then block."
    let "skipped_test_count++"
  else
    mkdir $output_dir
    if [ ! -w "$output_dir" ]; then
      let "skipped_test_count++"
      echo "### ERROR ### Could not created output dir $output_dir for threaded publish test"
    else
      # create the dir with files for publishing, then modify the publisher script
      cp -rp $input_dir/*  $output_dir
      if [ "$path_prefix" == "" ]; then
        #path_dir=`which ligolw_publish_threaded_dqxml_dqsegdb`
        path_dir=$(dirname `which ligolw_publish_threaded_dqxml_dqsegdb`)
      else
        path_dir=$path_prefix
      fi
    echo "Debugging: input_dir=$input_dir ; output_dir=$output_dir ; publisher_script=$publisher_script ; state_file=$state_file ; dqxml_dir=$dqxml_dir ; publisher_server=$publisher_server ; path_dir=$path_dir"
      if [ ! -e "$output_dir/run_publishing_threaded_test.sh" ] || [ ! -e "$output_dir/H-DQ_Segments_publishing_threaded_test.xml" ] || [ ! -d "$output_dir/dqxml/H1/" ]; then
        echo "### ERROR ### Missing one or more of the following: publisher script ($publisher_script), state file ($state_file), dqxml dir ($dqxml_dir).  Threaded publishing test cannot continue."
        let "skipped_test_count++"
      else
        cp -p $publisher_script $publisher_script.bak
        cp -p $state_file $state_file.bak
        # since we're substituting paths, we don't want to use "/" as the sed delimiter; we'll use "+", since that almost never shows up in a path (see https://stackoverflow.com/questions/12061410/how-to-replace-a-path-with-another-path-in-sed )
        sed -i "s+bin_target+$path_dir+g" $publisher_script
        sed -i "s+file_target+$output_dir+g" $publisher_script
        sed -i "s+segment_url+$publisher_server+g" $publisher_script
        # now we try to publish
        echo "### Publishing command: ' $publisher_script H '"
        $publisher_script H
        let "run_test_count++"
        # now we check the results of publishing; note: grep returns exit code 0 if it finds what it's looking for, exit code 1 if it doesn't find it, exit code 2 if the file isn't there
        publish_error_count=0
        if [ `cat $state_file | wc --lines` -eq `cat $state_file.bak | wc --lines` ]; then
          echo "### ERROR ### State file was not updated (' wc --lines  $state_file  $state_file.bak ').  Publishing failed."
          let "publish_error_count++"
        fi
        grep "pending files = []" $output_dir/*.log &> /dev/null
        if [ $? -eq 0 ]; then
          echo "### ERROR ### Publisher did not find any files to publish (see 'pending files =' in $output_dir/*.log ).  Publishing failed."
          let "publish_error_count++"
        fi
        grep -i -e "error " -e warn -e fail -e invalid $output_dir/*.log &> /dev/null
        if [ $? -eq 0 ]; then
          echo "### ERROR ### Publisher encountered errors (run 'grep -i -e error -e warn -e fail -e invalid $output_dir/*.log').  Assuming publishing failed."
          grep "cannot use a string pattern on a bytes-like object" $output_dir/*.log &> /dev/null
          if [ $? -eq 0 ]; then echo "### ERROR ### Error message 'cannot use a string pattern on a bytes-like object' in log file ($output_dir/*.log) implies that the 'glue' package (lscsoft-glue) needs to be updated to the Python 3 version (to update ldbd.py)."; fi
          let "publish_error_count++"
        fi
        if [ "$publish_error_count" -gt 0 ]; then
          let "failed_test_count++"
        else
          let "passed_test_count++"
          echo "Threaded publisher (ligolw_publish_threaded_dqxml_dqsegdb) test - PASSED."
        fi
      fi
    fi
  fi
fi

# let "run_test_count++"   let "passed_test_count++"   let "failed_test_count++"   let "skipped_test_count+=1"


echo "###"
echo "### SUMMARY ###"
end_time=$(date)
gps_end_time=$((`date +%s` - 315964782))
#echo "### INFO ### End time: $(date)  - GPS time: ~$((`date +%s` - 315964782))"
echo "### INFO ### Start time: $start_time  - GPS time: ~$gps_start_time"
echo "### INFO ### End time:   $end_time  - GPS time: ~$gps_end_time"
echo "### INFO ### Total run time = ~$((gps_end_time - gps_start_time)) seconds."
echo "### INFO ### Run test count:      $run_test_count"
echo "### INFO ### Passed test count:   $passed_test_count"
echo "### INFO ### Failed test count:   $failed_test_count"
echo "### INFO ### Skipped test count:  $skipped_test_count"
if [ $((passed_test_count + failed_test_count)) -ne $run_test_count ]
then  echo "### WARNING ### Number of run tests ($run_test_count) is not equal to the sum of passed tests and failed tests ($passed_test_count + $failed_test_count = $((passed_test_count + failed_test_count)) )."; exit_code=3
fi
echo "###"

exit $exit_code

# Reference
# ping test:
# ligolw_segment_query_dqsegdb  --segment-url http://segments.ldas.cit  --ping
#
# query to get known segments:
# ligolw_segment_query_dqsegdb --segment-url https://segments.ligo.org --query-segments --include-segments K1:GRD-LSC_LOCK_STATE_N_EQ_1000:1  --gps-start-time 0 --gps-end-time 1300000000  | grep segment_definer:segment_def_id:0  |  tail
#
# science mode flags:
# LHO: DMT-ANALYSIS_READY
# LLO: DMT-ANALYSIS_READY
# VGO: ITF_SCIENCE
# KAG: GRD-LSC_LOCK_STATE_N_EQ_1000
# GEO: GEO-UP
#
# xml output:
# 'known' lines look like this (leading whitespace removed):
# "",1100235797,0,"process:process_id:0","segment_definer:segment_def_id:0","segment_summary:segment_sum_id:0",1100000000,0,
# ...
# "",1110000000,0,"process:process_id:0","segment_definer:segment_def_id:0","segment_summary:segment_sum_id:42",1109695511,0,
# "",1110000000,0,"process:process_id:0","segment_definer:segment_def_id:1","segment_summary:segment_sum_id:43",1100000000,0
# 'active' lines look like this (leading whitespace removed):
# 1100235797,0,"process:process_id:0","segment_definer:segment_def_id:1","segment:segment_id:0",1100000000,0,
#
# find 'known' lines: 'grep segment_definer:segment_def_id:0.*segment_summary:segment_sum_id'
# find 'active' lines: 'grep segment:segment_id:'

