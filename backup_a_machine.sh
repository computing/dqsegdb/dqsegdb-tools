#!/bin/bash

### This is a script to backup all the files that we think are important on a segments-family machine, sending them to a dir that is not on that machine.
### Script created by Robert Bruntz on 2019.01.08 (based on previously-taken notes to perform this task).  Some notes on sources of contents are at the end.
### to do:
###   * add machine-specific stuff (i.e., stuff specifically for segments-web; done: segments, segments-backup)
###   * save the output to a log file (simultaneous to printing to screen)
###   * save log files to a tmp dir before tarballing them, to avoid 'tar: [something]: file changed as we read it' errors/warnings?
###   * add option to do mysqldump of DBs? maybe in machine-specific section?
###   * compare backed-up stuff to installation script
###   * figure out how to update .bash_history before backing it up ("history -n"?)
###   * how to get aliases backed up?
###   * should we be backing up SSL certificates? (currently, we do)
###   * how to handle backups of files (e.g., '/etc/odbc.ini.bck.Aug182015')?
###   * add option of whether to tarball dir's or not?


### Some things to do and check:
###   * Create a copy of this script (maybe as something like "backup_segments-web_2019.02.30.sh"), then modify that version, then run that version.
###   * Note that this script is expected to be run as root.  Running it as another user will probably cause it to crash, when it tries to read files it can't read.  (But maybe not.)
###   * Set the machine name, in the 'output_prefix' variable
###   * Make sure that all users who have crontabs are accounted for
###   * Check all configuration variables (e.g., backup_all_root, backup_root_bin, backup_http, backup_http_logs, backup_certificates, backup_grid_security, backup_lgmm, backup_db_stuff, etc.)


### set configuration variables
# This is the dir that the output directory will go in (i.e., the saved files will go in a new dir created inside THIS dir)
  #container_dir=/backup/segdb/reference/machine_backups/
  container_dir=/backup/segdb/machine_backups/
# This is the first part of the output dir name; probably easiest to use the machine's short name (segments, segments-backup, segments-web, segments-dev, segments-dev2, segments-other, etc.)
# Warning: You can set this variable to any arbitrary value (even with typos!); no check is done to make sure the name matches the machine.
  output_prefix="segments-dev"
# This option will read the machine name, rather than using the 'output_prefix' specified above
  use_uname=1
    if [ $use_uname -eq 1 ]; then output_prefix=`uname -n`; fi
# Get the date and time used for the backup dir name
  output_date=`date +%Y.%m.%d-%H.%M.%S`
# Name the output dir here
  output_dir=$container_dir${output_prefix}_$output_date
# Verbose output? (prints some notices that would otherwise be skipped)
  verbose=1
### In the following variables, 'variable=1' means 'backup that dir' (and 'variable=0' means 'don't backup that dir') (and 'variable=(anything else)' is rolling the dice)
## Quick backup: everything set to 1 except backup_all_root, backup_opt_dqsegdb, [backup_http_logs - no longer excludes this; 2020.07.24], backup_db_data_dir, and [backup_machine_specific - no longer excludes this; 2020.07.24], b/c each one could take a while
##               if set to zero, it doesn't change anything
  quick_backup=1
## Full backup: everything set to 1, so it could take a while
##              if set to zero, it doesn't change anything
##              note that 'full backup' overrides 'quick backup'
  full_backup=0
# Backup the entire /root/ dir? (might not want to do this if there's a lot of stuff in there)
  backup_all_root=1
# Backup /root/bin/? (separately from /root/, whether that is backed up or not)
  backup_root_bin=1
# Backup /usr1/ldbd/ or /local/ldbd/?
  backup_all_ldbd=1
# Backup /opt/dqsegdb/?
  backup_opt_dqsegdb=1
# Backup system stuff (/etc/fstab, /etc/profile, uname, ifconfig, etc.)
  backup_system_stuff=1
# Backup (and tarball) all /etc/?
  backup_all_etc=1
# Backup cron stuff, incl. user crontabs and system cron
  backup_cron=1
# Backup http stuff, excluding logs?
  backup_http=1
# Backup http logs? (separately from other http stuff)
  backup_http_logs=1
# Backup /var/log/ without subdirs?
  backup_var_log_without_subdirs=0
# Backup certificates (*key*, *cert*, *pem*) in /etc/grid-security/?
  backup_certificates=1
# Backup everything in /etc/grid-security/?
  backup_grid_security=1
# Backup /etc/pki/tls/? (includes SSL certificates and... other stuff)
  backup_tls=1
# Backup lgmm stuff - config file, grid-mapfile, whitelist, blacklist?
  backup_lgmm=1
# Backup Shibboleth stuff?
  backup_shib=1
# Backup mysql/mariadb stuff? (config files, log file, settings, etc.; not DBs themselves)
  backup_db_stuff=1
# Backup the entire mysql/maridb data dir? (contains DBs, but is not the same as mysqldump)
  backup_db_data_dir=0
# Backup history for root and ldbd?
  backup_history=1
# Do machine-specific backups (i.e., specific stuff only on a particular machine, like segments-backup)?  (compares the variable 'output_prefix' to a known list)
  backup_machine_specific=1
# Print output for the user to verify backup?
  user_verify=1
# Note - if any more variables are added, be sure to add them to the 'full backup' and 'quick backup' sections below, as well as to the 'user verify' section at the end


### report startup info
echo "###"
echo "### INFO ### Start time:  " `date`
echo "### INFO ### Output dir:  " `echo $output_dir`
if [ $full_backup -eq 1 ]
then
    backup_all_root=1; backup_opt_dqsegdb=1; backup_cron=1; backup_http_logs=1; backup_var_log_without_subdirs=1; backup_db_data_dir=1; backup_machine_specific=1;
    backup_root_bin=1; backup_all_ldbd=1; backup_system_stuff=1; backup_all_etc=1; backup_http=1; backup_certificates=1; backup_grid_security=1; backup_tls=1; backup_lgmm=1;
    backup_db_stuff=1; backup_history=1; user_verify=1;
    echo "### NOTICE ### Performing a 'full backup'."
else
  if [ $quick_backup -eq 1 ]
  then
    backup_all_root=0; backup_opt_dqsegdb=0; backup_cron=1; backup_http_logs=1; backup_var_log_without_subdirs=1; backup_db_data_dir=0; backup_machine_specific=1;
    backup_root_bin=1; backup_all_ldbd=1; backup_system_stuff=1; backup_all_etc=1; backup_http=1; backup_certificates=1; backup_grid_security=1; backup_tls=1; backup_lgmm=1;
    backup_db_stuff=1; backup_history=1; user_verify=1;
    echo "### NOTICE ### Performing a 'quick backup'."
  fi
fi

### check machine name
if [ `uname -n` != $output_prefix ]
then
  echo "### WARNING ### The reported machine name (`uname -n`) does not match the machine name in the script ($output_prefix)."
  echo "            ### This could cause trouble later, when machine-specific tasks are run."
  echo "            ### You have 10 seconds to abort this script (ctrl-c or ctrl-z) before it continues."
  sleep 10
fi
# special syntax: https://unix.stackexchange.com/questions/111508/bash-test-if-word-is-in-set
if [[ ! "$output_prefix" =~ ^(segments|segments-backup|segments-dev|segments-dev2|segments-web|segments-old|segments-backup-old|segments-dev-old|segments-dev2-old|segments-web-old)$ ]]
then
  echo "### WARNING ### The machine name ($output_prefix) is not in the list of recognized machines (below), so no machine-specific tasks will be run."
  echo "            ### You have 10 seconds to abort this script (ctrl-c or ctrl-z) before it continues."
  echo "            ### Recongnized machines: segments, segments-backup, segments-dev, segments-dev2, segments-web, segments-old, segments-backup-old, segments-dev-old, segments-dev2-old, segments-web-old"
  sleep 10
fi

### check for existence of target dir and make it
###   (note that the output dir is specific enough (based on time, up to the second) that we should never have a conflict with an existing dir)
if [ -d $output_dir ]; then
  echo "### ERROR ### Output dir ($output_dir) already exists."
  echo "          ### I can't fix this.  Exiting."
  exit
else
  mkdir -p $output_dir
fi


### backing-up stuff starts here

### /root/   ###root
if [ $backup_all_root -eq 1 ]
then
  echo "### INFO ### Backing up /root/"
  tar czf  $output_dir/root_files_${output_date}.tgz  -C /  ./root/
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up all root stuff"; fi
fi
if [ $backup_root_bin -eq 1 ]
then
  echo "### INFO ### Backing up /root/bin/"
  tar czf  $output_dir/root_bin_files_${output_date}.tgz  -C /root/  ./bin/
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up /root/bin/ (by itself)"; fi
  if [ $backup_all_root -ne 1 ]
  then
    echo "### WARNING ### Neither 'backup_all_root' nor 'backup_root_bin' are set to 1, so neither /root/ nor /root/bin/ are being backed up."
  fi
fi

### ldbd   ###ldbd
if [ $backup_all_ldbd -eq 1 ]
then
  if [ -d /usr1/ldbd/ ]
  then
    echo "### INFO ### Backing up /usr1/ldbd/"
    tar czf  $output_dir/ldbd_usr1_files_${output_date}.tgz  -C /usr1/  ./ldbd/
  fi
  if [ -d /local/ldbd/ ]
  then
    echo "### INFO ### Backing up /local/ldbd/"
    tar czf  $output_dir/ldbd_local_files_${output_date}.tgz  -C /local/  ./ldbd/
  fi
  if [ ! -d /usr1/ldbd/ -a ! -d /local/ldbd/ ]
  then
    echo "### WARNING ### Neither /usr1/ldbd/ nor /local/ldbd/ exists, so ldbd files are not being backed up."
  fi
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up all ldbd stuff"; fi
fi

### /opt/dqsegdb/   ###opt   ###dqsegdb
if [ $backup_opt_dqsegdb -eq 1 ]
then
  if [ -d /opt/dqsegdb/ ]
  then
    echo "### INFO ### Backing up /opt/dqsegdb/"
    tar czf  $output_dir/opt_dqsegdb_${output_date}.tgz  -C /opt/  ./dqsegdb/
  else
    if [ $verbose -eq 1 ]; then echo "### NOTICE ### No /opt/dqsegdb/ dir to back up (so not backing it up)"; fi
  fi
fi

### system stuff   ###system
if [ $backup_system_stuff -eq 1 ]
then
  echo "### INFO ### Backing up hostname info"
  hostname -A &> $output_dir/hostname_$output_date
  echo "### INFO ### Backing up /etc/init.d/"
  tar czf  $output_dir/etc_init.d_${output_date}.tgz  -C /etc/  ./init.d/
  echo "### INFO ### Backing up /etc/fstab"
  cp /etc/fstab  $output_dir/etc_fstab_$output_date
  if [ -e /etc/profile ]
  then
    echo "### INFO ### Backing up /etc/profile"
    cp  /etc/profile  $output_dir/etc_profile_$output_date
  else
    if [ $verbose -eq 1 ]; then echo "### NOTICE ### No /etc/profile to back up (so not backing it up)"; fi
  fi
  echo "### INFO ### Backing up /etc/default/ (if it exists)"
  if [ -d /etc/default/ ]; then tar czf $output_dir/etc_default_${output_date}.tgz  -C /etc/  ./default/ ; fi
  echo "### INFO ### Backing up 'uname -a'"
  uname -a >> $output_dir/uname_$output_date
  echo "### INFO ### Backing up 'ifconfig'"
  ifconfig >> $output_dir/ifconfig_$output_date
  echo "### INFO ### Backing up environment variables (env)"
  env >> $output_dir/env_$output_date
  echo "### INFO ### Backing up aliases"
  # note: must be a login shell to load aliases, thus the "/bin/bash -l" part
  /bin/bash -l alias >> $output_dir/alias_$output_date
  echo "### INFO ### Backing up OS version (if available)"
  if [ -e /etc/redhat-release ]; then cp -p /etc/redhat-release  $output_dir/etc_redhat-release_$output_date; fi
  if [ -e /etc/os-release ]; then cp -p /etc/os-release  $output_dir/etc_os-release_$output_date; fi
  if [ -e /etc/yum/vars/slreleasever ]; then cp -p /etc/yum/vars/slreleasever  $output_dir/etc_yum_vars_slreleasever_$output_date; fi
  if [ -e /etc/dnf/vars/stream ]; then cp -p /etc/dnf/vars/stream  $output_dir/etc_dnf_vars_stream_$output_date; fi
  echo "### INFO ### Backing up rpm and yum stuff"
  rpm -qa | sort >> $output_dir/rpm_-qa_sorted_$output_date
  yum list installed  >> $output_dir/yum_list_installed_$output_date
  yum repolist all  >> 	$output_dir/yum_repolist_all_$output_date
  which dnf 2> /dev/null; exit_code=$?
  if [ "$exit_code" -eq 0 ]
  then   # dnf *is* installed
      dnf repoquery -a --installed  >> $output_dir/repoquery_-a_--installed_$output_date
      cd /var/log/; tar czf  $output_dir/var_log_dnf_${output_date}.tgz  ./dnf*
      cp -p /etc/dnf/dnf.conf  $output_dir/dnf.conf_$output_date
      tar czf  $output_dir/yum_${output_date}.tgz  -C /etc/  ./dnf/
  else   # dnf *is not* installed; assume yum (et al.) installed
      repoquery -a --installed  >> $output_dir/repoquery_-a_--installed_$output_date
      cd /var/log/; tar czf  $output_dir/var_log_yum_${output_date}.tgz  ./yum*
      cp -p /etc/yum.conf  $output_dir/yum.conf_$output_date
      tar czf  $output_dir/yum_${output_date}.tgz  -C /etc/  ./yum/
      tar czf  $output_dir/yum.repos.d_${output_date}.tgz  -C /etc/  ./yum.repos.d/
  fi
  echo "### INFO ### Backing up services stuff"
  service --status-all >> $output_dir/service_--status_all_$output_date  2> /dev/null
  chkconfig --list >> $output_dir/chkconfig_--list_$output_date  2> /dev/null
  systemctl list-unit-files >> $output_dir/systemctl_list-unit-files_$output_date
  netstat -tulpn >> $output_dir/netstat_-tulpn_$output_date
  systemctl &> /dev/null   ### test the command; get an error code (0 = OK; not 0 = command doesn't work on this system)
    if [ $? -eq 0 ]   ### use the previous line's error code
    then
      systemctl >> $output_dir/systemctl_$output_date
      systemctl list-unit-files >> $output_dir/systemctl_list-unit-files_$output_date
    fi
  echo "### INFO ### Backing up iptables stuff"
  iptables-save > $output_dir/iptables-save_$output_date
  ip6tables-save > /tmp/ip6tables-save_$output_date
  if [ `cat /tmp/ip6tables-save_$output_date | wc -c` -gt 0 ]; then mv /tmp/ip6tables-save_$output_date  $output_dir/  2> /dev/null ; else rm /tmp/ip6tables-save_$output_date; fi
  iptables -L -v > $output_dir/iptables_-L_-v_$output_date
  iptables -L -v -n > $output_dir/iptables_-L_-v_-n_$output_date
  echo "### INFO ### Backing up OpenSSL version"
  openssl version &> $output_dir/openssl_version_$output_date
  echo "### INFO ### Backing up Apache (httpd) version and build parameters"
  httpd -V &> $output_dir/httpd_version_$output_date
  echo "### INFO ### Backing up Apache (httpd) list of modules compiled into the server"
  httpd -l &> $output_dir/httpd_-l_$output_date
  echo "### INFO ### Backing up Apache (httpd) list of directives provided by static modules"
  httpd -L &> $output_dir/httpd_-L_$output_date
  echo "### INFO ### Backing up Apache (httpd) list of loaded Static and Shared Modules"
  httpd -M &> $output_dir/httpd_-M_$output_date
  echo "### INFO ### Backing up Apache (httpd) settings as parsed from the config file"
  httpd -S &> $output_dir/httpd_-S_$output_date
  echo "### INFO ### Backing up Apache (httpd) syntax tests for configuration files"
  httpd -t &> $output_dir/httpd_-t_$output_date
  echo "### INFO ### Backing up Apache (httpd) details of the virtual host configuration"
  httpd -D DUMP_VHOSTS &> $output_dir/httpd_-D_DUMP_VHOSTS_$output_date
  echo "### INFO ### Backing up PHP version"
  php --version &> $output_dir/php_version_$output_date
  if [ -d /usr/share/phpmyadmin/  -o  -d /usr/share/phpMyAdmin/ ]   # /usr/share/phpMyAdmin/ = SL7; /usr/share/phpmyadmin/ = RL8
  then
    echo "### INFO ### Backing up phpMyAdmin version"
    phpmyadmin_file=$output_dir/phpMyAdmin_version_$output_date
    touch $phpmyadmin_file
    # using a variety of empirical tests, some of which work only for RL8, one that works for RL8 and SL7, and some of which work only for SL7
    grep -i version /usr/share/php?y?dmin/README 2> /dev/null| grep -v "GNU General Public License version" >> $phpmyadmin_file
    grep -i version /usr/share/php?y?dmin/composer.json 2> /dev/null| grep -v package-versions-deprecated >> $phpmyadmin_file
    grep -i version /usr/share/php?y?dmin/package.json 2> /dev/null >> $phpmyadmin_file
    grep -i version /usr/share/php?y?dmin/doc/html/_static/documentation_options.js 2> /dev/null >> $phpmyadmin_file
    grep -i "public const VERSION" /usr/share/php?y?dmin/libraries/classes/Version.php 2> /dev/null >> $phpmyadmin_file
    grep "<title>.*phpMyAdmin .* documentation</title>" /usr/share/php?y?dmin/doc/html/index.html 2> /dev/null >> $phpmyadmin_file   # this is the only one that works for both SL7 (4.4.15.10) and RL8 (5.2.1)
    grep -inR "set('PMA_VERSION'," /usr/share/php?y?dmin/ 2> /dev/null >> $phpmyadmin_file
    grep -inR "define('LICENSE_FILE'," /usr/share/php?y?dmin/ 2> /dev/null >> $phpmyadmin_file
  fi
  echo "### INFO ### Backing up MariaDB/MySQL version"
  mysqladmin version &> $output_dir/mariadb_version_$output_date
  echo "### INFO ### Backing up /etc/security/access.conf"
  cp -p /etc/security/access.conf &> $output_dir/etc_security_access.conf_$output_date
  echo "### INFO ### Backing up system logs (/var/log/, excluding httpd and publishing)"
  tar czf $output_dir/var_log_$output_date.tgz  --exclude=./log/httpd  --exclude=./log/publishing  -C /var  ./log
  echo "### INFO ### Backing up /etc/sysconfig/"
  tar czf $output_dir/etc_sysconfig_${output_date}.tgz  -C /etc/  ./sysconfig
  echo "### INFO ### Backing up /usr/share/ (excluding /usr/share/dqsedgdb_web*, if present)"
  tar czf $output_dir/usr_share_${output_date}.tgz  --exclude=./share/dqsegdb_web*  -C /usr/  ./share
  echo "### INFO ### Checking local Puppet run"
  echo "# /root/local_puppet.sh --noop" &> $output_dir/puppet_local_$output_date
  /root/local_puppet.sh --noop &>> $output_dir/puppet_local_$output_date
  echo "### INFO ### Checking CIT Puppet run"
  echo "# puppet agent -t --noop" &> $output_dir/puppet_CIT_$output_date
  puppet agent -t --noop &>> $output_dir/puppet_CIT_$output_date
  echo "### INFO ### Backing up Shibboleth (shibd) version"
  shibd -v &> $output_dir/shibd_version_$output_date
  echo "### INFO ### Backing up Shibboleth (shibd) configuration test output"
  shibd -t &> $output_dir/shibd_-t_$output_date
  echo "### INFO ### Backing up Git version"
  git --version &> $output_dir/git_version_$output_date
  if [ -d /usr/share/dqsegdb_web ]
  then
    echo "### INFO ### Backing up /usr/share/dqsegdb_web/ (excluding /usr/share/dqsedgdb_web/download*)"
    tar czf $output_dir/usr_share_dqsegdb_web_nodownloads_${output_date}.tgz  --exclude=./dqsegdb_web/download*  -C /usr/share/  ./dqsegdb_web
  fi
  if [ -d /var/www/nagios ]
  then
    echo "### INFO ### Backing up /var/www/nagios/"
    tar czf $output_dir/var_www_nagios_${output_date}.tgz  -C /var/www/  ./nagios
  fi
  python --version  &>  $output_dir/python_version_${output_date}.txt
  python2 --version  &>  $output_dir/python2_version_${output_date}.txt
  python3 --version  &>  $output_dir/python3_version_${output_date}.txt
  python3 -m sysconfig  &>  $output_dir/python3_sysconfig_${output_date}.txt
  python3 -m platform  &>  $output_dir/python3_platform_${output_date}.txt
  python3 -m locale  &>  $output_dir/python3_locale_${output_date}.txt
  df -h &> $output_dir/df_h_${output_date}.txt
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up system stuff"; fi
fi

### /etc/   ###etc   ###all_etc
if [ "$backup_all_etc" -eq 1 ]
then
  echo "### INFO ### Backing up entire /etc/ dir"
  tar czf $output_dir/etc_${output_date}.tgz  /etc/
fi

### crontab   ###crontab
if [ $backup_cron -eq 1 ]
then
  echo "### INFO ### Backing up cron files and crontabs"
  # system cron files
  tar czf $output_dir/cron_${output_date}.tgz  /etc/cron*
  # user crontabs
  for user in `ls -1 /var/spool/cron/`; do crontab -l -u $user >> $output_dir/crontab_${user}_$output_date; done
  # old way
  #crontab_count=0
  #if [ -e /var/spool/cron/root ]; then crontab -l -u root >> $output_dir/crontab_root_$output_date; crontab_count=`echo $(($crontab_count+1))`; fi
  #if [ -e /var/spool/cron/ldbd ]; then crontab -l -u ldbd >> $output_dir/crontab_ldbd_$output_date; crontab_count=`echo $(($crontab_count+1))`; fi
  #if [ -e /var/spool/cron/segdb ]; then crontab -l -u segdb >> $output_dir/crontab_segdb_$output_date; crontab_count=`echo $(($crontab_count+1))`; fi
  #if [ `ls -1 /var/spool/cron/ | wc --lines` -gt $crontab_count ]; then echo "### WARNING ### /var/spool/cron/ has more entries than just root, ldbd, and segdb; any others are not being backed up."; fi
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up crontabs"; fi
fi

### http   ###httpd
if [ $backup_http -eq 1 ]
then
  echo "### INFO ### Backing up http stuff (except for http log files)"
  if [ -e /etc/init.d/httpd ]; then cp /etc/init.d/httpd  $output_dir/etc_init.d_httpd_$output_date; fi
  tar czf  $output_dir/etc_httpd_${output_date}.tgz  -C  /etc/  ./httpd/
  tar czf  $output_dir/etc_httpd_modules_${output_date}.tgz  -C  /etc/httpd/  ./modules/
  cp /etc/sysconfig/httpd  $output_dir/etc_sysconfig_httpd_$output_date
  if [ -e /etc/systemd/system/multi-user.target.wants/httpd.service ]; then \
    cp /etc/systemd/system/multi-user.target.wants/httpd.service  $output_dir/etc_sytemd_system_multi-user.target.wants_httpd.service_$output_date; fi
  #cp /etc/odbc.ini  $output_dir/etc_odbc.ini_$output_date   ### this is done in the DB section
  #cp /etc/odbcinst.ini  $output_dir/etc_odbcinst.ini_$output_date   ### this is done in the DB section
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up all http stuff"; fi
fi
if [ $backup_http_logs -eq 1 ]
then
  echo "### INFO ### Backing up http logs"
  tar czf  $output_dir/var_log_httpd_${output_date}.tgz  -C  /var/log/  ./httpd/
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up http logs"; fi
fi
if [ $backup_var_log_without_subdirs -eq 1 ]
then
  echo "### INFO ### Backing up /var/log/ without subdirs"
  tar --no-recursion -czf  $output_dir/var_log_${output_date}.tgz  /var/log/*
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up /var/log/"; fi
fi

### certificates   ###certificates
if [ $backup_certificates -eq 1 ]
then
  echo "### INFO ### Backing up certificates in /etc/grid-security/"
  cd /etc/grid-security/   ### cd to the dir so that we can use wildcards in the next line
  tar czf  $output_dir/grid-security_certificates_${output_date}.tgz  ./*cert*  ./*key*  ./*pem*
  cd - &> /dev/null   ### return to the previous dir, but don't report it to std out
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up certificates in /etc/grid-security/"; fi
fi

### grid-security   ###grid-security
if [ $backup_grid_security -eq 1 ]
then
  echo "### INFO ### Backing up /etc/grid-security/"
  tar czf  $output_dir/grid_security_${output_date}.tgz  -C /etc/  ./grid-security/
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up all of /etc/grid-security/"; fi
fi

### tls   ###tls   ###ssl
if [ $backup_tls -eq 1 ]
then
  echo "### INFO ### Backing up /etc/pki/tls/"
  tar czf  $output_dir/etc_pki_tls_${output_date}.tgz  -C /etc/pki/  ./tls/
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up all of /etc/pki/tls/"; fi
fi

### lgmm   ###lgmm   ###grid-mapfile   ###whitelist   ###blacklist
if [ $backup_lgmm -eq 1 ]
then
  echo "### INFO ### Backing up lgmm stuff"
  if [ -e /etc/lgmm/lgmm_config.py ]
  then
    cp /etc/lgmm/lgmm_config.py  $output_dir/etc_lgmm_lgmm_config.py_$output_date
  else
    echo "### WARNING ### Couldn't find  /etc/lgmm/lgmm_config.py  so it has not been backed up."
  fi
  if [ -e /etc/grid-security/grid-mapfile ]
  then
    cp /etc/grid-security/grid-mapfile  $output_dir/etc_grid-security_grid-mapfile_$output_date
  else
    echo "### WARNING ### Couldn't find  /etc/grid-security/grid-mapfile  so it has not been backed up."
  fi
  if [ -e /etc/grid-security/whitelist ]; then cp  /etc/grid-security/whitelist  $output_dir/etc_grid-security_whitelist_$output_date; fi
  if [ -e /etc/grid-security/blacklist ]; then cp  /etc/grid-security/blacklist  $output_dir/etc_grid-security_blacklist_$output_date; fi
  if [ -d /var/log/lgmm/ ]; then tar czf  $output_dir/var_log_lgmm_${output_date}.tgz  -C /var/log/  ./lgmm/ ; fi
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up lgmm stuff"; fi
fi

### Shibboleth   #shibboleth
if [ $backup_shib -eq 1 ]
then
  if [ -d /etc/shibboleth ]; then tar czf $output_dir/etc_shibboleth_${output_date}.tgz  -C /etc/  ./shibboleth/; fi
  if [ -e /etc/httpd/conf.d/shib.conf ]; then cp  /etc/httpd/conf.d/shib.conf  $output_dir/etc_httpd_conf.d_shib.conf; fi
  if [ -e /etc/httpd/conf.d/shib.conf ] || [ -d /etc/shibboleth ]; then shibd -t &> $output_dir/shibd_-t_$output_date; fi
  if [ -d /usr/share/shibboleth ]; then tar czf $output_dir/usr_share_shibboleth_${output_date}.tgz  -C /usr/share/  ./shibboleth/; fi
fi

### mysql/mariadb   ###db   ###mysql   ###mariadb
if [ $backup_db_stuff -eq 1 ]
then
  echo "### INFO ### Backing up mysql/mariadb stuff"
  if [ -e /etc/my.cnf ]; then cp  /etc/my.cnf  $output_dir/etc_my.cnf_$output_date ; fi
  if [ -d /etc/my.cnf.d/ ]; then  tar czf  $output_dir/etc_my.cnf.d_${output_date}.tgz  -C /etc/  ./my.cnf.d/; fi
  if [ -e /etc/odbc.ini ]; then cp  /etc/odbc.ini  $output_dir/etc_odbc.ini_$output_date ; fi
  if [ -e /etc/odbcinst.ini ]; then cp  /etc/odbcinst.ini  $output_dir/etc_odbcinst.ini_$output_date ; fi
  if [ -e /var/log/mysqld.log ]; then cp  /var/log/mysqld.log  $output_dir/var_log_mysqld.log_$output_date ; fi
  mysql -e "show variables"  >  $output_dir/mysql_-e_show_variables_$output_date
  mysql -e "status"  >  $output_dir/mysql_-e_status_$output_date
  mysql -e "show status"  >  $output_dir/mysql_-e_show_status_$output_date
  mysql -e "show global status"  >  $output_dir/mysql_-e_show_global_status_$output_date
  if [ -d /var/log/mariadb/ ]; then  tar czf  $output_dir/var_log_mariadb_${output_date}.tgz  -C /var/log/  ./mariadb/; fi
  echo "### NOTICE ### Here are the DBs that exist on this machine:"
  echo "           ### (It is common to have one or more of the following on a standard mysql/mariadb installation: information_schema, mysql, performance_schema, test.)"
  mysql -e "show databases"
  echo "           ### You might want to backup some of those DBs (perhaps using a tool like '/root/bin/backup_dqsegdb_mysqldatabase_directed.sh')."
  mysql -e "show databases" > $output_dir/mysql_show_databases_$output_date
fi
if [ $backup_db_data_dir -eq 1 ]
then
  echo "### INFO ### Backing up entire MySQL/MariaDB data dir ( /var/lib/mysql/ ).  (This might take a little while.)"
  if [ -d /var/lib/mysql ]
  then
    tar czfh  $output_dir/var_lib_mysql_${output_date}.tgz  --exclude=./mysql/mysql.sock  -C /var/lib/  ./mysql/
  else
    echo "### WARNING ### Did not find DB dir ( /var/lib/mysql/ ), so it isn't being backed up.  You might want to look into this."
  fi
fi

### items specific to each machine   ###machine   ###segments   ###segments-backup   ###segments-web   ###segments-dev   ###segments-dev2
                                     ###segments-old   ###segments-backup-old   ###segments-web-old   ###segments-dev-old   ###segments-dev2-old
if [ $backup_machine_specific -eq 1 ]
then
  machine_name_match=0
  if [ "$output_prefix" == "segments" -o "$output_prefix" == "segments-old" ]   ###segments   ###segments-old   ###seg   ###seg-old
  then
    echo "### INFO ### Backing up specific items for machine '$output_prefix'."
    machine_name_match=1
    if [ -d /var/log/publishing/dev/ ]
    then
      tar czf  $output_dir/var_log_publishing_dev_${output_date}.tgz  -C /var/log/publishing/  ./dev/
    else
      echo "### WARNING ### Publisher log files on segments.ligo.org should be in /var/log/publishing/dev/, but that dir does not exist."
      echo "            ### The files *might* be in /root/Publisher/var/run/ or pointed to by /root/bin/publisher_log_files/, but you'll have to find and back them up yourself."
    fi
    if [ -d /root/bin/dqsegdb_current_code ]
    then
      tar czf  $output_dir/root_bin_dqsegdb_current_code_${output_date}.tgz  -C /root/bin/  ./dqsegdb_current_code
    #else
    #  echo "### WARNING ### /root/bin/dqsegdb_current_code files on segments.ligo.org should point to the current publisher code, but that dir does not exist."
    #  echo "            ### You'll have to find and back them up yourself."
    fi
    if [ -d /var/log/publishing/state/ ]
    then
      tar czf  $output_dir/var_log_publishing_state_${output_date}.tgz  -C /var/log/publishing/  ./state/
    else
      if [ -d /root/Publisher/var/spool/ ]
      then
        tar czf  $output_dir/root_Publisher_var_spool_${output_date}.tgz  -C /root/Publisher/var/  ./spool/
      else
        echo "### WARNING ### Publisher state files on segments.ligo.org should be in /var/log/publishing/state/, but that dir does not exist."
        echo "            ### The second option was /root/Publisher/var/spool/, but that dir does not exist."
        echo "            ### If /root/bin/state_files exists, it should point to them, but you'll have to find them and back them up yourself."
      fi
    fi
    if [ $backup_opt_dqsegdb -eq 0 ]
    then
      echo "### INFO ### Backing up /opt/dqsegdb/"
      tar czf $output_dir/opt_dqsegdb_${output_date}.tgz  -C /opt/  ./dqsegdb/
    else
      echo "### INFO ### Not backing up /opt/dqsegdb/ (should have been done already)."
    fi
  fi
  if [ "$output_prefix" == "segments-backup" -o "$output_prefix" == "segments-backup-old" ]   ###segments-backup   ###segments-backup-old   ###backup   ###-backup   ###-backup-old
  then
    echo "### INFO ### Backing up specific items for machine '$output_prefix'."
    machine_name_match=1
    if [ $backup_opt_dqsegdb -eq 0 ]
    then
      echo "### INFO ### Backing up /opt/dqsegdb/"
      tar czf $output_dir/opt_dqsegdb_${output_date}.tgz  -C /opt/  ./dqsegdb/
    else
      echo "### INFO ### Not backing up /opt/dqsegdb/ (should have been done already)."
    fi
  fi
  if [ "$output_prefix" == "segments-web" -o "$output_prefix" == "segments-web-old" ]   ###segments-web   ###segments-web-old   ###web   ###-web   ###-web-old
  then
    echo "### INFO ### Backing up specific items for machine '$output_prefix'."
    machine_name_match=1
    if [ -d /usr/share/dqsegdb_web/ ]
    then
      echo "### INFO ### Backing up /usr/share/dqsegdb_web/ (including 'downloads')"
      tar -czf  $output_dir/usr_share_dqsegdb_web_${output_date}.tgz  -C /usr/share/  ./dqsegdb_web/
    else
      echo "### WARNING ### segments-web.ligo.org should have the dir /usr/share/dqsegdb_web/ , but that dir does not exist."
      echo "            ### You should probably find out why, since there should be important contents there, and maybe do something about it."
    fi
    echo "### WARNING ### Not sure that all items have been added to this script for machine 'segments-web'.  If there is anything else, you should back it up manually."
  fi
  if [ "$output_prefix" == "segments-dev" -o "$output_prefix" == "segments-dev-old" ]   ###segments-dev   ###segments-dev-old   ###dev   ###-dev   ###-dev-old
  then
    echo "### INFO ### Backing up specific items for machine '$output_prefix'."
    machine_name_match=1
    if [ -d /var/log/publishing/dev/ ]
    then
      tar czf  $output_dir/var_log_publishing_dev_${output_date}.tgz  -C /var/log/publishing/  ./dev/
    else
      echo "### WARNING ### Publisher log files on segments-dev.ligo.org should be in /var/log/publishing/dev/, but that dir does not exist."
      echo "            ### The files *might* be in /root/Publisher/var/run/ or pointed to by /root/bin/publisher_log_files, but you'll have to find and back them up yourself."
    fi
    if [ -d /root/bin/dqsegdb_current_code ]
    then
      tar czf  $output_dir/root_bin_dqsegdb_current_code_${output_date}.tgz  -C /root/bin/  ./dqsegdb_current_code
    #else
    #  echo "### WARNING ### /root/bin/dqsegdb_current_code files on segments-dev.ligo.org should point to the current publisher code, but that dir does not exist."
    #  echo "            ### You'll have to find and back them up yourself."
    fi
    if [ -d /var/log/publishing/state/ ]
    then
      tar czf  $output_dir/var_log_publishing_state_${output_date}.tgz  -C /var/log/publishing/  ./state/
    else
      if [ -d /root/Publisher/var/spool/ ]
      then
        tar czf  $output_dir/root_Publisher_var_spool_${output_date}.tgz  -C /root/Publisher/var/  ./spool/
      else
        echo "### WARNING ### Publisher state files on segments-dev.ligo.org should be in /var/log/publishing/state/, but that dir does not exist."
        echo "            ### The second option was /root/Publisher/var/spool/, but that dir does not exist."
        echo "            ### If /root/bin/state_files exists, it should point to them, but you'll have to find them and back them up yourself."
      fi
    fi
    echo "### WARNING ### The specific items for machine 'segments-dev' might be out of date.  If there is anything missing, you should back it up manually (and update this script)."
  fi
  if [ "$output_prefix" == "segments-dev2" -o "$output_prefix" == "segments-dev2-old" ]   ###segments-dev2   ###segments-dev2-old   ###dev2   ###-dev2-old
  then
    echo "### INFO ### Backing up specific items for machine '$output_prefix'."
    machine_name_match=1
    if [ -d /var/log/publishing/dev/ ]
    then
      tar czf  $output_dir/var_log_publishing_dev_${output_date}.tgz  -C /var/log/publishing/  ./dev/
    else
      echo "### WARNING ### Publisher log files on segments-dev2.ligo.org should be in /var/log/publishing/dev/, but that dir does not exist."
      echo "            ### The files *might* be in /root/Publisher/var/run/ or pointed to by /root/bin/publisher_log_files, but you'll have to find and back them up yourself."
    fi
    if [ -d /root/bin/dqsegdb_current_code ]
    then
      tar czf  $output_dir/root_bin_dqsegdb_current_code_${output_date}.tgz  -C /root/bin/  ./dqsegdb_current_code
    #else
    #  echo "### WARNING ### /root/bin/dqsegdb_current_code files on segments-dev2.ligo.org should point to the current publisher code, but that dir does not exist."
    #  echo "            ### You'll have to find and back them up yourself."
    fi
    if [ -d /var/log/publishing/state/ ]
    then
      tar czf  $output_dir/var_log_publishing_state_${output_date}.tgz  -C /var/log/publishing/  ./state/
    else
      if [ -d /root/Publisher/var/spool/ ]
      then
        tar czf  $output_dir/root_Publisher_var_spool_${output_date}.tgz  -C /root/Publisher/var/  ./spool/
      else
        echo "### WARNING ### Publisher state files on segments-dev2.ligo.org should be in /var/log/publishing/state/, but that dir does not exist."
        echo "            ### The second option was /root/Publisher/var/spool/, but that dir does not exist."
        echo "            ### If /root/bin/state_files exists, it should point to them, but you'll have to find them and back them up yourself."
      fi
    fi
  fi
  if [ "$output_prefix" == "segments-old" ]   ###segments-old   ###old
  then
    echo "### INFO ### Backing up specific items for machine '$output_prefix'."
    machine_name_match=1
    if [ -d /root/Publisher/ ]
    then
      tar czf  $output_dir/root_Publisher_${output_date}.tgz  -C /root/  ./Publisher/
    else
      echo "### WARNING ### Publisher log files on segments-old.ligo.org should be in /root/Publisher/, but that dir does not exist."
      echo "            ### Not sure what's going on there, but you'll have to deal with it yourself."
      #echo "            ### The files *might* be in /root/Publisher/var/run/ or pointed to by /root/bin/publisher_log_files/, but you'll have to find and back them up yourself."
    fi
    if [ -d /root/bin/dqsegdb_current_code ]
    then
      tar czf  $output_dir/root_bin_dqsegdb_current_code_${output_date}.tgz  -C /root/bin/  ./dqsegdb_current_code
    #else
    #  echo "### WARNING ### /root/bin/dqsegdb_current_code files on segments.ligo.org should point to the current publisher code, but that dir does not exist."
    #  echo "            ### You'll have to find and back them up yourself."
    fi
    if [ -d /var/log/publishing/state/ ]
    then
      tar czf  $output_dir/var_log_publishing_state_${output_date}.tgz  -C /var/log/publishing/  ./state/
    else
      if [ -d /root/Publisher/var/spool/ ]
      then
        tar czf  $output_dir/root_Publisher_var_spool_${output_date}.tgz  -C /root/Publisher/var/  ./spool/
      else
        echo "### WARNING ### Publisher state files on segments.ligo.org should be in /var/log/publishing/state/, but that dir does not exist."
        echo "            ### The second option was /root/Publisher/var/spool/, but that dir does not exist."
        echo "            ### If /root/bin/state_files exists, it should point to them, but you'll have to find them and back them up yourself."
      fi
    fi
  fi
  if [ "$output_prefix" == "segments-other" ]   ###segments-other   ###other
  then
    echo "### INFO ### Backing up specific items for machine '$output_prefix'."
    machine_name_match=1
    echo "### WARNING ### No specific items have been added to this script for machine 'segments-other'.  If there is anything, you should back it up manually."
    echo "            ### You might want to look for publisher log files in /var/log/publishing/dev/, /root/Publisher/var/run/, or wherever /root/bin/publisher_log_files points."
  fi
  if [ $machine_name_match -eq 0 ]
  then
    echo "### WARNING ### The variable 'output_prefix' did not match any known machines, so no machine-specific backups were done."
    echo "            ### This might not be an error (e.g., backing up a non-segments machine), but you should make sure that's the case."
  fi
else
  if [ $verbose -eq 1 ]; then echo "### NOTICE ### Not backing up machine-specific stuff"; fi
fi

### services   ###services
### this is implemented in 'system' stuff
#echo "### INFO ### Backing up services stuff (not yet implemented)"


### history   ###history
if [ $backup_history -eq 1 ]
then
  echo "### INFO ### Backing up history for users root and ldbd (if present)"
  #history >> $output_dir/history_root_$output_date
  #sudo -u ldbd history >> $output_dir/history_ldbd_$output_date
  if [ -e /root/.bash_history ]
  then
    cp /root/.bash_history  $output_dir/root_bash_history_$output_date
  else
    echo "### WARNING ### /root/.bash_history was not found, so it isn't being backed up.  You might want to look into this."
  fi
  if [ -e /usr1/ldbd/.bash_history ]
  then
    cp /usr1/ldbd/.bash_history  $output_dir/ldbd_bash_history_$output_date
  else
    echo "### WARNING ### /usr1/ldbd/.bash_history was not found, so it isn't being backed up.  You might want to look into this."
  fi
fi

echo "### INFO ### Finishing time:  " `date`



### user verification   ###verification
if [ $user_verify -eq 1 ]
then
  echo "###"
  echo "### INFO ### Saved files should include the following:"
  if [ $backup_all_root -eq 1 ]; then echo "* all root files (tarballed)"; fi
  if [ $backup_root_bin -eq 1 ]; then echo "* /root/bin/ files (tarballed)"; fi
  if [ $backup_all_ldbd -eq 1 ]; then echo "* /usr1/ldbd/ files (tarballed) (if that dir exists)"; fi
  if [ $backup_opt_dqsegdb -eq 1 ]; then echo "* /opt/dqsegdb/ files (tarballed) (if that dir exists)"; fi
  if [ $backup_system_stuff -eq 1 ]; then echo "* system files (/etc/init.d/, /etc/fstab, /etc/profile (if it exists), /etc/default/ (tarballed), 'uname -a', 'ifconfig', 'env', 'alias', OS version, rpm/yum lists, services info, iptables info, /var/log/ (tarballed), /etc/sysconfig/ (tarballed), /usr/share/ (tarballed))"; fi
  if [ $backup_all_etc -eq 1 ]; then echo "* /etc/ dir (tarballed)"; fi
  if [ $backup_cron -eq 1 ]; then echo "* system cron files and user crontabs"; fi
  if [ $backup_http -eq 1 ]; then echo "* http files (except logs) (/etc/init.d/httpd, /etc/httpd/ (tarballed), /etc/httpd/modules/ (tarballed) )";
    echo "* more http files (/etc/sysconfig/httpd, /etc/systemd/system/multi-user.target.wants/httpd.service (?))"; fi
  if [ $backup_http_logs -eq 1 ]; then echo "* /var/log/httpd/ (tarballed)"; fi
  if [ $backup_certificates -eq 1 ]; then echo "* certificate files (*cert*, *key*, *pem*, in /etc/grid-security/) (tarballed)"; fi
  if [ $backup_grid_security -eq 1 ]; then echo "* /etc/grid-security/ (tarballed)"; fi
  if [ $backup_tls -eq 1 ]; then echo "* /etc/pki/tls/ (tarballed)"; fi
  if [ $backup_lgmm -eq 1 ]; then echo "* lgmm files: /etc/lgmm/lgmm_config.py, /etc/grid-security/grid-mapfile;  maybe /etc/grid-security/whitelist, maybe /etc/grid-security/blacklist; maybe lgmm log files (tarballed)"; fi
  if [ $backup_shib -eq 1 ]; then echo "* Shibboleth stuff: /etc/shibboleth/ (tarballed), /etc/httpd/conf.d/shib.conf, /usr/share/shibboleth/ (tarballed)"; fi
  if [ $backup_db_stuff -eq 1 ]; then echo "* database stuff: /etc/my.cnf, /etc/my.cnf.d/ (maybe) (tarballed), /etc/odbc.ini, /etc/odbcinst.ini, log files (/var/log/mysqld.log or /var/log/mariadb/), mysql variables and status; info on DBs"; fi
  if [ $backup_db_stuff -eq 1 ]; then echo "*   Note: DBs are NOT automatically backed up; you have to do that yourself (if appropriate) (maybe use /root/bin//backup_dqsegdb_mysqldatabase_directed.sh ?)"; fi
  if [ $backup_db_data_dir -eq 1 ]; then echo "* DB data dir (/var/lib/mysql/) (tarballed).  Note: this is not a DB dump; this is the whole dir."; fi
  if [ $backup_machine_specific -eq 1 ]; then echo "* Any machine-specific stuff (for $output_prefix )"; fi
  if [ $backup_history -eq 1 ]; then echo "* bash history for root and ldbd"; fi
#  echo "(more stuff)"
  echo "###"
  echo "Compare the above output to the following directory listing:  ( $output_dir )"
  ls -lthr --color=auto  $output_dir
  echo "###"
fi

exit

### Reference
# tar czf output.tgz  -C /dir1/  dir2/   ### this means change to /dir1/, then tarball dir2/, saving it as output.tgz; this way, untarballing output.tgz produces only dir2/, not dir1/dir2/
# string names can contain letters, numbers, and underscores; Bash will interpret a string name to be as long as possible; 
#   so 'ls $output_dir/root_bash_history_$output_date' unambiguously has a two string names, separated by "/root_bash_history_",
#   and 'ls $output_dir/root_bash_history_$output_date.tgz' is the same, with '.tgz' added onto the end of the second string name;
#   if there's any ambiguity or concern, enclose the string name in curly braces: 'ls $output_dir/root_bash_history_${output_date}_backup.tgz' (or Bash would think the variable was 'output_date_backup')


### Sources - places to find things that should be backed up
### * All of these should be checked on segments.ligo.org, segments-backup, segments-web, segments-dev, and segments-dev2
### * System stuff (ifconfig, /etc/fstab, etc.)
### * User stuff (/root/, /usr1/ldbd/, etc.)
### * Services, incl. the files that configure the service, the files that the service runs, output files, and log files (httpd, mariadb, dqxml_pull_from_obs, etc.)
### * Cron jobs, for root and user ldbd, incl. the files that the cron job runs, any input or configuration files that that job uses, and any output files (publishers, backups, etc.)
### * Installation script; anything that might be changed by the users; anything where we might want a reference copy (e.g., the python server in /opt/dqsegdb/, even if it wasn't changed)

### Some things that we don't back up
### * Standard installations, whose contents we don't care about (e.g., the mariadb installation itself, outside of config files and DBs)
### * DQ XML files (in /dqxml/), b/c we can always download them again (or grab them from /ifocache/DQ/)




