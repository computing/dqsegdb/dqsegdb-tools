#!/bin/bash
# Started on 2024.07.03.
# Improvements:
#   * [done] send output to non-obvious tmp files, then filter those for results to report
#   * check the latest 2 Apache log files of each type (b/c the log file might have just rolled over, so results could be missed)
#   * check server load? (unlikely to be useful, since many machines have transient high loads)
# Manual:
#   netstat -an | grep 8443 | grep -v "^unix.*DGRAM*.*8443.*" | grep -v "^unix.*STREAM*.*8443.*"

verbose=0
if [ $# -gt 0 ]; then verbose="$1"; fi
if [ "$verbose" -gt 0 ]; then date; fi

netstat_tmp="/tmp/overflow_n.txt"
netstat_tmp_filtered="/tmp/overflow_n_f.txt"
apache_tmp="/tmp/overflow_a.txt"
run_top=0

if [ "$verbose" -gt 0 ]; then echo "netstat check:"; fi
netstat -an > $netstat_tmp
cat $netstat_tmp | grep 8443 | grep -v "^unix.*DGRAM*.*8443.*" | grep -v "^unix.*STREAM*.*8443.*" > $netstat_tmp_filtered
cat $netstat_tmp_filtered
if [ $(cat $netstat_tmp_filtered | wc -l) -gt 0 ]; then run_top=1; fi
if [ "$verbose" -gt 0 ]
then
  if [ $(cat "$netstat_tmp_filtered" | wc -l) -eq 0 ]
  then echo "Netstat: no results after filters."
  fi
fi

if [ "$verbose" -gt 0 ]; then echo "/var/log/httpd/*log check:"; fi
grep -E "74\.91\.[0-9]{1,3}\.[0-9]{1,3}" /var/log/httpd/*log > $apache_tmp
cat $apache_tmp
if [ $(cat $apache_tmp | wc -l) -gt 0 ]; then run_top=1; fi
if [ "$verbose" -gt 0 ]
then
  if [ $(cat $apache_tmp | wc -l) -eq 0 ]
  then echo "Apache: no results."
  fi
fi

if [ "$run_top" -gt 0 ]; then top -b -n 1 -c -w 500 | head -n 30; fi

if [ "$verbose" -gt 0 ]; then echo "(done)"; fi

