#!/bin/bash
# This is just a quick script to check the name and date on an input cert; the idea is that the user doesn't have to remember all of the details of the openssl command, and this program can be run from a script.
# Useful: create an option in this script that searches for all ".cert", ".key", and ".pem" files on a system, excludes files in certain directories (including /usr/share/doc/), and runs the openssl command on everything else.
#   Directories to exclude: /usr/share/doc/; /etc/grid-security/certificates/
# Try these:  (note that ".cer" will match both ".cer" files and ".cert" files (as well as files like ".cert.pem", which will also match the ".pem" version)
#   for fname in `locate .cer | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" `; do check_cert.sh $fname; done
#   for fname in `locate .crt | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" `; do check_cert.sh $fname; done
#   for fname in `locate .key | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" `; do check_cert.sh $fname; done
#   for fname in `locate .pem | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" `; do check_cert.sh $fname; done
# combined:
#   for fname in `locate .cer .crt .key .pem | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" | grep -v "ca-bundle" `; do check_cert.sh $fname; done
#   for fname in `locate .cer .crt .key .pem | grep -v -e "/usr/share/doc/" -e "/etc/grid-security/certificates" -e "ca-bundle" -e "old_certs" `; do check_cert.sh $fname; done | grep -B 3 2020
#   for fname in `locate .cer .crt .key .pem | grep -v -e "/usr/share/doc/" -e "/etc/grid-security/certificates" -e "ca-bundle" -e "old_certs" `; do check_cert.sh $fname; done | grep -B 3 202 | grep -e "^" -e 202.   ### see all 202x expirations
#   for fname in `locate .cer .crt .key .pem | grep -v -e /usr/share/doc/ -e /etc/grid-security/certificates -e /etc/grid-security/old_certs/ -e ca-bundle -e "/etc/chrony.keys" -e /root/miniconda3/pkgs/ -e "/etc/trusted-key.key" -e /etc/grid-security/segments.*.ligo.org.keytab -e /etc/pki/tls/private/old_certs/ `; do check_cert.sh $fname; done
# to do:
#   * check that the file 

cert_file="$1"
echo "-----"
echo "Cert file = $cert_file"
if [ ! -f "$cert_file" ]; then echo "### ERROR ### File $cert_file does not exist.  Exiting."; exit 1; fi
openssl x509 -text -in  "$cert_file"  &> /dev/null
if [ "$?" -eq 0 ]
then
  openssl x509 -subject -dates -noout -in  "$cert_file"
else
  echo "### INFO ### File '$cert_file' is not a valid x509 certificate."
fi
#echo "-----"

exit 0


