#!/bin/bash
# A quick script to find the IP addresses that ran queries on a given date.
# Note: this is a very brittle script; maybe shore it up later.
# It checks SSL access log files (/var/log/httpd/ssl_access_log*) for the given date, extracts those lines to a temp file, finds the unique IP addresses from that log file,
#   counts the entries for each IP address, gets the host info for each IP address, and prints the count, IP adddress, and host info.
# To do:
#   * check that year is 4 digits, month matches a month string, day is 2 digits
#   * sort output by number of entries (most to fewest), not by IP address
#   * check that the date is completely contained in the log files - i.e., that the first log file doesn't start with the second half of the day
#   * [done] add option to only look at GET or PATCH or ...?


log_files=/var/log/httpd/ssl_access_log*

# check for sufficient input arguments; if too few, print usage
if [ $# -lt 3 ]
then
  echo "### ERROR ### Insufficient command-line arguments."
  echo "          ### Run like this: ' (script name)  YYYY Month DD  [optional search word; e.g., GET, PATCH, HEAD, POST, PUT, TRACE, HELP, \\\"-\\\", etc.]' "
  echo "          ### Example: ' /root/bin/httpd_logs_find_ips.sh  2021  Apr  15 ' "
  echo "          ### Example: ' /root/bin/httpd_logs_find_ips.sh  2021  Apr  15  GET ' "
  echo "          ### Note: day (DD) must be front-padded with 0s, e.g., '01'; month must be one of: Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec"
  exit 1
fi

# assign and check input values, print info for user
date
year=$1
month=$2
day=$3
if [ $# -gt 3 ]
then
  search_word=$4
  echo "### INFO ### search word = $search_word"
  temp_file=/tmp/httpd_logs_find_urls_${day}_${month}_${year}_${search_word}.txt
  search_term="${day}/${month}/${year}.*${search_word}"
else
  temp_file=/tmp/httpd_logs_find_urls_${day}_${month}_${year}.txt
  search_term="${day}/${month}/${year}"
fi
echo "### INFO ### log files = $log_files"
echo "### INFO ### temp file = $temp_file"

# search for matches in specified log files, using search term (which might be date or date + another string), assigned above
#grep -h "${day}/${month}/${year}" $log_files > $temp_file
grep -h  $search_term  $log_files  >  $temp_file

# find IP addresses in matches to search term
for ip in `awk '{print $1}' $temp_file | sort -h | uniq`   # get only IP addresses from temp file, then sort them and uniq them
do
  ip_count=`grep $ip $temp_file | wc --lines`
  host_ip=`host $ip`
  printf "%6d  %-15s   %s \n" $ip_count  $ip  "$host_ip"
done

# print commands the user might want for follow-up investigation
echo "###"
echo "to view activity by hour for a specific IP address (fill in 'xxx' below):"
echo "year=$year; month=$month; day=$day; search_word='$search_word'; log_files=$log_files ; ip=xxx"
echo "for hr in {00..24}; do hourf=\`printf \"%02.f\" \$hr\`; hour_count=\`grep \"\$ip.*\${day}/\${month}/\${year}:\${hourf}.*\${search_word}\" \$log_files | wc --lines\`; echo \$hourf  \$hour_count; done"

exit 0


### Reference
# typical line from /var/log/httpd/ssl_access_log:
# 131.215.113.205 - - [11/Apr/2021:09:13:40 -0700] "GET /dq/H1/CBC-C01C00_ANALYSIS_READY/1?s=815155213&e=875232014&include=active,known,metadata HTTP/1.1" 200 679

# grep -v -e GET -e HEAD -e PATCH -e POST -e TRACE -e HELP -e PUT /var/log/httpd/ssl_access_log | less

# for ii in {00..24}; do out_msg=`grep "${date} ${ii}:.*GET"  /opt/dqsegdb/python_server/logs/$date.log | wc --lines`; echo $ii: $out_msg; done

