#!/bin/bash
# A simple script to see if each filesystem on a list is mounted or not, and if so, if the filesystem is full (100%).
# Started by Robert Bruntz, 2022.02.10
# Note: this is an intentionally simple script, meant to be run in silent mode as a cron job, just to notify (someone) in case a filesystem is offline or full,
#   which are by far the most common filesystem problems that users run into at this point in time (often causing them confusion and loss of time and productivity).
#   This script should *not* be modified to add all sorts of bells and whistles.  There are probably better tools for that, like Nagios/Icinga plugins.
# Run: ' (script name)  (verbose level)  (filesystem list) '
# Example: ' /backup/segdb/reference/root_files/check_mounted_filesystems.sh  0 '   # verbose = 0 = quiet unless there's a problem; use default filesystem list, built into the script
# Example: ' /backup/segdb/reference/root_files/check_mounted_filesystems.sh  0  "/dqxml /ifocache /backup /backup2" '   # filesystem list in quotes and separated by spaces
# Example: ' /backup/segdb/reference/root_files/check_mounted_filesystems.sh  0  "/  /dqxml /ifocache /backup /backup2"  98 '   # filesystem list in quotes and separated by spaces; root there to watch size; warning usage = 98%
# To do:
#   * [done] maybe: add checks to see if the filesystems in the list are full, or in a fullness range?
#      * 'df /backup/segdb/ --output=pcent | tail -n 1 | cut -c1-3' returns used percentage for the given fs
#   * [done] add option to specify notification percentage on the command line
#   * find permanent-ish dirs on /home{1-N} for LHO and LLO, and create a list for each of those (see Reference section, at the end)

verbose=1
list="/  /dqxml /usr1/ldbd /ifocache /ifocache/DQ /backup /backup/segdb /backup/segdb/reference /backup2 /home/gstlalcbctest /home/stochastic /home/waveburst /home/detchar"
warn_pct=100
if [ $# -gt 0 ]; then verbose="$1"; fi   # 0 = quiet; 1 = verbose (actually, right now, anything g.t. 0 is verbose)
if [ $# -gt 1 ] && [ "$2" != "-" ]; then list="$2"; fi   # list should be in quotes, with spaces between items in the list, e.g., ' "/dqxml /ifocache /backup" ' (no single quotes)
if [ $# -gt 2 ] && [ "$3" != "-" ]; then warn_pct="$3"; fi   # optional percentage to use for check if FS is full; useful to run multiple times with different FSs and related pcts. (e.g., some are always > 98%, others, that's critical)

function check_fs {
# check if 'fs' is mounted, and if so, if it's full; note: variable 'fs' is global and set in the main part of the script
if [ "$verbose" -gt 0 ]; then echo $fs; fi
ls $fs &> /dev/null
exit_code=$?
if [ "$exit_code" -ne 0 ]
then
  if [ "$exit_code" -eq 2 ]   # exit code = 2 shows up when the filesystem isn't mounted; maybe other situations, too; if it's a problem, remove the 'not mounted' output line
  then echo "### ERROR ### Filesystem $fs is not mounted."
  else echo "### ERROR ### Filesystem $fs returned non-zero exit code $exit_code."
  fi
else
  used_pct=`df $fs --output=pcent | tail -n 1 | cut -c1-3`
  total_size=`df -h $fs | tail -n 1 | awk '{print $2}'`
  free_size=`df -h $fs | tail -n 1 | awk '{print $4}'`
  if [ "$verbose" -gt 0 ]; then echo "Used percentage = $used_pct % ($free_size available out of $total_size)"; fi
  #if [ "$used_pct" -ge "$warn_pct" ]; then echo "### WARNING ### Filesystem $fs is ${used_pct}% full, which is equal to or greater than warning level of ${warn_pct}%."; fi
  if [ "$used_pct" -ge "$warn_pct" ]; then echo "### WARNING ### Filesystem $fs is ${used_pct}% full ($free_size available out of $total_size), which is equal to or greater than warning level of ${warn_pct}%."; fi
fi
}

# main part of the script
for fs in $list
do
  check_fs
done

exit 0



### Reference
#$ df -h /home/detchar
#Filesystem               Size  Used Avail Use% Mounted on
#home6nfs:/home6/detchar  140T  139T  1.2T 100% /home/detchar
#
# filesystem info:
#   https://ldas-gridmon.ligo.caltech.edu/diskusage/
#   https://ldas-gridmon.ligo-la.caltech.edu/diskusage/
#   https://ldas-gridmon.ligo-wa.caltech.edu/diskusage/
# (open the link in a browser, then click on the 'filesystem' link (e.g., 'home4') to see a list of dirs in that filesystem)
# (to verify, list the home dir (e.g., 'll /home/pulsar'), then run mount and grep the same (e.g., 'mount | grep /home/pulsar') to see where the dir lives on disk)
# CIT:
#   /home/james.clark    = zfshome1:/home1/james.clark
#   /home/stochastic     = zfshome2:/home2/stochastic
#   /home/pycbc.offline  = zfshome3:/home3/pycbc.offline
#   /home/pulsar         = zfshome4:/home4/pulsar
#   /home/gstlalcbctest  = zfshome5:/home5/gstlalcbctest
#   /home/detchar        = zfshome4:/home4/detchar
# LHO:
#   /gds-h1/dmt          = vsmgateway:/gds-h1/dmt
#   /home/cal            = home21:/home21/cal
#   /home/idq            = home22:/home22/idq
#   /home/pycbc.live     = home31:/home31/pycbc.live
#   /home/gstlalcbc      = home32:/home32/gstlalcbc
#   /home/detchar        = home32:/home32/detchar
# (note that on the website, the home dirs are 'home{11-13,4,6}', updated 3 days before the above was assembled (2023.06.20); no idea if this is transitional, or if there is a 5th dir out there somewhere)
# LLO:
#   /gds-l1              = 10.13.5.52:/gds-l1
#   /home/detchar        = home6nfs:/home6/detchar
#   /home/duncan.macleod = home7nfs:/home7/duncan.macleod
# (note: on this filesystem, all user home dirs are on home7 (or cold1, for deactivated accounts), while all non-user home dirs are on home6)
