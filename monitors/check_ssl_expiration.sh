#!/bin/bash
# Quick script to check a list of URLs, to see if any of their SSL certs will expire within a certain amount of time.
# Started by Robert Bruntz on 2023.10.09
#msg1="Run like this: ' (script name)  (warning boundary, in days; floats OK; default = 7; use - to use default)  (old: file with list of URLs; not yet implemented; use - to use default)  (verbose level; default = 0 = quiet; 1 = verbose) ' "
msg1="Run like this: ' (script name)  (warning boundary, in days; floats OK; default = 7; use - to use default)  (new (maybe temporary): single URL to check; use - to use default)  (verbose level; default = 0 = quiet; 1 = verbose) ' "
msg2="Example: ' /root/bin/check_ssl_expiration.sh  7  -  1 ' "
msg3="Example: ' /root/bin/check_ssl_expiration.sh  7  ldas-jobs.ligo-wa.caltech.edu  1 ' "
# Note that output files (tmp_{file,error,out_date}, below) are deleted at the end when checking the standard list but not when checking a single URL - but the files will be overwritten on the next test.
# to do:
#   * create help output and set up to print that for "--help", "-h", and no arguments (but not print if even only 1 arg of "-")
#   * check that the SSL cert was gotten
#   * check if the cert *already* expired (seconds = 0?)
#   * if no cert, ping to see if the machine is reachable or not
#   * [partially done] count and report number of tests run, passed, and failed (and maybe identity of failed)
# better version (in Python):
#   * take input as file or URL
# manual test: modify the first 2 lines, then just run the other 3 lines
#   host=segments.ligo.org
#   days=7
#   seconds=`python3 -c "print(int(${days}*86400))"`
#   echo | openssl s_client -connect ${host}:443 > /tmp/check_ssl_expiration_cert.txt
#   openssl x509 -in /tmp/check_ssl_expiration_cert.txt -noout -enddate -checkend $seconds &> /dev/null; echo $?
#   # exit code = 0 -> SSL cert will *not* expire within the time specified; exit code = 1 = SSL cert *will* expire within the time specified

start_time=$(date)
days=7
verbose=0
tmp_file=/tmp/check_ssl_expiration_cert.txt
tmp_error=/tmp/check_ssl_expiration_cert_error.txt
tmp_out_date=/tmp/check_ssl_expiration_date.txt
sleep=0
test_count=0
pass_count=0
fail_count=0
error_count=0
if [ $# -eq 0 ]; then echo $msg1; echo $msg2; echo $msg3; exit 0; fi
if [ $# -gt 0 ] && [ "$1" != "-" ]; then days="$1"; fi
# check that 'days' is an unsigned integer
if [[ ! $days =~ ^[0-9]+$ ]]; then echo "### ERROR ### First argument should be an unsigned integer.:"; echo $msg1; echo $msg2; echo $msg3; exit 1; fi
#if [ $# -gt 1 ] && [ "$2" != "-" ]; then echo "### NOTICE ### Input URL files are not supported yet.  File entry ('$2') is being ignored."; fi
if [ $# -gt 2 ] && [ "$3" != "-" ]; then verbose="$3"; fi
if [ $# -gt 3 ]; then echo "### NOTICE ### Arguments beyond first 3 are ignored."; fi

seconds=`python3 -c "print(int(${days}*86400))"`
if [ "$verbose" -gt 0 ]; then echo "### Start time: $start_time ; warning boundary = $days days -> $seconds sec"; fi

host_list="\
segments.ligo.org segments-backup.ligo.org segments-web.ligo.org segments-dev.ligo.org segments-dev2.ligo.org ldas-sw.ligo.caltech.edu gwdet.icrr.u-tokyo.ac.jp \
ligo.org wiki.ligo.org git.ligo.org git-test.ligo.org git.igwn.org dcc.ligo.org dcc-backup.ligo.org dcc-lho.ligo.org dcc-llo.ligo.org dashboard.igwn.org dashboard-dev.igwn.org \
sympa.ligo.org sympa-ha.ligo.org docs.ligo.org gwosc.org remote.ligo.uwm.edu \
summary.ligo.org ldas-jobs.ligo.caltech.edu ldas-jobs.ligo-wa.caltech.edu ldas-jobs.ligo-la.caltech.edu openscience-dev.ligo.caltech.edu range.ligo.org containers.ligo.org \
dqr.ligo.caltech.edu ldvw.ligo.caltech.edu ldvw-dev.ligo.caltech.edu services1.ligo-la.caltech.edu ldas-cit.docs.ligo.org alog.ligo-la.caltech.edu alog.ligo-wa.caltech.edu \
ldas-gridmon.ligo.caltech.edu ldas-gridmon.ligo-wa.caltech.edu ldas-gridmon.ligo-la.caltech.edu accounting.ligo.org accounting-dev.ligo.org datafind-test.ligo.uwm.edu \
gracelive.igwn.org pubplan.ligo.org pnp.ligo.org roster.ligo.org subscriptions.ligo.org gracedb-dev1.ligo.org secrets.ligo.org federation-proxy.ligo.org datafind-dev.ligo.uwm.edu \
grouper.ligo.org my.ligo.org ldg.ligo.org login.ligo.org gstlal.ligo.caltech.edu online.igwn.org online.ligo.org ldrmaster.ligo.caltech.edu decommissioned.ligo.org \
fw.ego-gw.it wiki.virgo-gw.eu gwwiki.icrr.u-tokyo.ac.jp watchtower.ligo.uwm.edu sandbox.webcomm.ligo.org robots.ligo.org cilogon.org test.cilogon.org osdf.igwn.org \
ligo.gravity.cf.ac.uk jupyter.ligo.caltech.edu tds.virgo-gw.eu singularity.ligo.org galaxy.ligo.caltech.edu wbs.igwn.org gracedb.ligo.org gracedb-test.ligo.org gracedb-playground.ligo.org "
# note that dashboard-dev.igwn.org -> dashboard2-dev.ligo.uwm.edu
#
# don't check these (unless for testing) (note: decommissioned servers are listed here: https://decommissioned.ligo.org/):
# gracedb.ligo.org - hosted on AWS, so won't return a cert [actually, might be an issue that this version of openssl doesn't support TLSv1.3]
#   probably the same for gracedb-test.ligo.org and gracedb-playground.ligo.org
# ldas-cit.ligo.caltech.edu - no web server running, soon to be decommissioned
# lsc-group.phys.uwm.edu - no longer exists (ping doesn't resolve an IP address)
# monitor.ligo.org - no longer running (?)
# dashboard.ligo.org - no longer running (?)
# phys.uwm.edu - no longer exists (ping doesn't resolve an IP address)
# segments-temp - doesn't have a valid SSL cert, and probably never will
# bute2.aei.uni-hannover.de - doesn't have an SSL cert at all
# dqsegments.virgo.infn.it - doesn't have Apache running
# origin.ligo.caltech.edu, origin-ifo.ligo.caltech.edu, origin-shared.ligo.caltech.edu, origin-staging.ligo.caltech.edu - have SSL certs and Apache running, but probably protected by strict firewall
# sandbox-dev.webcomm.ligo.org - used to exist, but doesn't seem to anymore (as of 2024.01.24) (though sandbox.webcomm.ligo.org still does)
# cis.ligo.org - taken offline; maybe add back in later, when it has been replaced
# vote.ligo.org - no longer online
# vault.ligo.org - doesn't seem to want to provide its cert
# tds.ego-gw.it - old server; replaced by tds.ego-gw.eu
# kdc.ligo.org - not sure where I saw it, but it doesn't seem to be a valid URL (May 2024)
# ldap.lig.org - reports "no peer certificate available" (May 2024)
# segments-rl8.ligo.org - decommissioned

# hack to be able to check a single URL:
if [ $# -gt 1 ] && [ "$2" != "-" ]; then host_list="$2"; fi

for host in $host_list; do
  if [ "$verbose" -gt 0 ]; then echo "host = $host"; fi
  # do we get valid output XOR an error? if so, save all output to the same file; if sometimes both, use separate files; starting with the first option
#  echo | openssl s_client -connect ${host}:443 &> $tmp_file
  echo | openssl s_client -connect ${host}:443 > $tmp_file 2> $tmp_error
  exit_code1=$?
  # exit code = 0 = success; 1 = no SSL cert available; not sure what other codes mean
  let test_count++
  if [ "$exit_code1" -eq 0 ]; then
    openssl x509 -in $tmp_file -noout -enddate -checkend $seconds &> $tmp_out_date
    exit_code2=$?
    #if [ "$verbose" -gt 0 ]; then echo "   `cat $tmp_out_date`"; fi
    # exit code = 0 = SSL cert will *not* expire within the time specified; exit code = 1 = SSL cert *will* expire within the time specified
    if [ "$exit_code2" -eq 0 ]; then
      if [ "$verbose" -gt 0 ]; then echo "   OK - SSL cert for host '$host' will *not* expire within $seconds sec: `cat $tmp_out_date`"; fi
      # check for errors that don't cause a non-zero exit code (e.g., https://git.ligo.org/computing/helpdesk/-/issues/5538 )
      error_check=$(grep -i error $tmp_error | wc -l)
      if [ "$error_check" -eq 0 ]
      then
        let pass_count++
      else
        let error_count++
        echo "### ERROR ### An unknown error occurred in getting the certificate for $host :"
        grep -i error $tmp_error
        # the check of test_count is an imperfect proxy for counting how many URLs in host_list; the printed info will be wrong if the error is on the first
        #   URL of a list, since the files will be deleted at the end; maybe fix this later
        if [ "$test_count" -eq 1 ]; then echo "### INFO ### More info might be available in $tmp_error (or $tmp_file or $tmp_out_date)."; fi
      fi
    else
      if [ "$exit_code2" -eq 1 ]; then
        let fail_count++
        echo "### WARNING ### SSL cert for host '$host' *will* expire within $days days ($seconds sec): `cat $tmp_out_date`"
      else   # exit code != 0 and != 1
        let error_count++
        echo "### ERROR ### Host '$host': An unknown failure occurred in processing SSL cert.  Exit code = ${exit_code2}."
        echo "          ### Output of $tmp_file:"
        cat $tmp_file
        echo "          ### Output of $tmp_error:"
        cat $tmp_error
        echo "          ### Output of $tmp_out_date:"
        cat $tmp_out_date
      fi
    fi
  else   # exit_code1 > 0
    let error_count++
    if [ "$exit_code1" -eq 1 ] && [ `cat $tmp_file | grep 'unable to load certificate' | wc --lines` -gt 0 ]; then
      echo "### ERROR ### Host '$host': No certificate available."
    else
      echo "### ERROR ### Host '$host': openssl s_client returned exit code ${exit_code1}.  Output:"; cat $tmp_file
    fi
  fi
  sleep $sleep
  # Don't delete the output files for checking a single site, since there's a good chance it has trouble that needs to be investigated
  #   vs. the list, where the output files are just for the last site checked
  #   Note that the output files are overwritten for each site checked, so there's no danger of using more than a trivial amount of space.
  if [ "$test_count" -gt 1 ]
  then
    if [ -e "$tmp_file" ]; then rm $tmp_file; fi
    if [ -e "$tmp_error" ]; then rm $tmp_error; fi
    if [ -e "$tmp_out_date" ]; then rm $tmp_out_date; fi
  fi
done

end_time=$(date)
if [ "$verbose" -gt 0 ]
then
  echo "### End time: $end_time"
  echo; echo "Report:"
  echo "Test count  = $test_count"
  echo "Pass count  = $pass_count"
  echo "Fail count  = $fail_count"
  echo "Error count = $error_count"
  count_check=$((test_count - pass_count - fail_count - error_count))
  if [ "$count_check" -ne 0 ]; then echo "### ERROR ### Count check (test_count - pass_count - fail_count - error_count) = $count_check, but it should be 0"; fi
fi

exit 0


failed checks:

$ echo | openssl s_client -connect gracedb.ligo.org:443 2> /dev/null | openssl x509 -noout -dates
unable to load certificate
139874555193232:error:0906D06C:PEM routines:PEM_read_bio:no start line:pem_lib.c:707:Expecting: TRUSTED CERTIFICATE
[probably a result of running on AWS, not a local machine]

# echo | openssl s_client -connect ldas-cit.ligo.caltech.edu:443 
socket: Bad file descriptor
connect:errno=9
[probably has no web server running]

# echo | openssl s_client -connect monitor.ligo.org:443 
socket: Bad file descriptor
connect:errno=9
# echo $?
1
[machine is offline or decommissioned]

