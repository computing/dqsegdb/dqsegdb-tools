#!/bin/bash
# This is a script to get info on the DB that is running, esp. to spot an issue before it becomes a problem.
# Note that the nightly DB backup on segments.ligo.org was triggering the monitor, so some code had to be added to ignore those queries (as long as they're under a certain age).
# To do:
#   * [done] check that DB specified by 'db_name' exist
#   * check that hung query time is a number


# set default values and process command-line arguments
verbose=1
logging=1
#db_name=dqsegdb
db_name=`grep DATABASE /etc/odbc.ini | cut -c10-`
db_existence=-1   # -1 = couldn't find DB; 1 or higher = found DB name in DB name listing
hung_query_time=600
hung_query_count=-1   # -1 = nothing was ever changed (probably some kind of logic error); 0 or higher = actual count; -2 = mysqladmin server ping failed; -3 = couldn't find DB
db_backup_max_time=$((3*60*60))   # set this to the max number of seconds that the DB backup can run before it's considered to have hung; 2 hours is plausible; 3 hours is probably safe through 2021
gps_time=$((`date +%s` - 315964782))
human_time=$(date)
host_name=$(uname -n)
output_dir=/var/www/nagios
log_file=/var/www/nagios/db_monitor_log_file.txt

if [ $# -eq 0 ] && [ $verbose -eq 1 ]; then echo "### Usage: ' (script name)  (DB name)  (hung-query time)  (verbose flag) '"; echo "### Example: ' /root/bin/db_monitor.sh  dqsegdb  600  1 '"; fi
if [ $# -gt 0 ] && [ $1 != "-" ]; then db_name=$1; fi
if [ $# -gt 1 ] && [ $2 != "-" ]; then hung_query_time=$2; fi
if [ $# -gt 2 ] && [ $3 != "-" ]; then verbose=$3; fi

if [ $verbose -eq 1 ]; then echo "### INFO ### Variables: DB name = $db_name; hung-query time = $hung_query_time; verbose = $verbose; logging = $logging; log file = $log_file; Nagios dir = $output_dir"; fi

# check if mysqld/mariadb is running
mysqladmin ping &> /dev/null
db_status=$?   # 0 = everything is fine; anything else = trouble

# check if the DB name exists
if [ $db_status -eq 0 ]; then if [ `mysql -e "show databases;" | grep --word-regexp $db_name | wc --lines` -gt 0 ]; then db_existence=1; fi; fi   # "--word-regexp" should mean 'only match whole words' (e.g., 'dqsegdb' wouldn't match 'dqsegdb_backup')

# if both of the above check out, then check the DB status
if [ $db_status -ne 0 ]
then hung_query_count=-2; if [ $verbose -eq 1 ]; then echo "### ERROR ### mysqld (mariadb) does not seem to be running.  Try 'mysqladmin ping' and 'systemctl status mariadb' for more info."; fi
fi
if [ $db_status -eq 0 ] && [ $db_existence -ne 1 ]
then hung_query_count=-3; if [ $verbose -eq 1 ]; then echo "### ERROR ### Named DB ($db_name) does not seem to exist.  DB names:"; mysql -e "show databases;"; fi
fi
if [ $db_status -eq 0 ] && [ $db_existence -eq 1 ]
then
  if [ $verbose -eq 1 ]
  then
    echo "### INFO ### DB name = $db_name; hung_query_time = $hung_query_time"
    mysql -e "SHOW GLOBAL variables LIKE 'long_query_time';"
    mysqladmin status
    #mysqladmin proc | awk '{if ((NF > 10) && ($8 == $db_name) && ($12 >= 10)) print $0}'
    mysqladmin proc | awk -v db_name="$db_name" -v hung_query_time="$hung_query_time" '{if ((NF > 10) && ($8 == db_name) && ($12 >= hung_query_time)) print $0}'
  fi
  # handle long-running DB backup queries
  # check for long-running DB backup queries, then any long-running queries, then long-running DB backup queries again, to make sure that the number of DB queries doesn't change during the set of queries
  # note that backup queries are only counted as such (and thus don't contribute to the count of long-running queries) if they're less than 'db_backup_max_time' seconds old; after that, they *will* count
  backup_query_count1=`mysqladmin proc | awk -v db_name="$db_name" -v hung_query_time="$hung_query_time" -v db_backup_max_time="$db_backup_max_time" \
                      '{if ((NF > 10) && ($8 == db_name) && ($10 != "Sleep") && ($12 >= hung_query_time) && ($12 <= db_backup_max_time)) print $0}' | grep "INTO OUTFILE" | wc --lines`
  hung_query_count=`mysqladmin proc | awk -v db_name="$db_name" -v hung_query_time="$hung_query_time" '{if ((NF > 10) && ($8 == db_name) && ($10 != "Sleep") && ($12 >= hung_query_time)) print $0}' | wc --lines`
  backup_query_count2=`mysqladmin proc | awk -v db_name="$db_name" -v hung_query_time="$hung_query_time" -v db_backup_max_time="$db_backup_max_time" \
                      '{if ((NF > 10) && ($8 == db_name) && ($10 != "Sleep") && ($12 >= hung_query_time) && ($12 <= db_backup_max_time)) print $0}' | grep "INTO OUTFILE" | wc --lines`
  #backup_query_count2=`mysqladmin proc | awk -v db_name="$db_name" -v hung_query_time="$hung_query_time" '{if ((NF > 10) && ($8 == db_name) && ($10 != "Sleep") && ($12 >= hung_query_time)) print $0}' | grep "INTO OUTFILE" | wc --lines`
  # the following line detects if there are long-running queries, but they're *all* doing the DB backup
#  if [ $backup_query_count1 -eq $hung_query_count ] || [ $backup_query_count2 -eq $hung_query_count ]; then hung_query_count=0; fi
  # all queries in backup_query_count1 or 2 should also be counted in hung_query_count, so as long as both queries (1 and 2) produced the same number, we can subtract them from the count of long-running queries without a danger of going negative;
  #   if all of the long-running queries are for the DB backup, then the total turns out to be 0
  # if the values don't match (probably b/c of a race condition, in which a query aged past a deadline while the 3 queries were being done), then nothing is done (at this point - maybe address it if it becomes an issue)
  if [ $backup_query_count1 -eq $backup_query_count2 ]
  then hung_query_count=$((hung_query_count - backup_query_count1))
  else echo "### WARNING ### Variables backup_query_count1 and backup_query_count2 should be identical, but they're $backup_query_count1 and ${backup_query_count2}, so no value is being subtracted from variable hung_query_count ($hung_query_count)" >> $output_file
  fi
fi

# for testing
###hung_query_count=`mysqladmin proc | awk -v db_name="$db_name" -v hung_query_time="$hung_query_time" '{if ((NF > 10) && ($8 == db_name) && ($12 >= hung_query_time)) print $0}' | wc --lines`
###hung_query_count=5
if [ $hung_query_count -eq -1 ];                                then msg1="WARNING: DB monitor (db_monitor.sh) on host '$host_name' has malfunctioned and is not reporting info on the DB as of GPS time $gps_time ($human_time).";            num_status=3; fi
if [ $hung_query_count -eq -2 ];                                then msg1="WARNING: DB monitor (db_monitor.sh) on host '$host_name' cannot retrieve DB info (mysqladmin DB ping failed) as of GPS time $gps_time ($human_time).";              num_status=3; fi
if [ $hung_query_count -eq -3 ];                                then msg1="WARNING: DB monitor (db_monitor.sh) on host '$host_name' cannot find the named DB ($db_name) as of GPS time $gps_time ($human_time).";                              num_status=3; fi
if [ $hung_query_count -eq 0  ];                                then msg1="OK: DB monitor on host '$host_name' is running and reporting no issues as of GPS time $gps_time ($human_time).";                                                    num_status=0; fi
if [ $hung_query_count -gt 0  ] && [ $hung_query_count -lt 4 ]; then msg1="WARNING: DB monitor on host '$host_name' is reporting $hung_query_count long-running queries (over $hung_query_time sec) as of GPS time $gps_time ($human_time).";  num_status=1; fi
if [ $hung_query_count -ge 4  ];                                then msg1="CRITICAL: DB monitor on host '$host_name' is reporting $hung_query_count long-running queries (over $hung_query_time sec) as of GPS time $gps_time ($human_time)."; num_status=2; fi
msg2="UNKNOWN: Cannot verify if the DB monitor (db_monitor.sh) is running on host '$host_name' after GPS time $gps_time ($human_time)."
output_message="{\"created_gps\": $gps_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"$msg1\", \"num_status\": $num_status}, {\"start_sec\": 600, \"txt_status\": \"$msg2\", \"num_status\": 3}]}"
output_file=$output_dir/db_monitor.json
echo $output_message > $output_file

if [ $logging -eq 1 ] && [ $hung_query_count -ne 0 ]
then
  echo $output_message >> $log_file
  mysqladmin proc >> $log_file
fi

if [ $verbose -eq 1 ]
then
  echo $output_message
#python3 -mjson.tool < echo $output_message   # bin/db_monitor.sh: line 44: echo: No such file or directory
#python3 -mjson.tool < `echo $output_message`   # bin/db_monitor.sh: line 44: `echo $output_message`: ambiguous redirect
#echo $output_message | python3 -mjson.tool   # No JSON object could be decoded
#echo $output_message > python3 -mjson.tool   # No JSON object could be decoded
  python3 -mjson.tool $output_file
fi

exit 0


### reference
# 0 = OK; 1 = Warning; 2 = Critical; 3 = Unknown
# 'pass' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}
# 'fail' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}
# 'unknown' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}
