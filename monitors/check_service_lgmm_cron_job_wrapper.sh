#!/bin/bash
# This is a wrapper for the 'check_service_lgmm_cron_job.sh' script.
# It is shamelessly lifted from a wrapper for the 'check_service_status_for_nagios.sh' script.
# This lets us use the service name in both the regular output file and the generic log file, then scan the log file; if there is an error, copy the generic log file to a permanent name, so it isn't overwritten and can be examined later.

if [ "$#" -lt 2 ]
then
  echo "### ERROR ### This script (check_service_status_for_nagios_wrapper.sh) requires at least 2 command line arguments."
  echo "          ### Template:  /root/bin/check_service_status_for_nagios_wrapper.sh  [service_name]  [verbose_level]  [output_dir]"
  echo "          ### Example: /root/bin/check_service_status_for_nagios_wrapper.sh  httpd  1  /var/www/nagios"
  exit
fi


# Set up variables
#  default values
verbose=1
output_dir=/var/www/nagios
#  modified values
service_name=$1
if [ "$#" -ge 2 ]; then if [ $2 -eq 0 ]; then verbose=0; fi; fi
if [ "$#" -ge 3 ]; then output_dir=$3; fi
# note that we don't check for or use command-line variables above #3
status_file=$output_dir/nagios_${service_name}_status.json
log_file=$output_dir/nagios_${service_name}_status_log.txt

# Run the monitor script
### Format is: ' [script_name]  [service_name]  [verbose_level]  [output_file_name]  &>  [log_file_name]' (verbose = 0 = no printed messages; only write a Nagios/dashboard output file)
#/root/bin/check_service_status_for_nagios.sh  $service_name  $verbose  $output_dir/nagios_${service_name}_status.json  &>  $output_dir/nagios_${service_name}_status_log.txt
#/root/bin/check_service_status_for_nagios.sh  $service_name  $verbose  $status_file  &>  $log_file
# note that for check_service_lgmm_cron_job.sh, the input values are currently ignored; they might be implemented at some point; log file is useful, though
/root/bin/check_service_lgmm_cron_job.sh  $service_name  $verbose  $status_file  &>  $log_file


# Check the log file, to see if the service is running; if not, save the file in a unique name, to be analyzed later
### get status code
status_code=`awk '{loc1 = index($0, "num_status"); num_status = substr($0, (loc1 + 13), 1); print num_status}' $status_file`
if [ "$status_code" -ne 0 ]
then
### get GPS time
  gps_time=`awk '{gps_field = $2; gps_time=substr(gps_field,1,10); print gps_time}' $status_file`
#  cp  $output_dir/nagios_${service_name}_status_log.txt  $output_dir/nagios_${service_name}_status_log_${gps_time}.txt
  cp  $log_file  ${log_file}_${gps_time}.txt
fi
