#!/bin/bash
# This is a script to check on the JSON files that are created on this machine and checked by Nagios/Icinga/Dashboard.
# Started by Robert Bruntz on 2020.04.17
# To do:
#   * check for existence of JSON files before running other checks
#   * check for the right number of lines in JSON files before pulling the status code and message out of them
#   * add more debugging steps for non-0 status inside JSON files

# relevant nagios files, dirs, etc.
nagios_dir=/var/www/nagios/
filename1="verify_published_segments_H1.json"
filename2="verify_published_segments_L1.json"
filename3="verify_published_segments_V1.json"
filename4="verify_published_segments_G1.json"
filename5="verify_published_segments_K1.json"
file1=${nagios_dir}/${filename1}
file2=${nagios_dir}/${filename2}
file3=${nagios_dir}/${filename3}
file4=${nagios_dir}/${filename4}
file5=${nagios_dir}/${filename5}
# this interval changed on 2023.04.26, when the monitor was changed from running every 1 minute to every 5 minutes
#interval1=$((1*60))
interval1=$((5*60))
interval2=$interval1
interval3=$interval1
interval4=$interval1
interval5=$interval1
interval_margin=$((1*60))
cron1="* * * * * /root/bin/verify_published_segments.sh  0"
cron2="* * * * * /root/bin/verify_published_segments.sh  0"
cron3="* * * * * /root/bin/verify_published_segments.sh  0"
cron4="* * * * * /root/bin/verify_published_segments.sh  0"
cron5="* * * * * /root/bin/verify_published_segments.sh  0"
service1="H1 segments in DB match segs. in DQ XML files"
service2="L1 segments in DB match segs. in DQ XML files"
service3="V1 segments in DB match segs. in DQ XML files"
service4="G1 segments in DB match segs. in DQ XML files"
service5="K1 segments in DB match segs. in DQ XML files"

echo "$(date)  - GPS time: ~$((`date +%s` - 315964782))"

# list relevant nagios files
# samples:
#-rw-r--r-- 1 root root 378 Apr 28 01:33 /var/www/nagios//verify_published_segments_H1.json
#-rw-r--r-- 1 root root 378 Apr 28 01:33 /var/www/nagios//verify_published_segments_L1.json
echo "### Nagios file listings:"
echo "File for Nagios/Icinga/Dashboard service '$service1':"
ls -l $file1
echo "File for Nagios/Icinga/Dashboard service '$service2':"
ls -l $file2
echo "File for Nagios/Icinga/Dashboard service '$service3':"
ls -l $file3
echo "File for Nagios/Icinga/Dashboard service '$service4':"
ls -l $file4
#echo "File for Nagios/Icinga/Dashboard service '$service5':"
#ls -l $file5

# check size of relevant nagios files
echo "### Nagios file sizes:"
size1=`ls -l $file1 | awk '{print $5}'`
size2=`ls -l $file2 | awk '{print $5}'`
size3=`ls -l $file3 | awk '{print $5}'`
size4=`ls -l $file4 | awk '{print $5}'`
#size5=`ls -l $file5 | awk '{print $5}'`
#echo $size1 $size2
bad_size_count=0
if [ "$size1" -eq 0 ]; then echo "ERROR ### Nagios file $file1 is 0 bytes!"; let "bad_size_count++"; fi
if [ "$size2" -eq 0 ]; then echo "ERROR ### Nagios file $file2 is 0 bytes!"; let "bad_size_count++"; fi
if [ "$size3" -eq 0 ]; then echo "ERROR ### Nagios file $file3 is 0 bytes!"; let "bad_size_count++"; fi
if [ "$size4" -eq 0 ]; then echo "ERROR ### Nagios file $file4 is 0 bytes!"; let "bad_size_count++"; fi
#if [ "$size5" -eq 0 ]; then echo "ERROR ### Nagios file $file5 is 0 bytes!"; let "bad_size_count++"; fi
if [ "$bad_size_count" -eq 0 ]; then echo "File sizes are OK (> 0 B)."; fi

# check age of relevant nagios files
echo "### Nagios file ages:"
time1=`stat -c %Y $file1`
time2=`stat -c %Y $file2`
time3=`stat -c %Y $file3`
time4=`stat -c %Y $file4`
#time5=`stat -c %Y $file5`
time_now=$(date +%s)
#echo $time1 $time2 $time_now
age1=$((time_now - $time1))
age2=$((time_now - $time2))
age3=$((time_now - $time3))
age4=$((time_now - $time4))
#age5=$((time_now - $time5))
bad_age_count=0
if [ "$age1" -ge $((interval1 + interval_margin)) ]; then echo "ERROR ### Nagios file $file1 is over $interval1 seconds old!"; let "bad_age_count++"; fi
if [ "$age2" -ge $((interval2 + interval_margin)) ]; then echo "ERROR ### Nagios file $file2 is over $interval2 seconds old!"; let "bad_age_count++"; fi
if [ "$age3" -ge $((interval3 + interval_margin)) ]; then echo "ERROR ### Nagios file $file3 is over $interval3 seconds old!"; let "bad_age_count++"; fi
if [ "$age4" -ge $((interval4 + interval_margin)) ]; then echo "ERROR ### Nagios file $file4 is over $interval4 seconds old!"; let "bad_age_count++"; fi
#if [ "$age5" -ge $((interval5 + interval_margin)) ]; then echo "ERROR ### Nagios file $file5 is over $interval5 seconds old!"; let "bad_age_count++"; fi
if [ "$bad_age_count" -eq 0 ]; then echo "File ages are OK (< $((interval1 + interval_margin)) sec)."; fi

# get status from relevant nagios files
bad_status_count=0
status1=-1; status2=-1; status3=-1; status4=-1; status5=-1
echo "### Nagios file statuses:"
#if [ "$size1" -ne 0 ]; then
#  status1=`python3 -m json.tool < $file1 | grep num_status | head -n 1 | awk '{print $2}' | cut -c1-1`
#  status_message1=`python3 -m json.tool < $file1 | head -n 7 | tail -n 1`
#fi
#if [ "$status1" -ne 0 ] || [ "$size1" -eq 0 ]; then
#  echo "ERROR ###"
#  echo "Nagios file $file1 status:  $status1"
#  echo "   $status_message1"
#  echo "### Cron job: ' $cron1 ' "
#  let "bad_status_count++"
#fi

function eval_nagios_status() {
  if [ "$fn_size" -ne 0 ]; then
    fn_status=`python3 -m json.tool < $fn_file | grep num_status | head -n 1 | awk '{print $2}' | cut -c1-1`
    fn_status_message=`python3 -m json.tool < $fn_file | head -n 7 | tail -n 1`
  fi
  if [ "$fn_status" -ne 0 ] || [ "$fn_size" -eq 0 ]; then
    echo "ERROR ###"
    echo "Nagios file $fn_file status:  $fn_status"
    echo "   $fn_status_message"
    echo "### Cron job: ' $fn_cron ' "
    let "bad_status_count++"
  fi
}

fn_file=$file1; fn_size=$size1; fn_cron="$cron1"
eval_nagios_status 
status1="$fn_status"; status_message1="$fn_status_message"
fn_status=-1; fn_status_message=-1

fn_file=$file2; fn_size=$size2; fn_cron="$cron2"
eval_nagios_status 
status2="$fn_status"; status_message2="$fn_status_message"
fn_status=-2; fn_status_message=-2

fn_file=$file3; fn_size=$size3; fn_cron="$cron3"
eval_nagios_status 
status3="$fn_status"; status_message3="$fn_status_message"
fn_status=-3; fn_status_message=-3

fn_file=$file4; fn_size=$size4; fn_cron="$cron4"
eval_nagios_status 
status4="$fn_status"; status_message4="$fn_status_message"
fn_status=-4; fn_status_message=-4

#fn_file=$file5; fn_size=$size5; fn_cron="$cron5"
#eval_nagios_status 
#status5="$fn_status"; status_message5="$fn_status_message"
#fn_status=-5; fn_status_message=-5

# following line commented out to remove KAGRA
#if [ "$bad_status_count" -eq 0 ]; then echo "File statuses are OK ($status1, $status2, $status3, $status4, and $status5)"; fi
if [ "$bad_status_count" -eq 0 ]; then echo "File statuses are OK ($status1, $status2, $status3, and $status4)"; fi

bad_count=$((bad_size_count + bad_age_count + bad_status_count))
if [ "$bad_count" -gt 0 ]; then exit_code=1; fi
exit $bad_count
