#!/bin/bash
# A quick script to check if the mounting of any filesystems is stuck.
# Started by Robert Bruntz on 2022.07.10
# Usage: ' (script)  (verbose level; 0 = print only errors; default = 1)  (snooze duration before checking output; default = 30) '
# Example: '  /home/detchar/bin/strace_df_mounted_filesystems.sh  0  30 '   (waits 30 seconds before checking output, then only prints out info on issues)
# Example: '  /home/detchar/bin/strace_df_mounted_filesystems.sh  1  20 '   (waits 20 seconds before checking output; prints numerous potentially-useful messages)
# This script works by looping over the filesystems returned by the 'mount' command, running strace on each one and backgrounding it, with the output going to a temp file,
#   then waiting a certain amount of time ('snooze' value), then checking all of those temp files.  If any do not show "exited with 0" in the last line,
#   the filesystem either had an error or is hung.  Verbose level controls how much output is printed; if 0, output is only printed if there's an error, in which case
#   a message is printed that can be used to check the temp files again, in case something just took longer than the snooze time.
# On some systems, 20 sec is enough to clear all filesystems as OK; on some, up to 90 sec might be needed to clear a slow check.
# Note that lines have been added to ignore filesystems mounted on /tmp/ (but not /tmp itself), /run/user/, and /net/local/, b/c those tend to be short-lived issues that we don't care about.
#   Similar for /sys/kernel/debug/tracing/, which we don't have permission to read.  For /big and /fast, we don't care about those.
#   Also ignoring auto.cold, since that's storage for deactivated accounts, so issues there won't affect end users.  (Example: 'mount | grep auto.cold' -> 'auto.cold on /home/tomoki.isogai type autofs')
# to do:
# * delete all output files at the end of the run except those that had an issue? (could be difficult, since some filesystems report OK, but take longer than the snooze time)
# * add info on how to extract the name of a filesystem being reported on in a given output file


# set up some variables, including defaults
full_date=$(date +%Y.%m.%d-%H.%M.%S)
fs_count=0   # tracks how many filesystems have been checked and gives each a number
snooze=30   # seconds to sleep after last strace command has been started, to give all strace commands a fair chance to finish; 20 seconds seems to be enough
verbose=1   # 1 = print lots of output; 0 = only print output for any issues
temp_dir=/tmp/strace   # dir where files will be saved from checking each mounted filesystem; not deleted right away so that issues can be investigated
keep_days=7   # number of days worth of old files to keep in the temp dir; will delete any files named "strace*.txt" in temp dir modified more than N*24 hours ago
min_temp_size=512   # min space on temp dir for this script to run, given in MB; mainly to avoid filling up the dir
ps_max_count=10   # number of allowable results from 'ps aux | grep strace | wc -l' before a warning is printed

# get command-line args, if any
if [ $# -gt 0 ] && [ "$1" != "-" ]; then verbose="$1"; fi
if [ $# -gt 1 ] && [ "$2" != "-" ]; then snooze="$2"; fi
#echo "verbose = $verbose ; snooze = $snooze"; exit

# create temp dir, if it doesn't already exist, then check that it exists, and if not, use default temp dir
mkdir -p $temp_dir
if [ ! -d $temp_dir ]; then temp_dir=/tmp ; fi

# delete strace output files older than some age (before checking space on temp dir)
#find $temp_dir -type f -name "strace*.txt" -mtime +7 -exec ls -lh {} \; | wc
#find $temp_dir -type f -name "strace*.txt" -mtime +${keep_days} -exec rm -f {} \;
# being super paranoid, to make sure we don't delete anything we don't want to, if something goes wrong
cd $temp_dir
if [ "$PWD" == "$temp_dir" ]
then
  find . -type f -name "strace*.txt" -mtime +${keep_days} -exec rm -f ${temp_dir}/{} \;
fi

# check that there's enough space in temp dir, so that we don't risk filling it up
# -mtime +7 = files which were modified more than 7*24 hours ago
free_1k_blocks=`df -k $temp_dir | tail -n 1 | awk '{print $4}'`
# must multiply min_temp_size (in MB) by 1024 to get that size in KB
if [ "$free_1k_blocks" -lt $((min_temp_size*1024)) ]; then
  echo "### ERROR ### temp dir ($temp_dir) filesystem must have at least $min_temp_size MB = $((min_temp_size*1024)) KB free for this script to run, but it only has $free_1k_blocks KB free.  Exiting."
  exit 1
fi

# loop over all filesystems returned by 'mount', run 'strace df' on each and background it, sending output to temp files
if [ "$verbose" -gt 0 ]; then echo $full_date; fi
#for fs in `mount | grep -v "^auto.cold" | awk '{print $3}'`   # had to add filter of auto.cold here, b/c it's already removed in the 'fs' variable that is produced, which then has nothing to mark it as filter-worthy
for fs in `mount | grep -v "^auto\..*" | awk '{print $3}'`   # ignoring any 'auto.[filesystem]' entries, to avoid mounting unmounted automounted filesystems; see notes from Stuart, below
do
  if [ `echo $fs | grep /tmp/ | wc --lines` -gt 0 ]; then continue; fi   # ignore anything mounted on /tmp/; we probably don't care about it
  if [ `echo $fs | grep /run/user/ | wc --lines` -gt 0 ]; then continue; fi   # ignore anything mounted on /run/user/; we probably don't care about it
  if [ `echo $fs | grep /net/local/ | wc --lines` -gt 0 ]; then continue; fi   # ignore anything mounted on /net/local/; we probably don't care about it
  if [ `echo $fs | grep /sys/kernel/debug/tracing | wc --lines` -gt 0 ]; then continue; fi   # ignore anything mounted on /sys/kernel/debug/tracing/; we don't have permissions to read it
  if [ `echo $fs | grep "^/big" | wc --lines` -gt 0 ]; then continue; fi   # ignore anything mounted on /big; we probably don't care about it
  if [ `echo $fs | grep "^/fast" | wc --lines` -gt 0 ]; then continue; fi   # ignore anything mounted on /fast; we probably don't care about it
  if [ `echo $fs | grep "^auto.cold" | wc --lines` -gt 0 ]; then continue; fi   # ignore anything mounted on auto.cold; it's deactivated users, so we don't care about it
  if [ `echo $fs | grep "^/hdfs" | wc --lines` -gt 0 ]; then continue; fi   # ignore /hdfs because it's in the process of being deactivated
  let fs_count++
  if [ "$verbose" -gt 0 ]; then echo $fs_count , $fs; fi
  strace df $fs &> ${temp_dir}/strace_${full_date}_${fs_count}.txt &
done

if [ "$verbose" -gt 0 ]; then echo "Sleeping for $snooze seconds."; fi
sleep $snooze

# after waiting the given amount of time, check the temp files and report if any weren't OK (always - regardless of verbose level)
bad_count=0
for out_file in `ls -1 ${temp_dir}/strace_${full_date}_*txt`
do
  exec_line=`grep execve $out_file`
  tail_line=`tail -n 1 $out_file`
  if [ `grep "exited with 0" $out_file | wc --lines` -ne 1 ]
  then
    let bad_count++
    echo "-"
    echo "### Problem with this filesystem:   '${exec_line}' "
    echo "### Final line in $out_file :   '${tail_line}' "
  fi
done

# if any were bad, report the count of them and print a command for how to check on them again (useful if something was just taking a long time)
#if [ "$bad_count" -gt 0 ]; then echo "-"; echo "To re-check last lines, run this: ' for out_file in `ls -1 /tmp/strace_${full_date}_*txt`; do echo $out_file; tail -n 1 $out_file; done ' "; fi
if [ "$bad_count" -gt 0 ]
then
  echo "-   (bad count: $bad_count)"
  echo "To re-check last lines of all files, run this: ' for out_file in \`ls -1 ${temp_dir}/strace_${full_date}_*txt\`; do if [ \`tail -n 1 \$out_file | grep \"exited with 0\" | wc --lines\` -ne 1 ]; then echo \"\${out_file}: \$(tail -n 1 \$out_file)\"; fi; done ' "
fi

# check for excess strace processes, which might be left over from previous runs
ps_count=`ps aux | grep strace | grep -v "grep --color=auto strace" | wc -l`
if [ "$ps_count" -gt "$ps_max_count" ]
then
  echo "### WARNING ### The count from 'ps aux | grep strace | grep -v "grep --color=auto strace" | wc -l' is $ps_count, which is over the count that triggers a warning ($ps_max_count)."
  echo "            ### You might want to investigate and maybe kill some long-running strace processes."
fi

exit 0


# comment on auto-mounting, from Stuart Anderson, 2023.12.14 email:
# [quote]
# [root@detchar ~]# mount | grep hdfs
# auto.direct on /hdfs type autofs (rw,relatime,fd=28,pgrp=1935,timeout=300,minproto=5,maxproto=5,direct,pipe_ino=43280)
# fuse_dfs on /hdfs type fuse.fuse_dfs (rw,relatime,user_id=0,group_id=0,default_permissions,allow_other)
# The first one, ^auto.direct, indicates that if any process accesses that path then it will be automounted. The second one indicates that /hdfs is currently mounted.
# [/quote]
# note that (as of 2023.12.14) we avoid checking any lines that start with auto.* so that we don't mount automount filesystems that weren't already mounted
