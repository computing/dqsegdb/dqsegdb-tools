#!/bin/bash
# This is a script to query a random flag from a list of pre-chosen flags over a random time range from a pre-chosen time span, then output the start date and time, how long the query took, and the flag (including IFO, flag name, and flag version).
# Output can be redirected to a file, to track results.
# This is a script planned for a specific job, but it should be made to be as general as possible.
# Run like this:  ' (script name)  (flag IFO, as H1 or L1)  (server, as URL)  (query duration = span of time being queried, in seconds) '
# Example:  ' /home/detchar/dqsegdb/query_timer.sh  L1  segments-backup.ligo.org  60 '
# Example cron jobs:
# 1-59/4 * * * * /home/detchar/dqsegdb/query_timer.sh  H1  segments.ligo.org  60  >> /home/detchar/dqsegdb/query_timer_output.txt
# 3-59/4 * * * * /home/detchar/dqsegdb/query_timer.sh  L1  segments-backup.ligo.org  60  >> /home/detchar/dqsegdb/query_timer_output.txt
# Features/options to maybe add:
#   * [done] select the length of the query
#   * select the flag to be queried
#   * input 'random' to randomly pick segments or segments-backup
# To do:
#   * make IFO input more robust, e.g., accept H1, H, LHO, etc.
#   * check that inputs are valid
#   * ping the server before querying, and if offline, print only date, time, and message about ping failing
#   * confirm that the ligolw_segment_query_dqsegdb is available (and works, with --help?); if not, output message
#   * get flag_count by counting flag_pool elements
#   * 

if [ $# -lt 3 ]; then echo "Run like this:  ' (script name)  (flag IFO, as H1 or L1)  (server, as URL)  (query duration = span of time being queried, in seconds) ' "; \
   echo "Example:  ' /home/detchar/dqsegdb/query_timer.sh  L1  segments-backup.ligo.org  60 ' "; exit -1; fi

ifo=$1
server=$2
query_duration=$3
# hack to allow using internal network at CIT; note: http://segments*ldas.cit URLs will *only* work at CIT
if [ $(echo $server | grep 'ldas.cit' | wc -l) -gt 0 ]; then server_url="http://${server}"; else server_url="https://${server}"; fi

# queries will be over the time range of ER14, which is 1235750418 to 1238166018 = 2415600 sec
# note, however, that the query must start early enough that the end of it is inside the allowed time range
query_start=`shuf -i 1235750418-$((1238166018-query_duration)) -n1`
query_end=$((query_start + query_duration))

# flags are chosen to be currently used, but also populated over all of ER14, but not commonly-used enough that they're likely to be cached
# picked from: https://segments-web.ligo.org/?c=00035
# note that all flags will be version 1
#flag_pool=(DMT-DC_READOUT  DMT-INJECTION_BURST  GRD-VIOLIN_DAMPING_OK  GRD-SUS_IM2_OK  DMT-ITMY_L2_DAC_OVERFLOW  GRD-ALS_COMM_OK)
# problem with the above: GRD-VIOLIN_DAMPING_OK, DMT-ITMY_L2_DAC_OVERFLOW, and GRD-ALS_COMM_OK only exist at LHO, not LLO
# new list below, with some other candidates here: (DCH-O2_ALWAYS_ON, DCS-CALIB_FILTER_NOT_OK_C01, DMT-PRC_LOCK, DMT-XARM_LOCK
flag_pool=(DCH-HIGH_VIOLINS  DCS-UP_C01  DMT-DC_READOUT  DMT-INJECTION_BURST  GRD-ALS_YARM_OK  GRD-SUS_IM2_OK)
flag_count=6   # at some point, change this to be determined from the number of flags in the array
flag_choice=$(( $RANDOM % $flag_count ))   # note that array indices in Bash start at 0, not 1
query_flag=${ifo}:${flag_pool[$flag_choice]}:1

query_run_start_date=$(date +%Y.%m.%d)
query_run_start_time=$(date +%H.%M.%S)
# "$$" appends the process PID, so that more than one copy of the command can be run on a server at the same time without clobbering each other's temp files
time_output_file=/tmp/query_timer_time_output_${query_run_start_date}_${query_run_start_time}_$$.txt
#cmd="/usr/bin/time -f %E -o $time_output_file -- ligolw_segment_query_dqsegdb  --query-segments  --segment-url https://${server}  --include-segments $query_flag  --gps-start-time $query_start  --gps-end-time $query_end"
cmd="/usr/bin/time -f %E -o $time_output_file -- ligolw_segment_query_dqsegdb  --query-segments  --segment-url ${server_url}  --include-segments $query_flag  --gps-start-time $query_start  --gps-end-time $query_end"
#echo $cmd
error_file=/tmp/query_timer_error_${query_run_start_date}_${query_run_start_time}_$$.txt
eval $cmd  > /dev/null  2> $error_file
exit_code=$?
if [ $exit_code -eq 0 ]
then
  output_message="`cat $time_output_file`  $query_flag  $query_start $query_end"   # by including start and end, we can track the query on the server
else
  output_message="###ERROR### Query failed with exit code $exit_code; error ends with '`cat $error_file | tail -n 1`'"
fi

echo "$query_run_start_date $query_run_start_time  $output_message"

rm $time_output_file $error_file

exit $exit_code
