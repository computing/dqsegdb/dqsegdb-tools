#!/bin/bash
# Started by Robert Bruntz on 2021.03.31

# This is a script to check the status of lgmm after upgrade from 1.2.0 to 1.3.0.  
#   The upgrade turned lgmm from an init.d service into a broken systemd service, so a cron job was set up to run lgmm as a quasi-service.
#   As a result, the typical service monitoring script doesn't work for lgmm anymore.
# Much of this script was copied from /root/bin/check_service_status_for_nagios.sh .
# The cron job is something like "00 * * * * /root/bin/lgmm_cron_job.sh &>> /var/log/lgmm/lgmm.log"
#   That job runs lgmm 2x, with the default config file then a KAGRA config file.
#   Output is sent to /etc/grid-security/grid-mapfile.ligo and /etc/grid-security/grid-mapfile.kagra,
#   then those are combined into /etc/grid-security/grid-mapfile .
# This script will (eventually) check several things:
# * run checks on input to the script (location of output files, verbose level, ??)
# * check that /etc/grid-security/grid-mapfile exists, and same for LIGO and KAGRA grid-mapfiles in that dir
#   * all exist = OK
#   * 1 or more missing = CRITICAL
# * check how old each of the 3 files are
#   * < 65 min = OK (based on lgmm cron job running every 60 min)
#   * > 65 min, < 185 min = WARNING
#   * > 185 min = CRITICAL 
# * check size of each of the 3 files
#   * all > 0 B = OK
#   * 1 or more = 0 B = CRITICAL
# * use above checks to get the final status
#   * if all OK, service is OK
#   * if not OK, report where the issues are
# * send summary to generic json file for lgmm and output along the whole way to generic log file for lgmm (if there's a problem, the wrapper script will copy the log file to a permanent filename, for later analysis)
#
# to do:
# * check script input
# * check files' existence
# * check files' ages
# * check files' sizes
# * summarize results
# * write JSON file

# set up some variables
gps_time=$((`date +%s` - 315964782))
human_time=$(date)
host_name=$(uname -n)
# set up some default values
service_name=lgmm
output_dir=/var/www/nagios
#output_dir=/tmp
verbose=1
output_file=$output_dir/nagios_${service_name}_status.json
lfile=/etc/grid-security/grid-mapfile.ligo
kfile=/etc/grid-security/grid-mapfile.kagra
gfile=/etc/grid-security/grid-mapfile
status_code=1


# this is where we'd normally (and will probably eventually) evaluate command-line arguments and settings
if [ $verbose -eq 1 ]; then echo "### INFO ### Run time: $(date) - GPS time: ~$((`date +%s` - 315964782))"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Verbose level = $verbose"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Output file = $output_file"; fi


# do some checks on the files; note that this is currently just 'are all 3 files there and not empty?'; no useful info in the output, either
if [ -s $lfile ] && [ -s $kfile ] && [ -s $gfile ]   # -s = True if FILE exists and has size larger than 0
then
  status_code=0
else
  status_code=5
fi
if [ $verbose -eq 1 ]; then echo "### INFO ### status_code = $status_code"; fi


# this part lifted directly from /root/bin/check_service_status_for_nagios.sh, so it might seem a little awkward - but it should work fine
# check the status code from the check
# status codes: 0 = service running; 1 = ??; 2 = ??; 3 = service stopped/inactive; 4 = service not found
# JSON file codes: 0 = OK; 1 = Warning; 2 = Critical; 3 = Unknown
case $status_code in
0)   # this is if the service is running
  if [ $verbose -eq 1 ]; then echo "### INFO ### Executing code for status code = 0."; fi
  output_message="{\"created_gps\": $gps_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"OK: Service '$service_name' is running on host '$host_name' at GPS time $gps_time ($human_time).\", \"num_status\": 0}, \
  {\"start_sec\": 600, \"txt_status\": \"UNKNOWN: Cannot verify if service '$service_name' is running on host '$host_name' after GPS time $gps_time ($human_time).\", \"num_status\": 3}]}"
  echo "### INFO ### Output message:", $output_message
#  echo $output_message > $output_dir/nagios_${service_name}_status.json
  echo $output_message > $output_file
#  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_dir/nagios_${service_name}_status.json"; fi
  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_file"; fi
  ;;
4)   # this is if the service was not found on this machine
  if [ $verbose -eq 1 ]; then echo "### INFO ### Executing code for status code = 4."; fi
  output_message="{\"created_gps\": $gps_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"WARNING: Service '$service_name' was not found on host '$host_name' at GPS time $gps_time ($human_time).\
  This might be a configuration issue.\", \"num_status\": 1}, \
  {\"start_sec\": 600, \"txt_status\": \"UNKNOWN: Cannot verify if service '$service_name' is running on host '$host_name' after GPS time $gps_time ($human_time).\", \"num_status\": 3}]}"
  echo "### INFO ### Output message:", $output_message
#  echo $output_message > $output_dir/nagios_${service_name}_status.json
  echo $output_message > $output_file
#  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_dir/nagios_${service_name}_status.json"; fi
  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_file"; fi
  ;;
*)   # this is anything else (status_code = (not 0), 1, 2, 3, (not 4), anything else)
  if [ $verbose -eq 1 ]; then echo "### INFO ### Executing code for status code = $status_code."; fi
  output_message="{\"created_gps\": $gps_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"CRITICAL: Service '$service_name' is NOT running on host '$host_name' at GPS time $gps_time ($human_time).\", \"num_status\": 2}, \
  {\"start_sec\": 600, \"txt_status\": \"UNKNOWN: Cannot verify if service '$service_name' is running on host '$host_name' after GPS time $gps_time ($human_time).\", \"num_status\": 3}]}"
  echo "### INFO ### Output message:", $output_message
#  echo $output_message > $output_dir/nagios_${service_name}_status.json
  echo $output_message > $output_file
#  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_dir/nagios_${service_name}_status.json"; fi
  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_file"; fi
  ;;
esac


exit 0


### reference
# 0 = OK; 1 = Warning; 2 = Critical; 3 = Unknown
# 'pass' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}
# 'fail' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}
# 'unknown' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}

# from https://ldas-jobs.ligo-wa.caltech.edu/~detchar/dqsegdb/nagios/nagios-validate-H1-DMT-ANALYSIS_READY:1.json :
# "{"created_gps": 1245015027, "status_intervals":\
# [{"end_sec": 720, "txt_status": "All segments known times for H1:DMT-ANALYSIS_READY:1 in https://segments.ligo.org match raw frame data availability\nGPS Range Checked: 1245009020 - 1245012620\n[Run by detchar on detchar]", "num_status": 0},\
# {"end_sec": 1200, "start_sec": 720, "txt_status": "https://segments.ligo.org validator slow", "num_status": 3},\
# {"start_sec": 1200, "txt_status": "https://segments.ligo.org validator not running", "num_status": 3}],\
# "author": {"name": "Ryan Fisher", "email": "ryan.fisher@ligo.org"},\
# "ok_txt": "All segments regenerated from frames match those in the database",\
# "warning_txt": "Errors occurred when reading data from frames, or gaps were found in the recovered frame cache",\
# "unknown_txt": "The segment validation check is not running for some reason",\
# "critical_txt": "Mismatches were found between the known OR active segments for H1:DMT-ANALYSIS_READY:1 in https://segments.ligo.org and segments regenerated from frame data"}"
