#!/bin/bash
# This is a script to check whether a service is active or not.  Started by Robert Bruntz on 2019.06.14.
# A service is identified as active if the status code from 'systemctl [service] status' is 0; for other values, it is not active.
# Outline:
#  * set up some variables
#  * make sure that the command line had at least one variable, and that it matches a recognized service
#  * check the status of the service that was passed in
#  * write out an output file for Nagios/Dashboard
# Output options (status codes):
#    * 0 = OK = service is recognized and running
#    * 1 = Warning = service is not recognized (no status available)
#    * 2 = Critical = service is recognized but not running
#    * 3 = Unknown = the script produced an error when it ran (or has not run in some specified time period)
# A few thoughts/decisions:
#  * No log file is written; if a log file is desired, output (std out and std err) should be redirected (e.g., by appending "&> /var/www/nagios/[output_file_name]_log.txt" to the run command
#  * We don't check if a service is known/valid/etc.; if a service is unknown, it will produce a 'warning' output (since any service being checked should presumably be installed); note that typos will also produce files with 'warning' contents
# To do:
#  * don't bother checking the service name - the script should accept any service name, even if it's not a valid service (return 'unknown' or 'warning')
#  * test if output dir is writeable?
#  * test if output file was written?


# set up some variables
gps_time=$((`date +%s` - 315964782))
human_time=$(date)
host_name=$(uname -n)
# set up some default values
output_dir=/var/www/nagios
output_dir=/tmp
verbose=1

# make sure that there is at least one command-line variable (i.e., the service name; others can use default values)
if [ "$#" -lt 1 ]
then
  echo "### ERROR ### This script must be called with a service name."
  echo "          ### Format is: ' [script_name]  [service_name]  [verbose_level]  [output_file_name]' (verbose = 0 = no printed messages; only write a Nagios/dashboard output file)"
  echo "          ### Example: ' /root/bin/check_service_status_for_nagios.sh  httpd  1  /var/www/nagios/nagios_httpd_status.json '"
# write a file anyway? no - don't know what service name to write it for
  exit
else
  service_name=$1
fi

if [ "$#" -ge 2 ]
then
  if [ "$2" -eq 0 ]; then verbose=0; else verbose=1; fi   # this is for if there is a value for $2, whether it's 0, 1, or something else
else
  verbose=1   # this is for if there is no value for $2
fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Run time: $(date) - GPS time: ~$((`date +%s` - 315964782))"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Verbose level = $verbose"; fi

if [ "$#" -ge 3 ]
then
  output_file=$3
else
  output_file=$output_dir/nagios_${service_name}_status.json
fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Output file = $output_file"; fi

# note that we currently don't check for or use any command-line arguments past #3


# run the service check (without checking if the service name is valid) (common services: httpd, mariadb, shibd, lgmm, dqxml_push_to_ifocache)
if [ $verbose -eq 1 ]; then echo "### INFO ### Running: 'systemctl status $service_name &> /dev/null'"; fi
systemctl status $service_name &> /dev/null
status_code=$?
if [ $verbose -eq 1 ]; then echo "### INFO ### Returned status code = $status_code"; fi
# sometimes exit code = 0, but status is "active (exited)", rather than "active (running)" ("Active: active (exited) since [date and time; duration]"); we check to make sure that it's "active (running)" here
if [ "$status_code" -eq 0 ]; then
  if [ $verbose -eq 1 ]; then echo "### INFO ### Checking for 'active (running)' status (e.g., not 'active (exited)')"; fi
  running_count=`systemctl status $service_name | grep -c 'active (running)'`
  if [ "$running_count" -ne 1 ]; then
    if [ $verbose -eq 1 ]; then echo "### INFO ### 'systemctl status $service_name | grep -c \"active (running)\" ' returned $running_count matches, but we expected exactly 1.  Changing exit code from $status_code to -1, to signal a problem."; fi
    status_code=-1
  else
    if [ $verbose -eq 1 ]; then echo "### INFO ### 'systemctl status $service_name | grep -c \"active (running)\" ' returned exactly 1, as expected for status code 0.  Not changing anything."; fi
  fi
fi

# check the status code from the check
# status codes: 0 = service running; 1 = ??; 2 = ??; 3 = service stopped/inactive; 4 = service not found
# JSON file codes: 0 = OK; 1 = Warning; 2 = Critical; 3 = Unknown
case $status_code in
0)   # this is if the service is running
  if [ $verbose -eq 1 ]; then echo "### INFO ### Executing code for status code = 0."; fi
  output_message="{\"created_gps\": $gps_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"OK: Service '$service_name' is running on host '$host_name' at GPS time $gps_time ($human_time).\", \"num_status\": 0}, \
  {\"start_sec\": 600, \"txt_status\": \"UNKNOWN: Cannot verify if service '$service_name' is running on host '$host_name' after GPS time $gps_time ($human_time).\", \"num_status\": 3}]}"
  echo "### INFO ### Output message:", $output_message
#  echo $output_message > $output_dir/nagios_${service_name}_status.json
  echo $output_message > $output_file
#  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_dir/nagios_${service_name}_status.json"; fi
  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_file"; fi
  ;;
4)   # this is if the service was not found on this machine
  if [ $verbose -eq 1 ]; then echo "### INFO ### Executing code for status code = 4."; fi
  output_message="{\"created_gps\": $gps_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"WARNING: Service '$service_name' was not found on host '$host_name' at GPS time $gps_time ($human_time).\
  This might be a configuration issue.\", \"num_status\": 1}, \
  {\"start_sec\": 600, \"txt_status\": \"UNKNOWN: Cannot verify if service '$service_name' is running on host '$host_name' after GPS time $gps_time ($human_time).\", \"num_status\": 3}]}"
  echo "### INFO ### Output message:", $output_message
#  echo $output_message > $output_dir/nagios_${service_name}_status.json
  echo $output_message > $output_file
#  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_dir/nagios_${service_name}_status.json"; fi
  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_file"; fi
  ;;
*)   # this is anything else (status_code = (not 0), 1, 2, 3, (not 4), anything else)
  if [ $verbose -eq 1 ]; then echo "### INFO ### Executing code for status code = $status_code."; fi
  output_message="{\"created_gps\": $gps_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"CRITICAL: Service '$service_name' is NOT running on host '$host_name' at GPS time $gps_time ($human_time).\", \"num_status\": 2}, \
  {\"start_sec\": 600, \"txt_status\": \"UNKNOWN: Cannot verify if service '$service_name' is running on host '$host_name' after GPS time $gps_time ($human_time).\", \"num_status\": 3}]}"
  echo "### INFO ### Output message:", $output_message
#  echo $output_message > $output_dir/nagios_${service_name}_status.json
  echo $output_message > $output_file
#  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_dir/nagios_${service_name}_status.json"; fi
  if [ $verbose -eq 1 ]; then echo "### INFO ### Output file is: $output_file"; fi
  ;;
esac

exit



### reference
# 0 = OK; 1 = Warning; 2 = Critical; 3 = Unknown
# 'pass' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}
# 'fail' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}
# 'unknown' file template:
# {"created_gps": $gps_time, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service '$service_name' is running on host '$host_name'.", "num_status": 0}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if published segments match segments in DQ XML files for the specified time period.", "num_status": 3}]}

# from https://ldas-jobs.ligo-wa.caltech.edu/~detchar/dqsegdb/nagios/nagios-validate-H1-DMT-ANALYSIS_READY:1.json :
# "{"created_gps": 1245015027, "status_intervals":\
# [{"end_sec": 720, "txt_status": "All segments known times for H1:DMT-ANALYSIS_READY:1 in https://segments.ligo.org match raw frame data availability\nGPS Range Checked: 1245009020 - 1245012620\n[Run by detchar on detchar]", "num_status": 0},\
# {"end_sec": 1200, "start_sec": 720, "txt_status": "https://segments.ligo.org validator slow", "num_status": 3},\
# {"start_sec": 1200, "txt_status": "https://segments.ligo.org validator not running", "num_status": 3}],\
# "author": {"name": "Ryan Fisher", "email": "ryan.fisher@ligo.org"},\
# "ok_txt": "All segments regenerated from frames match those in the database",\
# "warning_txt": "Errors occurred when reading data from frames, or gaps were found in the recovered frame cache",\
# "unknown_txt": "The segment validation check is not running for some reason",\
# "critical_txt": "Mismatches were found between the known OR active segments for H1:DMT-ANALYSIS_READY:1 in https://segments.ligo.org and segments regenerated from frame data"}"

