#!/bin/bash
# A script to check Apache logs for queries from dqsegdb monitors.
msg1="This is a script to check Apache logs for queries from dqsegdb monitors.  Note that it must be run on the server whose monitors are being checked."
msg2="Usage: ' (script name)  (date string, like 30/Jul/2024)  (dqsegdb monitor string, like dqsegdb-latency-h1hoft) "
msg3="Example: ' /root/bin/check_dqsegdb_monitor_query.sh  30/Jul/2024  dqsegdb-latency-h1hoft  '"
msg4="Accepted monitor names; dqsegdb, dqsegdb-latency-{{g1,h1,k1,l1,v1}hoft, {h1,l1}dq}, dqxml-latency-{g,h,k,l,v}1, https; also: 'all' or 'any' for all traffic from D2."
msg5="Note that 'date string' can be anything, from '/', for all dates (maybe only '*' or '.') to date + time, like '30/Jul/2024:09:12:42'."
# ping4 doesn't seem to show up in Apache logs
# to do:
#   * scan all ssl_access_log* files?
#   *

#Dashboard2 IP address:
d2ip=129.89.61.46
date=$(date +%d/%b/%Y)
ssl_log=/var/log/httpd/ssl_access_log

if [ $# -lt 1 ]; then echo $msg1; echo $msg2; echo $msg3; echo $msg4; echo $msg5; exit 1; fi
if [ $# -gt 0 -a "$1" != "-" ]; then date="$1"; fi
if [ $# -gt 1 -a "$2" != "-" ]; then monitor="$2"; fi
echo "Run time: $(date +%d/%b/%Y:%H:%M:%S)"

case $monitor in
  dqsegdb)
    flag="GET /dq HTTP/1.1";;
  dqsegdb-latency-g1hoft)
    flag="G1:GEO-SCIENCE:1";;
  dqsegdb-latency-h1dq)
    flag="H1:GRD-ISC_LOCK_OK:1";;
  dqsegdb-latency-h1hoft)
    flag="H1:DMT-UP:1";;
  dqsegdb-latency-k1hoft)
    flag="K1:GRD_SCIENCE_MODE:1";;
  dqsegdb-latency-l1dq)
    flag="L1:GRD-ISC_LOCK_OK:1";;
  dqsegdb-latency-l1hoft)
    flag="L1:DMT-UP:1";;
  dqsegdb-latency-v1hoft)
    flag="V1:DQ_META_STATUS_ITF:1";;
  https)
    flag="GET / HTTP/1.0";;
  any)
    flag=".";;
  all)
    flag=".";;
#  x)
#    flag="";;
  *)
    echo "### ERROR ### Monitor string ($monitor) not recognized"; exit 2;;
esac
#echo $flag
flag=$(echo $flag | tr ':' '/')
echo "$monitor -> $flag"

grep "${d2ip}.*${date}.*${flag}" ${ssl_log}

exit 0

# Reference
in /var/log/httpd/:
ssl_request_log
[30/Jul/2024:12:10:05 -0700] 129.89.61.46 TLSv1.2 - "GET /dq/K1/GRD_SCIENCE_MODE/1?s=1405710622.0&e=1406401822&include=known HTTP/1.1" 308
ssl_access_log
129.89.61.46 - - [30/Jul/2024:09:12:42 -0700] "GET /dq/H1/DMT-UP/1?s=1405699980.0&e=1406391180&include=known HTTP/1.1" 200 164187 **56/56891746** 09:13:39

