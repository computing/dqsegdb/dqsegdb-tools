#!/bin/bash
# This is a monitor to check the output of several service monitors, to make sure that all of the services on a given machine are working.
#   Those files are probably created by check_service_status_for_nagios_wrapper.sh + check_service_status_for_nagios.sh
# Run like this: '  [script_name]  [verbose_level]  [output_file]  &>  [log_file]  '
# Example:       '  /root/bin/service_monitor_monitor.sh  1  /var/www/nagios/nagios_service_monitor.json  &>  /var/www/nagios/nagios_service_monitor_log.txt  '
# Currently, all service monitors run every 1 minute, as does this script, so this script will sleep for 10 seconds, then check those monitors.
# Outline:
# * For each monitor that should be active on a machine, check the GPS time and status of the current service monitor file (e.g., /var/www/magios/nagios_httpd_status.json)
# * If the GPS time of the monitor file is within 3.5 (?) minutes of the time that this monitor runs *and* its status is OK, it's listed as OK
# * If the GPS time of the monitor file is more than 3.5 (?) minutes of the time that this monitor runs *or* its status is *not* OK, it's listed as not OK (Warning, Critical, or Unknown)
# * After all monitors have been checked, this monitor is OK if *all* monitors are OK; if *any* are not OK, this monitor is not OK (taking the highest (first) from this list: Critical, Warning, Unknown)
# * Generate an output file; give this monitor's status, then list each status level and which services (if any) are at that level
# Some assumptions and decisions
# * We assume that the status file's GPS time is same as or before this script's GPS time; since the latter is *now*, this should be reliable, but malformed files could violate that.  If it becomes a problem, we'll deal with it.
# To do:
#   * (probably not) Check that the status file's GPS time is same as or earlier than this script's run's GPS time.  (Not likely to ever be a problem.  Maybe if our GPS estimation is off by 1 sec and sleep=0...)
#   * (probably not) Modify code to not start a list of services with a space (b/c list = list + " " + (service name being added to that list) ) (this is a very minor formatting issue; not worth much/any time


# set variable values
#   machine-specific monitor list
#monitor_list="httpd mariadb"
monitor_list="httpd mariadb lgmm"
#monitor_list="httpd mariadb lgmm shib shibd decoy"
#   default values
host_name=$(uname -n)
verbose=1
sleep_duration=10
human_time=$(date)
gps_run_time=$((`date +%s` - 315964782))   # this is the GPS time for the run of this monitor
gps_margin=$((3*60 + 30))
monitor_dir=/var/www/nagios
output_file=$monitor_dir/nagios_service_monitor.json
ok_list=""
warning_list=""
critical_list=""
unknown_list=""
#   values (maybe) set by command-line arguments
if [ "$#" -ge 1 ]; then if [ "$1" -eq 0 ]; then verbose=0; fi; fi   # if the cmd-line arg is there and is 0, set verbose to 0; if no arg or any other value, keep verbose at 1
if [ "$#" -ge 2 ]; then output_file=$2; fi
#if [ "$#" -ge 2 ]; then monitor_dir=$2; fi
# print some setup info
if [ $verbose -eq 1 ]; then echo "### INFO ### Run time: $(date) - GPS time: ~$((`date +%s` - 315964782))"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Verbose = $verbose"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Services (status files) to be checked: $monitor_list"; fi


# sleep, so that the other monitors can finish running
if [ $verbose -eq 1 ]; then echo "### INFO ### Sleeping for $sleep_duration seconds, to allow the service monitor scripts time to run."; fi
sleep $sleep_duration
gps_run_time=$((`date +%s` - 315964782))   # this is the GPS time for the run of this monitor
if [ $verbose -eq 1 ]; then echo "### INFO ### Run time: $(date) - GPS time: ~$((`date +%s` - 315964782))"; fi


# check each monitor
for service_name in $monitor_list; do
  if [ $verbose -eq 1 ]; then echo "### INFO ### Checking service '$service_name'"; fi
  status_file=$monitor_dir/nagios_${service_name}_status.json
  if [ $verbose -eq 1 ]; then echo "### INFO ### Status file = $status_file"; fi
  if [ ! -e $status_file ]
  then
    if [ $verbose -eq 1 ]; then echo "### INFO ### The status file ($status_file) was not found.  The service ($service_name) is being categorized as 'Unknown'."; fi
    unknown_list=${unknown_list}" "${service_name}
    continue   # this will skip to the next iteration of the loop (if any; else, exit the loop)
  fi
  monitor_gps_time=`awk '{gps_field = $2; gps_time=substr(gps_field,1,10); print gps_time}' $status_file`
  if [ $verbose -eq 1 ]; then echo "### INFO ### GPS time for $service_name = $monitor_gps_time"; fi
  status_code=`awk '{loc1 = index($0, "num_status"); num_status = substr($0, (loc1 + 13), 1); print num_status}' $status_file`
  if [ $verbose -eq 1 ]; then echo "### INFO ### Status code for $service_name = $status_code"; fi
  if [ $(($gps_run_time - $monitor_gps_time)) -lt $gps_margin ]
  then
    if [ $verbose -eq 1 ]; then echo "### INFO ### Last monitor file was $(($gps_run_time - $monitor_gps_time)) seconds ago, which is less than the gps margin of $gps_margin seconds; analyzing status."; fi
    if [ $status_code -eq 0 ]; then ok_list=${ok_list}" "${service_name};             if [ $verbose -eq 1 ]; then echo "### INFO ### Service $service_name added to the 'OK' list."; fi; fi
    if [ $status_code -eq 1 ]; then warning_list=${warning_list}" "${service_name};   if [ $verbose -eq 1 ]; then echo "### INFO ### Service $service_name added to the 'Warning' list."; fi; fi
    if [ $status_code -eq 2 ]; then critical_list=${critical_list}" "${service_name}; if [ $verbose -eq 1 ]; then echo "### INFO ### Service $service_name added to the 'Critical' list."; fi; fi
    if [ $status_code -eq 3 ]; then unknown_list=${unknown_list}" "${service_name};   if [ $verbose -eq 1 ]; then echo "### INFO ### Service $service_name added to the 'Unknown' list."; fi; fi
  else
    if [ $verbose -eq 1 ]; then echo "### INFO ### Last monitor file was $(($gps_run_time - $monitor_gps_time)) seconds ago, which is more than the gps margin of $gps_margin seconds; the status will not be analyzed."; fi
    unknown_list=${unknown_list}" "${service_name}   # if the difference in the GPS times is out of the margin, it's 'unknown'
    if [ $verbose -eq 1 ]; then echo "### INFO ### This service ($service_name) is being categorized as 'Unknown'."; fi
  fi
done

# Analyze results and write an output file
# we start with a default result of 'OK', then change it if there is a more-serious status anywhere
monitor_status="OK"; monitor_status_code=0; if [ "$ok_list" == "" ]; then ok_list="(none)"; fi
if [ "$unknown_list" != "" ]; then monitor_status="Unknown";   monitor_status_code=3; else unknown_list="(none)"; fi
if [ "$warning_list" != "" ]; then monitor_status="Warning";   monitor_status_code=1; else warning_list="(none)"; fi
if [ "$critical_list" != "" ]; then monitor_status="Critical"; monitor_status_code=2; else critical_list="(none)"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Contents of 'OK' list:  '$ok_list'"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Contents of 'Warning' list:  '$warning_list'"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Contents of 'Critical' list:  '$critical_list'"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Contents of 'Unknown' list:  '$unknown_list'"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Final status:  $monitor_status"; fi
# create the message for the JSON file
output_message="{\"created_gps\": $gps_run_time, \"status_intervals\": [{\"start_sec\": 0, \"end_sec\": 600, \"txt_status\": \"$monitor_status: Services checked on host '$host_name' at GPS time $gps_run_time ($human_time): '$monitor_list'.  \
Results: Critical: '$critical_list'; Warning: '$warning_list'; Unknown: '$unknown_list'; OK: '$ok_list'\", \"num_status\": $monitor_status_code}, \
{\"start_sec\": 600, \"txt_status\": \"UNKNOWN: Cannot verify if these services ('$service_list') are running on host '$host_name' after GPS time $gps_run_time ($human_time).\", \"num_status\": 3}]}"
if [ $verbose -eq 1 ]; then echo "### INFO ### Output message:"; echo $output_message; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Output file: $output_file"; fi
echo $output_message > $output_file

exit

if [ $verbose -eq 1 ]; then echo "### INFO ### "; fi

### Reference
# typical 'OK' status file:
# {"created_gps": 1246226959, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service 'httpd' is running on host 'segments-dev2' at GPS time 1246226959 (Wed Jul 3 15:09:01 PDT 2019).", "num_status": 0}, \
{"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if service 'httpd' is running on host 'segments-dev2' after GPS time 1246226959 (Wed Jul 3 15:09:01 PDT 2019).", "num_status": 3}]}
# typical 'Warning' status file:
# {"created_gps": 1246219917, "status_intervals": [{"start_sec": 0, "txt_status": "WARNING: Service 'shib' was not found on host 'segments-dev2' at GPS time 1246219917 (Wed Jul 3 13:11:39 PDT 2019). This might be a configuration issue.", \
"num_status": 1}, {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if service 'shib' is running on host 'segments-dev2' after GPS time 1246219917 (Wed Jul 3 13:11:39 PDT 2019).", "num_status": 3}]}
# typical 'Critical' status file:
# {"created_gps": 1246219862, "status_intervals": [{"start_sec": 0, "txt_status": "CRITICAL: Service 'shibd' is NOT running on host 'segments-dev2' at GPS time 1246219862 (Wed Jul 3 13:10:44 PDT 2019).", "num_status": 2}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if service 'shibd' is running on host 'segments-dev2' after GPS time 1246219862 (Wed Jul 3 13:10:44 PDT 2019).", "num_status": 3}]}
