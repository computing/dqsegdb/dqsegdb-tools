#!/bin/bash
# This is a simple script to check the number of tcp and udp connections, using netstat.
# Started on 2023.09.26 by Robert Bruntz.
# Run like this: ' (script)  (optional: sleep interval, in seconds; default = 5) '
#   Example: ' /root/bin/monitor_netstat.sh  10  &>>  /root/netstat_output.txt  & '
# Output is sent to std out, so to save output, redirect it, e.g., ' (script) &>> /root/netstat_output.txt '
# Ouptut is probably in the format ' (count of https (port 443) connnections)  (count of http (port 80) connections)  (count of all connections reported by netstat (excluding output header lines))  (formatted date, as YYYY.MM.DD-HH.MM.SS) '
#   Example: '  5    0   20  2023.09.26-08.35.39 '
# Note that there are likely numerous connections on ports other than 443 and 80 (as seen in the example above).  To explore those, comment out the line that deletes the output file ('rm $output_file').
# to do:
#   * check that netstat is installed on the system
#   * add an option to run only N iterations
#   * 


sleep_val=5   # in sec
if [ $# -gt 0 ]; then sleep_val=$1; fi
if [ $# -gt 1 ]; then echo "### Warning ### Script runs like ' (script)  (optional: sleep interval, in seconds; default = 5) '; extra arguments are ignored."; fi
if [ "$sleep_val" -lt 1 ]; then echo "### ERROR ### Sleep time must be 1 second or higher.  Current value is $sleep_val.  Exiting."; exit 1; fi


while [ true ]
do
  # send output to a file and then scan that, to avoid the chance of output changing over several successive runs of netstat (and the overhead of same)
  fdate=$(date +%Y.%m.%d-%H.%M.%S)
  output_file=/tmp/netstat_monitor_${fdate}.txt
  netstat_cmd="netstat -tupn > $output_file"
  #netstat_cmd="netstat -tun > $output_file"
    # -t = --tcp
    # -u = --udp
    # -p = --program = "Show the PID and name of the program to which each socket belongs."
    # -n = --numeric = "Show numerical addresses instead of trying to determine symbolic host, port or user names."
  # line below is for testing/debugging; comment out when not needed
  #echo "sleep val = $sleep_val ; output file = $output_file ; formatted date = $fdate ; netstat cmd =' $netstat_cmd'"
  eval $netstat_cmd
  exit_code=$?
  if [ "$exit_code" -eq 0 ]; then
    count_443=`grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}:443' $output_file | wc -l`
    count_80=`grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}:80' $output_file | wc -l`
    count_all=$((`cat $output_file | wc -l` - 2))   ### -2 b/c of header lines
    # line below is for testing/debugging; comment out when not needed
    #echo "443: $count_443 ; 80: $count_80 ; all: $count_all"
    printf "%3d  %3d  %3d  %s\n" $count_443 $count_80 $count_all $fdate
  else
    echo "### ERROR ### netstat command ('$netstat_cmd') failed with error code $exit_code."
  fi
  rm $output_file
  sleep $sleep_val
done

exit 0


# while [ true ]; do count_443=`netstat -tupn | grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}:443' | wc -l`; count_80=`netstat -tupn | grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}:80' | wc -l`; printf "%3d  " $count_443 $count_80; date; sleep 5; done &>> /root/netstat_monitor.txt &

