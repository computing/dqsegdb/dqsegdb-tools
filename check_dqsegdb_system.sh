#!/bin/bash
# Initially a very quick and dirty script to check if various files that should on a segments* server exist do, to check systems installed by Puppet.
# Script has been expanded to check a lot of things on segments* servers, to help diagnose, predict, or even prevent issues.
# The goal is to make this a scrpit to run often, as a diagnostic tool.
# Ordering is pretty arbitrary, except that Puppet checks are at the end, because they tend to take several seconds to complete.
# Lots of plans and ideas are noted in the script but not implemented yet.  Idea: mark not-yet-implemented things with '#here' or '# here', to find them easily.
# Started by Robert Bruntz in Feb. or March 2024
# Run like this: ' (script name)  (optional verbose level; default = 1 = verbose; 0 = print only errors/issues) '
# verbose = 1 is useful to see info on a lot of stuff on the system
# verbose = 0 is useful to see only errors/problems
# note that not all items comply with verbose = 0 yet; some print the same messages/output either way
# to do:
#   * make each section a function, then run them in sequence at the end (easier to turn sections on and off, for testing, and to remove sections that are having trouble, as well as keep track of all of the sections)
#   * modify dir_should_exist to check if arg is a file
#   * modify file_should_exist to check if arg is a dir
#   * collect items from Puppet installation files that should be checked
#   * collect items from post-Puppet script that should be checked
#     * to check: segments.pp, apache.pp  cvmfs.pp  lgmm.pp  mariadb.pp  packages.pp  segments_backup.pp  segments_dev2.pp  segments_dev.pp  segments.pp  segments_web.pp  seg_server.pp  symlinks.pp
#     * already checked: web_server.pp
#   * on seg, check if gap-filling has been done recently (see #dqxml in DQSegDB Admin)


failed_checks=0
passed_checks=0
run_checks=0
verbose=1   # 0 = print only errors; 1 = print summaries, even if OK; 2 = print verbose output whether OK or not
gps_time=$((`date +%s` - 315964782))
gps_day=`echo $gps_time | cut -c1-5`
gps_daytime=`echo $gps_time | cut -c6-10`
server=`hostname`

if [ "$#" -gt 0 ]; then verbose="$1"; fi

alias ll='ls -lh --color=auto'
alias ll='ls -l --color=auto'
function ll () {
  ls -lh --color=auto $*
}

if [ "$verbose" -gt 0 ]
then
  if [ "$#" -eq 0 ]
  then
    echo "### INFO ### Run like this: ' (script name) (verbose level; default = 1) '; e.g., ' /root/bin/check_dqsegdb_system.sh 0 '"
    echo "         ### Verbose levels: 0 = print only errors; 1 = print summaries, even if OK; 2 = print verbose output whether OK or not"
    exit 0
  fi
  echo "$(date)  - GPS time: ~$gps_time"
  echo "### test"
  ll /nothing
 fi

# function listing:
# check_group - check that the group of a file or dir matches expectations
# check_owner - check that the owner of a file or dir matches expectations
# check_permissions - check that the permissions of a file or dir match expectations
# check_string_count - check a file for a specified number of instances of a string
# dir_should_exist - check if a dir exists; print an error if it doesn't; print a message if it does and verbose > 0
# file_should_be_link - check if a file (or dir?) exists, and if so, is a link, and if so, make sure the link is not broken and that it points to the right file (optional)
# file_should_exist - check if a file exists; print an error if it doesn't; print a message if it does and verbose > 0
# get_dir_permissions - get the permissions of a dir, as a string
# test_this_cmd - function to test a command and evaluate the exit code
# verbose_echo - function to print a string if verbose level is equal to or greater than a specified level

function check_group () {
  # check that the group of a file or dir matches expectations
  # call like this: 'check_group /local/ root'
  # check that the file or dir actually exists
  if [ ! -e "$1" ]; then echo "### ERROR ### File or dir $1 does not exist."; return 1; fi
  # get group of file or dir; output of ls command should look like this: 'drwxrwxrwx 10 root root 10 Jun 27 19:17 /local/'
  fn_group=$(ls -ld $1 | awk '{print $4}')
  if [ "$fn_group" != "$2" ]
  then
    echo "### ERROR ### Group for $1 is '$fn_group', but it was expected to be '$2'"
    return 2
  else
    verbose_echo 1 "### INFO ### Group for $1 is '$fn_group', as expected"
    return 0
  fi
}

function check_owner () {
  # check that the owner of a file or dir matches expectations
  # call like this: 'check_owner /local/ root'
  # check that the file or dir actually exists
  if [ ! -e "$1" ]; then echo "### ERROR ### File or dir $1 does not exist."; return 1; fi
  # get owner of file or dir; output of ls command should look like this: 'drwxrwxrwx 10 root root 10 Jun 27 19:17 /local/'
  fn_owner=$(ls -ld $1 | awk '{print $3}')
  if [ "$fn_owner" != "$2" ]
  then
    echo "### ERROR ### Owner for $1 is '$fn_owner', but it was expected to be '$2'"
    return 2
  else
    verbose_echo 1 "### INFO ### Owner for $1 is '$fn_owner', as expected"
    return 0
  fi
}

function check_permissions () {
  # check that the permissions of a file or dir match expectations
  # call like this: 'check_permissions /local/ drwxrwxrwx'
  # check that the file or dir actually exists
  if [ ! -e "$1" ]; then echo "### ERROR ### File or dir $1 does not exist."; return 1; fi
  # get permissions; output of ls command should look like this: 'drwxrwxrwx 10 root root 10 Jun 27 19:17 /local/'
  perm=$(ls -ld $1 | awk '{print $1}')
  if [ "$perm" != "$2" ]
  then
    echo "### ERROR ### Permissions for $1 are '$perm', but they were expected to be '$2'"
    return 2
  else
    verbose_echo 1 "### INFO ### Permissions for $1 are '$perm', as expected"
    return 0
  fi
}

function check_string_count () {
  # check a file for a specified number of instances of a string
  # if the counts match and verbose > 0, print a message; if the counts don't match, print a message and maybe a listing of instances
  # note that this will detect even commented-out instances of a string (thus the printing of matches if the counts don't match)
  #   you can probably search for instances at the start of a line by including "^" at the start of the search, but it won't detect the string if there's leading whitespace
  # run like 'check_string_count (file to check) (string to look for) (expected count of string)'
  # returns the grep count, not 0 for success or something other than 0 for failure
  grep_file="$1"
  grep_string="$2"
  expected_count="$3"
  grep_count=`grep "$grep_string" $grep_file | wc -l`
  if [ "$grep_count" -ne "$expected_count" ]
  then
    echo "### ERROR ### File $grep_file should contain $expected_count instance(s) of the string '$grep_string', but it actually has $grep_count instance(s)."
    if [ "$grep_count" -gt 0 ]; then echo "          ### Matching instances:"; grep "$grep_string" $grep_file; fi
  else
    if [ "$verbose" -gt 0 ]
    then
      echo "### INFO ### File $grep_file has $grep_count instance(s) of the string '$grep_string', as expected."
    fi
  fi
  return $grep_count
}

function dir_should_exist () {
  # check if a dir exists; print an error if it doesn't; print a message if it does and verbose > 0
  # not sure how this will handle broken link to dir
  dir="$1"
  if [ ! -d "$dir" ]
  then
    echo "### ERROR ### Dir '$dir' should exist, but it doesn't."
    return 1
  else
    if [ "$verbose" -gt 0 ]
    then
      echo "### INFO ### Dir '$dir' exists, as expected."
    fi
    return 0
  fi
}

function file_should_be_link () {
  # check if a file (or dir?) exists, and if so, is a link, and if so, make sure the link is not broken and that it points to the right file (optional)
  # run like '(cmd) (link file) (link target)'  (using other terminology: '(cmd) (LINK_NAME) (TARGET)')
  # e.g., ' file_should_be_link  /root/.pythonrc  /root/git/dqsegdb-private/root_.pythonrc '
  # note that this is *opposite* how links are created (e.g., 'ln -s /etc/real_file  /etc/symlink_file')
  link_file="$1"
  # reference:
  # -e = file, good link, or dir exists; broken link fails
  # -h = good or broken link passes; file or dir fails
  # make sure file exists
  if [ -e "$link_file"  -o  -h "$link_file" ]
  then verbose_echo 1 "### INFO ### File or dir $link_file exists, as expected."
  else echo "### ERROR ### File or dir $link_file does not exist."; return 1
  fi
  # make sure file is a link
  if [ -h "$link_file" ]
  then verbose_echo 1 "### INFO ### File or dir $link_file is a symlink, as expected."
  else echo "### ERROR ### File or dir $link_file was expected to be a symlink but is actually a regular file or dir."; ls -ld --color=auto "$link_file"; return 2
  fi
  # make sure link is not broken
  if [ -e "$link_file"  -a  -h "$link_file" ]
  then verbose_echo 1 "### INFO ### File or dir $link_file is a good symlink, as expected."
  else echo "### ERROR ### File or dir $link_file is a symlink, as expected, but it is a broken symlink."; ls -ld --color=auto "$link_file"; return 3
  fi
  # make sure link points to the right file (optional)
  if [ $# -gt 1 ]
  then
    expected_target="$2"
    real_target=$(realpath $link_file)
    if [ "$real_target" == "$expected_target" ]
    then verbose_echo 1 "### INFO ### File or dir $link_file is a valid link to $expected_target, as expected."
    else echo "### ERROR ### File or dir $link_file is a valid link but was expected to point to $expected_target, not $real_target."; ls -ld --color=auto "$link_file"; return 4
    fi
  fi
  return 0
}

function file_should_exist () {
  # check if a file exists; print an error if it doesn't; print a message if it does and verbose > 0
  # note that a broken link is counted as a file existing; checking for broken links is a different thing
  file="$1"
  # have to check for failure of both -e (file exists, but fails if it's a broken link) and -L (file is a link, broken or not)
  if [ ! -e "$file" ] && [ ! -L "$file" ]
  then
    echo "### ERROR ### File '$file' should exist, but it doesn't."
    return 1
  else
    if [ "$verbose" -gt 0 ]
    then
      echo "### INFO ### File '$file' exists, as expected."
    fi
    return 0
  fi
}

function get_dir_permissions () {
  # get the permissions of a dir, as a string
  # call like this: 'get_dir_permissions /local/'
  #   or maybe like this: dir_perm=$(get_dir_permissions /local/)
  # check that dir actually exists
  if [ ! -d "$1" ]; then echo "### ERROR ### Dir $1 does not exist."; return 1; fi
  # print out permissions; output of ls command should look like this: 'drwxrwxrwx 10 root root 10 Jun 27 19:17 /local/'
  ls -ld $1 | awk '{print $1}'
  return 0
}

function test_this_cmd () {
  # function to test a command and evaluate the exit code
  # global variable 'test_cmd' is expected to be set before calling this function
  # (maybe modify it to pass the command in as an argument to the function at some point)
  # (maybe provide a way to determine whether std out and std err are printed or redirected to /dev/null at some point)
  if [ "$verbose" -gt 0 ]; then echo "### Testing command '$test_cmd'"; fi
  if [ "$verbose" -gt 0 ]
  then eval "$test_cmd"
  else eval "$test_cmd" 1> /tmp/test_this_cmd_std_out.txt 2> /tmp/test_this_cmd_std_err.txt
  fi
  exit_code=$?
  if [ "$exit_code" -ne 0 ]
  then
    echo "### ERROR ### '$test_cmd' returned exit code $exit_code"
  else
    if [ "$verbose" -gt 0 ]; then echo "### OK ### '$test_cmd' returned exit code $exit_code"; fi
  fi
  return "$exit_code"
}

function verbose_echo () {
  # function to print a string if verbose level is equal to or greater than a specified level
  # global variable 'verbose' is expected to be set before callign this function
  # should be called like 'verose_echo 1 (string) '
  if [ "$verbose" -ge "$1" ]; then echo "$2"; fi
}


# test function file_should_be_link ; must be after function verbose_echo is declared
if [ 1 -eq 0 ]
then
touch /tmp/empty.txt
mkdir -p /tmp/blank_dir
ln -sf /tmp/empty.txt  /tmp/good_link
ln -sf /tmp/missing_file  /tmp/broken_link
file_should_be_link /tmp/empty.txt
file_should_be_link /tmp/blank_dir
file_should_be_link /tmp/good_link
file_should_be_link /tmp/broken_link
file_should_be_link /tmp/no_file.txt
file_should_be_link /tmp/good_link /tmp/empty.txt
file_should_be_link /tmp/good_link /tmp/wrong_target.txt
file_should_be_link /var/spool/cron/root  /root/git/dqsegdb-private/cron/$(hostname)_root_rl8
fi


### files and dirs exist
verbose_echo 1 "### Check that misc. files and dirs exist and have expected properties"
file_should_exist /etc/sudoers.d/admins
file_should_exist /etc/sudoers.d/managers
file_should_exist /etc/sudoers.d/nrpe
file_should_exist /etc/sudoers.d/zfs
#file_should_exist /etc/sudoers.d/segments   # only on seg
dir_should_exist /local/
exit_code=$?
if [ "$exit_code" -eq 0 ]
then
  check_permissions /local/ drwxrwxrwx
  check_owner /local/ root
  check_group /local/ root
fi
dir_should_exist /root/git/
dir_should_exist /root/git/dqsegdb-private
dir_should_exist /root/git/dqsegdb_puppet_repo
dir_should_exist /root/git/dqsegdb-tools
dir_should_exist /root/git/server
if [ "$server" == "segments-web" ]
then
  dir_should_exist /root/git/web
  dir_should_exist /root/log
  file_should_exist /root/bin/backup_dqsegdb_web.sh
  file_should_exist /root/bin/run_backup_dqsegdb_web.sh
  file_should_exist /root/bin/backup_dqsegdb_web_new.sh
  file_should_exist /root/bin/run_backup_dqsegdb_web_new.sh
  file_should_exist /root/bin/backup_dqsegdb_web_downloads.sh
  file_should_exist /root/bin/run_backup_dqsegdb_web_downloads.sh
  file_should_exist /root/bin/web_files
  file_should_exist /root/bin/json_payloads
  file_should_exist /root/bin/check_for_checkbox_issue.sh
  file_should_exist /usr/share/dqsegdb_web/classes
  file_should_exist /usr/share/dqsegdb_web/css
  file_should_exist /usr/share/dqsegdb_web/images
  file_should_exist /usr/share/dqsegdb_web/index.php
  file_should_exist /usr/share/dqsegdb_web/init
  file_should_exist /usr/share/dqsegdb_web/python_utilities
  file_should_exist /usr/share/dqsegdb_web/scripts
  dir_should_exist /usr/share/dqsegdb
  dir_should_exist /usr/share/dqsegdb/web
  dir_should_exist /usr/share/dqsegdb/web/test
  dir_should_exist /usr/share/dqsegdb/web/test/downloads
  dir_should_exist /usr/share/dqsegdb/web/test/plots
  dir_should_exist /usr/share/dqsegdb/web/test/classes
  dir_should_exist /usr/share/dqsegdb/web/test/css
  file_should_exist /usr/share/dqsegdb/web/test/index.php
  dir_should_exist /usr/share/dqsegdb/web/test/init
  dir_should_exist /usr/share/dqsegdb/web/test/python_utilities
  dir_should_exist /usr/share/dqsegdb/web/test/resources
  dir_should_exist /usr/share/dqsegdb/web/test/scripts
fi
if [ "$server" == "segments" ]
then
  dir_should_exist /root/git/client
  dir_should_exist /root/git/glue
  dir_should_exist /root/git/monitors
  dir_should_exist /root/git/web

fi


### services
# maybe also check if a service is enabled or disabled?
function check_service () {
  service="$1"
  #echo "service = $service"
  check_text=`systemctl status $service`
  # minor race condition b/c we have to run this again, b/c the output in check_text is seen as all 1 line by grep
  check_text_filtered=`systemctl status $service | grep "Active: active (running)"`
  exit_code=$?
  check_count=`echo $check_text | grep "Active: active (running)" | wc --lines`
  if [ "$check_count" -ne 1 ]
  then echo "### ERROR ### Service $service does not seem to be running.  (Try ' systemctl status $service '.)"; let failed_checks++
  else
    if [ "$verbose" -gt 0 ]; then echo "### INFO ### Service $service is running. ('$check_text_filtered')"; fi
    if [ "$exit_code" -gt 0 ]; then echo "### ERROR ### Exit code for 'systemctl status $service' is $exit_code, which might indicate a problem."; fi
    if [ `echo $check_text | grep -i -e error -e fail | wc -l` -gt 0 ]
    then
      echo "### ERROR ### Output of 'systemctl status $service' reports 1 or more instances of 'error' or 'fail', which might indicate a problem."
      echo "          ### Relevant lines:"
      systemctl status $service | grep -i -e error -e fail
    fi
  fi
}
verbose_echo 1 "### services"
#if [ "$verbose" -gt 0 ]; then echo "### services"; fi
#if [ "$verbose" -gt 0 ]; then echo "### Apache"; fi
# old method:
#echo "Apache:"
#systemctl status httpd | grep "Active:"
check_service httpd
check_service mariadb
check_service crond
if [ "$server" == "segments" ]; then check_service dqxml_push_to_ifocache; fi
# firewall should be disabled -> exit_code = 1
systemctl is-enabled firewalld &> /dev/null
exit_code=$?
let run_checks++
if [ "$exit_code" -ne 1 ]
then
  let failed_checks++
  echo "### ERROR ### Firewall should be disabled, but it is not ('systemctl is-enabled firewalld' returns `systemctl is-enabled firewalld`)"
else
  let passed_checks++
  if [ "$verbose" -gt 0 ]; then echo "### INFO ### Firewall is disabled, as it should be.  ('systemctl is-enabled firewalld' returns `systemctl is-enabled firewalld`)"; fi
fi

echo "### NOTICE ### Most things below this line are not yet affected by verbose level setting."


### crontabs
verbose_echo 1 "### crontabs"
# all machines should have user root; all except -web should have ldbd; only seg and -backup should have segdb (though others might, too)
file_should_be_link  /var/spool/cron/root  /root/git/dqsegdb-private/cron/$(hostname)_root_rl8
if [ "$server" != "segments-web" ]; then file_should_exist /var/spool/cron/ldbd; fi
if [ "$server" == "segments"  -o  "$server" == "segments-backup" ]; then file_should_exist /var/spool/cron/segdb; fi
if [ -e /var/spool/cron/ldbd ]; then file_should_be_link  /var/spool/cron/ldbd  /root/git/dqsegdb-private/cron/$(hostname)_ldbd_rl8; fi
if [ -e /var/spool/cron/segdb ]; then file_should_be_link  /var/spool/cron/segdb  /root/git/dqsegdb-private/cron/$(hostname)_segdb_rl8; fi
cd /var/spool/cron
# check for unexpected users; ignore files like /var/spool/cron/root_2024.09.16.bak
for user in `ls -1 | grep -v -e root -e ldbd -e segdb -e root*bak`
do
echo "### WARNING ### Unexpected user in /var/spool/cron/: $user"
ls -l --color=auto /var/spool/cron/$user
done


### apache
### httpd
verbose_echo 1  "### Apache"
# x.509, x509, X.509, X509
dir_should_exist /etc/httpd/x509-certs/
file_should_exist /etc/httpd/x509-certs/segments*.ligo.org.cert
file_should_exist /etc/httpd/x509-certs/segments*.ligo.org.key
file_should_exist /etc/httpd/x509-certs/segments*.ligo.org.pem
file_should_exist /etc/httpd/x509-certs/${server}.ligo.org.cert
file_should_exist /etc/httpd/x509-certs/${server}.ligo.org.key
file_should_exist /etc/httpd/x509-certs/${server}.ligo.org.pem
#here - check that the above files are non-zero bytes
#here - check that the above files (.pem and .cert?) are valid files
#here - check that the above files (.pem and .cert?) are valid by date
#here - check that above pem and key match each other
pem_file=/etc/httpd/x509-certs/${server}.ligo.org.pem
if [ -s $pem_file ]
then
  # check if SSL file is currently valid; redirect output for tests since we only want exit code
  if openssl x509 -checkend 0 -noout -in $pem_file > /dev/null
  then
    verbose_echo 1 "X.509 (SSL) cert (PEM file $pem_file) is currently valid."
    month_sec=$((30*86400))
    if openssl x509 -checkend $month_sec -noout -in $pem_file > /dev/null
    then
      verbose_echo 1 "X.509 (SSL) cert (PEM file $pem_file) is valid for at least 30 more days."
    else
      echo "### WARNING ### X.509 (SSL) cert (PEM file $pem_file) will expire in less than 30 days."
      openssl x509 -subject -dates -noout -in $pem_file
    fi
  else
    echo "### ERROR ### X.509 (SSL) cert (PEM file $pem_file) is *not* currently valid."
    openssl x509 -subject -dates -noout -in $pem_file
  fi
fi
# Apache (httpd) configuration
file_should_exist /etc/httpd/conf/httpd.conf
file_should_exist /etc/httpd/conf.d/autoindex.conf
file_should_exist /etc/httpd/conf.d/dqsegdb.conf
file_should_exist /etc/httpd/conf.d/manual.conf
file_should_exist /etc/httpd/conf.d/notrace.conf
file_should_exist /etc/httpd/conf.d/php.conf
file_should_exist /etc/httpd/conf.d/README
file_should_exist /etc/httpd/conf.d/ssl.conf
file_should_exist /etc/httpd/conf.d/userdir.conf
file_should_exist /etc/httpd/conf.d/welcome.conf
if [ "$server" != "segments-web" ]; then file_should_exist /etc/httpd/conf.d/wsgi.conf ; fi
file_should_exist /etc/httpd/conf.modules.d/00-mpm.conf
file_should_exist /etc/httpd/conf.modules.d/10-wsgi-python3.conf
file_should_exist /etc/sysconfig/httpd
# previously, SSLCertificateFile was set in dqsegdb.conf and pointed to /etc/pki/tls/certs/localhost.crt ; now, it's in ssl.conf and points to /etc/httpd/x509-certs/segments.ligo.org.pem (et al.)
#file_should_exist /etc/pki/tls/certs/localhost.crt
# previously, SSLCertificateKeyFile was set in dqsegdb.conf and pointed to /etc/pki/tls/private/localhost.key ; now, it's set in ssl.conf and points to /etc/httpd/x509-certs/segments.ligo.org.key (et al.)
#file_should_exist /etc/pki/tls/private/localhost.key
# here - check that the above files are non-zero bytes (?)
# here - check for essential lines in the above files
dir_should_exist /etc/grid-security/certificates
echo "### NOTICE ### Various Apache files are checked for existence, but not yet checked for specific contents."


### mariadb
# to do: add check for specific DBs on specific servers
if [ "$verbose" -gt 0 ]; then echo "### MariaDB"; mysql -e "show databases;"; fi
# check for mysql.sock file
let run_checks++
if [ -e /var/lib/mysql/mysql.sock ]
then
  let passed_checks++
  verbose_echo 1 "### INFO ### MariaDB socket file (/var/lib/mysql/mysql.sock) exists, as expected."
#  if [ "$verbose" -gt 0 ]; then echo "### INFO ### MariaDB socket file (/var/lib/mysql/mysql.sock) exists, as expected."; fi
else
  let failed_checks++
  echo "### ERROR ### MariaDB socket file (/var/lib/mysql/mysql.sock) does not exist."
fi
# check for existence of my.cnf, and if present, check contents
let run_checks++
# note: -e will fail for a link that exists but is broken; -L will pass for a broken link
if [ ! -e /etc/my.cnf ] && [ ! -L /etc/my.cnf ]
then
  let failed_checks++
  echo "### ERROR ### File /etc/my.cnf does not exist."
else
  # below here are only for if my.cnf exists
  let passed_checks++
  verbose_echo 1 "### INFO ### File /etc/my.cnf exists, as expected."
  # check if my.cnf is a link, as expected
  if [ ! -L /etc/my.cnf ]
  then
    echo "### ERROR ### File /etc/my.cnf should be a link but is a regular file."
    ls -l /etc/my.cnf
  else
    verbose_echo 1 "### INFO ### File /etc/my.cnf is a symlink, as expected."
  fi
  # check that my.cnf is not an empty file
  # -s = file exists and is larger than 0 bytes
  if [ ! -s /etc/my.cnf ]
  then
    echo "### ERROR ### File /etc/my.cnf is 0 bytes, but it should have content in it."
  else
    verbose_echo 1 "### INFO ### File /etc/my.cnf has content, as expected."
    # check that sql_mode is exactly as expected
    expected_sql_mode_line="sql_mode = 'ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'"
    if [ "`grep sql_mode /etc/my.cnf`" != "$expected_sql_mode_line" ]
#"sql_mode = 'ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'" ]
    then
      echo "### ERROR ### Check sql_mode in /etc/my.cnf.  Should be '$expected_sql_mode_line' '"
#'sql_mode = 'ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' '"
      echo "          ### Actual value: `grep sql_mode /etc/my.cnf`"
    else
      verbose_echo 1 "### INFO ### File /etc/my.cnf has expected sql_mode: ' $expected_sql_mode_line '"
    fi
    # if verbose not 0, print non-commented lines with innodb_buffer_pool_size (if more than one present, last one is used)
    if [ "$verbose" -gt 0 ]; then echo "### INFO ### From file /etc/my.cnf: ' $(grep innodb_buffer_pool_size /etc/my.cnf | grep -v ^#) '"; fi
    # check that innodb_buffer_pool_size is big enough
    ibps=`grep innodb_buffer_pool_size /etc/my.cnf | grep -v "^#" | tail -n 1 | grep -oE " [[:digit:]]+G" | grep -oE [[:digit:]]+`   # for "innodb_buffer_pool_size = 80G", should return "80"
    total_ram=`free -gt | tail -n 1 | awk '{print $2}'`
    if [ "$ibps" -gt $((total_ram * 70/100)) ]
    then echo "### WARNING ### innodb_buffer_pool_size is set to $ibps GB, but it should be less than 70% of free RAM, and total RAM is $total_ram GB.  This will likely cause trouble (like 'ERROR 2013 (HY000)' and 'ERROR 2002 (HY000)')."
    else verbose_echo 1 "### INFO ### innodb_buffer_pool_size is set to $ibps GB, which is be less than 70% of free RAM, as it should be.  (Total RAM = $total_ram GB.  70% = $((total_ram * 70/100)) GB.)"
    fi
  fi
fi
echo "### NOTICE ### Several other aspects of /etc/my.cnf should be checked but are not yet checked (e.g., max_connections value, symbolic-links value, etc.)"
echo "### NOTICE ### Various aspects of MariaDB are checked, but specific DBs on specific servers are not yet checked."
# odbc.ini
file_should_exist /etc/odbc.ini
file_should_be_link /etc/odbc.ini
if [ -s /etc/odbc.ini ]
then
  expected_driver="MariaDB"
  # count uncommented DRIVER lines; > 1 is an error, but not fatal; last DRIVER line will be used
  if [ $(grep ^DRIVER /etc/odbc.ini | wc -l) -gt 1 ]; then echo "### ERROR ### /etc/odbc.ini should have exactly 1 uncommented 'DRIVER' line, but it has more than 1:"; grep ^DRIVER /etc/odbc.ini; fi
  if [ $(grep ^DRIVER /etc/odbc.ini | wc -l) -lt 1 ]
  then
    echo "### ERROR ### /etc/odbc.ini should only exactly 1 uncommented 'DRIVER' line, but it has 0.  This will probably cause errors."
  else   # DRIVER count >= 1
    driver_val=$(grep "^DRIVER" /etc/odbc.ini | tail -n 1 | awk 'BEGIN{FS="="} {print $2}')   # for 'DRIVER=MariaDB' should return 'MariaDB'
    if [ "$driver_val" == "$expected_driver" ]
    then
      verbose_echo 1 "### INFO ### DRIVER in /etc/odbc.ini = $expected_driver , as expected."
      if [ "$verbose" -gt 0 ]; then echo "         ### $(grep ^DRIVER /etc/odbc.ini | tail -n 1)"; fi
    else
      echo "### ERROR ### DRIVER in /etc/odbc.ini should be $expected_driver , but is actually $driver_val .  This may cause trouble."
      grep "^DRIVER" /etc/odbc.ini | tail -n 1
    fi
  fi
  #here # also check for "[DQSEGDB]" line? DATABASE line? USER line?
  if [ "$server" == "segments-backup" ]
  then
    file_should_exist /root/bin/flip_odbc_target.sh
    file_should_exist /etc/sudoers.d/flip_odbc
  fi
fi
echo "### NOTICE ### Several other aspects of /etc/odbc.ini should be checked but are not checked yet ('[DQSEGDB]' line, DATABASE line, maybe USER line)"


### PHP
verbose_echo 1 "### PHP"
php --version &> /dev/null
if [ $? -eq 0 ]
then
  if [ "$verbose" -gt 0 ]; then echo "PHP version:"; php --version; fi
  if [ $(grep LoadModule /etc/httpd/conf.modules.d/00-mpm.conf | grep -v "^#" | wc -l) -eq 1  -a  $(grep LoadModule /etc/httpd/conf.modules.d/00-mpm.conf | grep -v "^#" | grep mpm_prefork | wc -l) -eq 1 ]
  then
    if [ "$verbose" -gt 0 ]; then echo "MPM module setup looks good: from /etc/httpd/conf.modules.d/00-mpm.conf, should be mpm_prefork:"; grep LoadModule /etc/httpd/conf.modules.d/00-mpm.conf | grep -v "^#" | grep mpm_prefork; fi
  else
    echo "### ERROR ### /etc/httpd/conf.modules.d/00-mpm.conf should have exactly one uncommented LoadModule line, and it should be for mpm_prefork.  Actual results:"
    grep LoadModule /etc/httpd/conf.modules.d/00-mpm.conf | grep -v "^#"
  fi
else
  echo "### ERROR ### PHP is not installed ('php --version &> /dev/null' returned non-zero exit code)"
fi


### phpMyAdmin
verbose_echo 1 "### phpMyAdmin"
if [ -s /usr/share/phpmyadmin/config.inc.php ]
then
  if [ "$verbose" -gt 0 ]
  then
    echo "phpMyAdmin is installed (/usr/share/phpmyadmin/config.inc.php exists)"
    echo "Version: $(grep "<title>.*phpMyAdmin .* documentation</title>" /usr/share/php?y?dmin/doc/html/index.html 2> /dev/null | grep -o "phpMyAdmin .* documentation" | awk '{print $2}')"
  fi
else
  echo "### phpMyAdmin not installed (/usr/share/phpmyadmin/config.inc.php doesn't exist)."
fi
# phpmyadmin should be expected and reported if not found
# check various items here: https://git.ligo.org/computing/dqsegdb/server/-/issues/65#note_1049040


### LGMM
verbose_echo 1 "### LGMM"
ll -d /etc/grid-security/certificates/
ll /etc/grid-security/grid-mapfile
if [ "$server" != "segments-web" ] && [ "$server" != "segments-web-old" ]
then ll /etc/grid-security/grid-mapfile-insert
fi
ll /etc/grid-security/grid-mapfile.kagra
ll /etc/grid-security/grid-mapfile.ligo
ll /etc/grid-security/robot_proxy_cert.pem
ll /etc/grid-security/segments*keytab
ll /etc/grid-security/whitelist
grep 'ldaps://ldap.gw-astronomy.cilogon.org' /etc/lgmm/lgmm_config_kagra.py


### Shibboleth
if [ `echo $server | grep "segments-web" | wc -l` -gt 0 ]
then
  verbose_echo 1 "### Shibboleth"
  file_should_exist /etc/httpd/conf.d/shib.conf
  file_should_exist /usr/lib64/shibboleth/mod_shib_24.so
  file_should_exist /usr/share/shibboleth/main.css
  dir_should_exist /etc/shibboleth
fi


### root
verbose_echo 1 "### root"
dir_should_exist /root/bin/
dir_should_exist  /root/crontab_backup_segments*/
dir_should_exist  /root/git/
dir_should_exist  /root/proxy_cert_logs/
#ll /root/bin/


verbose_echo 1 "### ldbd"
#if [ "$server" != "segments-web"  -a  "$server" != "segments-web-old" ]
#then
  dir_should_exist  /local/ldbd/
  if [ "$server" == "segments"  -o  "$server" == "segments-backup" ]
  then
    dir_should_exist  /local/ldbd/git/
    dir_should_exist  /local/ldbd/git/server/
  fi
  #ll /local/ldbd/
#fi


echo "### misc. files"
#if [ "$server" != "segments-web" ]
#then
  if [ `grep ldbd /etc/security/access.conf | wc -l` -lt 1 ]
  then echo "### ERROR ### File /etc/security/access.conf does not contain user ldbd."
  else if [ "$verbose" -gt 0 ]; then echo "### INFO ### Confirmed: File /etc/security/access.conf *does* contain user ldbd:"; grep ldbd /etc/security/access.conf; fi
  fi
  if [ `grep segdb /etc/security/access.conf | wc -l` -lt 1 ]
  then echo "### ERROR ### File /etc/security/access.conf does not contain user segdb."
  else if [ "$verbose" -gt 0 ]; then echo "### INFO ### Confirmed: File /etc/security/access.conf *does* contain user segdb:"; grep segdb /etc/security/access.conf; fi
  fi
#fi


### abrt
abrt_file="/etc/abrt/abrt-action-save-package-data.conf"
file_should_exist $abrt_file
exit_code=$?
if [ "$exit_code" -eq 0 ]
then
  check_string_count $abrt_file "OpenGPGCheck = no" 1
  check_string_count $abrt_file "OpenGPGCheck = yes" 0
  check_string_count $abrt_file "ProcessUnpackaged = no" 0
  check_string_count $abrt_file "ProcessUnpackaged = yes" 1
fi


### mounted filesystems
if [ "$verbose" -gt 0 ]; then echo "### mounted filesystems ###"; fi
backup_dir="/backup/segdb/reference/"
for filesystem_dir in /backup/segdb/reference/ /ifocache/DQ/
do
  let run_checks++
  if [ ! -d $filesystem_dir ]
  then
    let failed_checks++
    echo "### ERROR ### Could not access mounted filesystem $filesystem_dir ."
  else
    let passed_checks++
    if [ "$verbose" -gt 0 ]; then echo "### INFO ### Successfully accessed mounted filesystem $filesystem_dir , as expected."; fi
    if [ "$filesystem_dir" == "/ifocache/DQ/" ]
    then
      ### ifocache dirs
      if [ "$verbose" -gt 0 ]; then echo "### ifocache DQ dirs ###"; fi
      # don't want to check for current ifocache dir in the first hour of the day, since it might not be there yet; maybe tune this time span down a bit later, if useful
      # gps_day = 1st 5 digits of current (when script started) GPS time (gps_time) = metric day; gps_daytime = last (i.e., 2nd) 5 digits of gps_time = second of metric day
      if [ "$gps_daytime" -lt 3600 ]; then gps_day2=$((gps_day - 1)); else gps_day2=$gps_day; fi
      for ifo in H L V G
      do
        let run_checks++
        ifocache_dir="/ifocache/DQ/${ifo}1/${ifo}-DQ_Segments-${gps_day2}/"
        if [ ! -d $ifocache_dir ]
        then
          let failed_checks++
          echo "### ERROR ### Missing current ifocache dir for IFO ${ifo}1 (i.e., no dir $ifocache_dir)."
        else
          let passed_checks++
          if [ "$verbose" -gt 0 ]; then echo "### INFO ### Current ifocache dir for IFO ${ifo}1 (i.e., $ifocache_dir) exists, as expected."; fi
        fi
      done
    fi
  fi
done


echo "server_name = `hostname -f`"
echo "### internal network:"
echo "from ifconfig:  `ifconfig | grep 10.14`"
echo "from /etc/httpd/conf.d/dqsegdb.conf:"
grep 10.14 /etc/httpd/conf.d/dqsegdb.conf
echo "### external network:"
echo "from ifconfig:  `ifconfig | grep 131.215`"
echo "from /etc/httpd/conf.d/ssl.conf:"
grep 131.215 /etc/httpd/conf.d/ssl.conf


# check state files
# check that each state file (H,L,V,G) exists (note: no K yet, maybe ever), is non-zero, and is a valid XML file; latter might be approximated at first by size or last line
# [ -f FILE ]   True if FILE exists and is a regular file.
# [ -s FILE ]   True if FILE exists and has size larger than 0
if [ "$verbose" -gt 0 ]; then echo "### INFO ### Checking state files"; fi
for host in segments segments-backup segments-dev segments-old segments-backup-old segments-dev-old
do
  if [ "$host" == "$server" ]
  then
    #for ifo in H L V G K
    for ifo in H L V G
    do
      file=/root/bin/state_files/${ifo}-DQ_Segments_current.xml
      if [ ! -f "$file" ]; then echo "### ERROR ### State file $file does not exist.";
      else
	if [ ! -s "$file" ]; then echo "### ERROR ### State file $file exists but is 0 bytes.";
        else
          if [ `tail -n 1 $file` != "</LIGO_LW>" ]
          then echo "### ERROR ### State file $file is probably corrupt.  Last line:  '`tail -n 1 $file`'"
          else if [ "$verbose" -gt 0 ]; then echo "State file $file seems to be OK."; fi
          fi
	fi
      fi
    done
  fi
done


# check for broken links
if [ "$verbose" -gt 0 ]; then echo "### INFO ### Checking for broken links"; fi
function check_for_broken_links () {
  # note: 'find' checks a dir and all subdirs, so no need for explicitly checking subdirs
  check_dir="$1"
  broken_count=`find "$check_dir" -xtype l 2> /dev/null | wc -l`
  let run_checks++
  if [ "$broken_count" -gt 0 ]
  then
    let failed_checks++
    echo "### ERROR ### Broken links:"
    find "$check_dir" -xtype l
  else
    let passed_checks++
  fi
}
# note: 'find' checks a dir and all subdirs, so no need for explicitly checking subdirs
if [ "$server" == "segments-web"  -o  "$server" == "segments-web-old" ]
then
  path_list="/root/bin /etc /var/spool/cron /usr/share/dqsegdb_web"
else
  path_list="/root/bin /etc /var/spool/cron /local/ldbd/bin"
fi
for path in $path_list
do
  check_for_broken_links "$path"
done


### check that Puppet runs would work
if [ "$verbose" -gt 0 ]; then echo "### Testing Puppet commands"; fi
test_cmd="/root/local_puppet.sh  --noop"
test_this_cmd
test_cmd="puppet agent -t --noop"
test_this_cmd


#dir_should_exist "/root/bin/"
#dir_should_exist "/root/bin2/"
#dir_should_exist "/tmp/broken_dir_link"
#file_should_exist "/etc/fstab"
#file_should_exist "/etc/fstab2"
#file_should_exist "/etc/odbc.ini"
#file_should_exist "/tmp/broken_link"

# packages that we would like to make sure they're installed at some point
# mysqladmin git nano mlocate screen telnet symlinks tree wget lsof zip unzip abrt time
# "dqsegdb >= 2.0.0" ecp-cookie-init ciecp-utils httpd mod_ssl ca-certificates
# php-json php-mbstring
# osg-ca-certs igtf-ca-certs
# python3-mod_wsgi "python3-scitokens >= 1.7.4" python3-pytest htgettoken
# mariadb-server mariadb mariadb-devel mytop php php-mysqlnd python3-pyodbc python3-m2crypto
# phpmyadmin ligo-grid-mapfile-manager ligo-proxy-utils strace
# shibboleth
# cvmfs cvmfs-config-osg cvmfs-x509-helper
# something with iptables; maybe make sure minimum settings for each segments* machine?

exit 0

