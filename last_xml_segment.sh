#!/bin/bash
# A quick script to return the GPS times of the last 'known' segment in a segment XML file, like a KAGRA segment XML file, state file, etc.
# Started by Robert Bruntz on 2024.07.26.
msg1="A script to return the GPS times of the last 'known' segment(s) in a segment XML file, state file, etc.  Checks for 'segment_definer:segment_def_id' and uses 'ltconvert'."
msg2="  Note: this doesn't work on HLV DQXML files (yet?)."
msg3="Run like this: ' (script name)  (XML file)  (number of lines to report on [only partially implemented]) ' "
msg4="Example: ' /root/bin/last_xml_segment.sh  /dqxml/K1_test/K1-GRD_SCIENCE_MODE/2024/K1-GRD_SCIENCE_MODE_SEGMENT_UTC_2024-07-24.xml ' "
msg5="Example: ' /home/robert.bruntz/bin/last_xml_segment.sh  /mnt/segments/K1-GRD_SCIENCE_MODE/2024/K1-GRD_SCIENCE_MODE_SEGMENT_UTC_2024-07-19.xml ' "
msg6="Example: ' /root/bin/last_xml_segment.sh  /root/bin/state_files/V-DQ_Segments_current.xml  10 ' "
msg7="Doesn't work: ' /root/bin/last_xml_segment.sh  /dqxml/H1/H-DQ_Segments-14060/H-DQ_Segments-1406000000-16.xml ' "
msg8="Doesn't work: ' /root/bin/last_xml_segment.sh  /dqxml/L1/L-DQ_Segments-14060/L-DQ_Segments-1406000000-16.xml  4 ' "
# to do:
#   * check that 'number of lines to report on' is an integer
#   * improve main routine to run 'ltconvert A B' on the pairs of GPS times in a line, to get the duration, as well
#   * check that there is/are at least 1/'number of lines to report on' lines with 'segment_definer:segment_def_id' in it/them
#   * figure out how/if it can work on regular DQXML files (HLVG)
#   * add an option to print the line with the segments in it


# set up variables
check_line_count=1
if [ $# -eq 0 ]; then echo $msg1; echo $msg2; echo $msg3; echo $msg4; echo $msg5; echo $msg6; echo $msg7; echo $msg8; exit 1; fi
if [ $# -gt 0 ]; then check_file="$1"; fi
if [ $# -gt 1 -a "$2" != "-" ]; then check_line_count="$2"; fi

# do checks on varibles, etc.
# -f = file exists and is a regular file; -s = file exists and is > 0 bytes
if [ ! -f "$check_file" ]; then echo "### Error ### File to check ($check_file) does not exist.  Exiting."; exit 2; fi
if [ ! -s "$check_file" ]; then echo "### Error ### File to check ($check_file) is 0 bytes.  Exiting."; ls -l $check_file; exit 3; fi
  which ltconvert &> /dev/null
  ltc_exit_code=$?
  if [ "$ltc_exit_code" -ne 0 ]
  then
    echo "### NOTICE ### The command 'ltconvert' was not found in PATH, so the GPS times will be reported but not converted to human time."
  fi

# determine if old or new ligolw format file
old_count=$(grep "<Column.*Name=.*process_id" $check_file | grep 'Type="ilwd:char"' | wc -l)
new_count=$(grep "<Column.*Name=.*process_id" $check_file | grep 'Type="int_8s"' | wc -l)
#echo "old count = $old_count; new count = $new_count"
if [ "$old_count" -eq 0  -a  "$new_count" -eq 0 ]
then
  echo "### ERROR ### Couldn't determine whether file $check_file is old or new ligolw format."
  echo "          ### Could not find 'Type=\"ilwd:char\"' or 'Type=\"int_8s\"' in lines with '<Column.*Name=.*process_id'."
  exit 4
fi
if [ "$old_count" -gt 0  -a  "$new_count" -gt 0 ]
then
  echo "### ERROR ### Couldn't determine whether file $check_file is old or new ligolw format."
  echo "          ### Found both 'Type=\"ilwd:char\"' (count = $old_count) *and* 'Type=\"int_8s\"' (count = $new_count) in lines with '<Column.*Name=.*process_id'."
  exit 5
fi
if [ "$old_count" -gt 0 ]; then file_type="old"; fi
if [ "$new_count" -gt 0 ]; then file_type="new"; fi
if [ "$file_type" != "old"  -a  "$file_type" != "new" ]; then echo "### ERROR ### File type should be 'old' or 'new', but it is '$file_type'.  Something very unexpected went wrong."; exit 6; fi
echo "file type = $file_type"

# check file structure
table_start_count=$(grep "<Table" $check_file | wc -l)
table_end_count=$(grep "</Table" $check_file | wc -l)
stream_start_count=$(grep "<Stream" $check_file | wc -l)
stream_end_count=$(grep "</Stream" $check_file | wc -l)
if [ "$table_start_count" != "$table_end_count" ]; then echo "### ERROR ### Number of Table start tags ($table_start_count) does not equal number of end tags ($table_end_count)."; exit 7; fi
if [ "$stream_start_count" != "$stream_end_count" ]; then echo "### ERROR ### Number of Stream start tags ($stream_start_count) does not equal number of end tags ($stream_end_count)."; exit 8; fi
known_stream_count=$(grep "<Stream.*Name=\"segment_summary:table\"" $check_file | wc -l)
active_stream_count=$(grep "<Stream.*Name=\"segment:table\"" $check_file | wc -l)
if [ "$known_stream_count" -ne 1 ]; then echo "### ERROR ### Should be exactly 1 'known' segment stream ('<Stream.*Name=\"segment_summary:table\"'), but count is $known_stream_count."; exit 9; fi
if [ "$active_stream_count" -ne 1 ]; then echo "### ERROR ### Should be exactly 1 'active' segment stream ('<Stream.*Name=\"segment:table\"'), but count is $active_stream_count."; exit 10; fi
#echo "known stream count = $known_stream_count; active stream count = $active_stream_count"

#for gpstime in $(awk '{FS=","; if (NF == 10) {print $9, $3}}' $check_file | tail -n "$check_line_count" | grep -o '[0-9]\{10\}')
#awk '{FS=","; if (NF == 10) {print $9, $3}}' $check_file | tail -n "$check_line_count" | grep -o '[0-9]\{10\}'

# Find the GPS times and report on them
# this needs to be modified to swap the order of the output lines, but not right now
#for gpstime in $(grep "segment_definer:segment_def_id" "$check_file" | tail -n "$check_line_count" | grep -o '[0-9]\{10\}')
# this checks based on the number of fields, with 10 for the last (and maybe only) known segment or 11 for segments before the last one [but 10 matches active segments before the last one]
#for gpstime in $(awk '{FS=","; if ((NF == 10) || (NF == 11)) {print $9, $3}}' $check_file | tail -n "$check_line_count" | grep -o '[0-9]\{10\}')
# this checks based on the number of fields and the contents of the last field, with 10 for the last segment, which should end with '0' (ns) in the last field
#   or 11 for segments before the last one, with an empty last field, b/c there's a comma at the end of the line to separate lines in the stream (but not at the end of the last line)
for gpstime in $(awk '{FS=","; if ((NF == 10 && $NF == 0) || (NF == 11 && $NF == "")) {print $9, $3}}' $check_file | tail -n "$check_line_count" | grep -o '[0-9]\{10\}')
do
  if [ "$ltc_exit_code" -eq 0 ]
  then
    ltconvert $gpstime
  else
    echo $gpstime
  fi
done

exit 0

