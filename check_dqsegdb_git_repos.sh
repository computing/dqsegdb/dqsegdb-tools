#!/bin/bash

# This is a script to check if any of the standard dqsegdb repos are dirty (i.e., have changes) on a given machine.
#   This could be important to make sure there are no changes that haven't been pushed to the main repo.
# Started on 2023.06.20 by Robert Bruntz.
# Note that this is a very specific tool, made for just the dqsegdb repos.
# Modified on 2024.08.05 to handle /local/ldbd/git/ repos, which meant rewriting everything to be a bit more brittle, but easier to manage.
#   Better, if it's ever worth the effort: check in expected dirs on segments* machines for any repos in them, e.g., /root/git/ and /local/*/git/ - but probably only after all SL7 systems have been taken offline (b/c of their different naming scheme).
#   Note that checking a Git repo owned by another user requires a configuration change, which will probably be printed when you try to check it; for us, it was:
#     git config --global --add safe.directory /local/ldbd/git/server
#   See: https://confluence.atlassian.com/bbkb/git-command-returns-fatal-error-about-the-repository-being-owned-by-someone-else-1167744132.html
# Usage: ' (script) '
# Outline:
#   * Loop over list of known dqsegdb repos
#   * For each, check if on master/main branch; if not, report that
#   * Whatever the branch, check for changes that have not been pushed to repo
#   * Maybe check if there are other branches than master/main, and if so, report that
#   * Maybe give some commands that will help do tasks to help with above results
# To do:
#   *
# Known issues:
#   * Not sure how it will handle multiple dirs in the dir containing a repo (e.g., /root/git_dqsegdb_tools/ containing dirs dqsegdb-tools and dqsegdb-tools_2023.04.20-23.59.27.bak)
#   * If multiple branches in a repo, changes to a branch other than the open branch will not be detected (though multiple branches should alert the user to possible issues)
#   * Changes to the master/main repo since the last pull won't be detected by the diff between open branch and local copy of master/main branch on remote repo, which could produce erroneous results


# this is the list of dqsegdb repos, as found here: https://git.ligo.org/computing/dqsegdb
#repo_list="git_dqsegdb_client/*  git_dqsegdb_private/*  git_dqsegdb_puppet_repo/*  git_dqsegdb_server/*  git_dqsegdb_tools/*  git_dqsegdb_web/*  git_dqsegdb_metadata-monitors/*  git_dqsegdb_glue/*"
repo_list="git_dqsegdb_client/client  git_dqsegdb_private/dqsegdb-private  git_dqsegdb_puppet_repo/dqsegdb_puppet_repo  git_dqsegdb_server/server  git_dqsegdb_tools/dqsegdb-tools  git_dqsegdb_web/web  git_dqsegdb_metadata-monitors/monitors  git_dqsegdb_glue/glue"
# possible repo list is all of the repos that could possibly exist on the SL7 or RL8 servers; detchar accounts are handled separately
psb_repo_list="/root/git_dqsegdb_client/client  /root/git_dqsegdb_private/dqsegdb-private  /root/git_dqsegdb_puppet_repo/dqsegdb_puppet_repo  /root/git_dqsegdb_server/server  /root/git_dqsegdb_tools/dqsegdb-tools \
 /root/git_dqsegdb_web/web  /root/git_dqsegdb_metadata-monitors/monitors  /root/git_dqsegdb_glue/glue \
/root/git/client  /root/git/dqsegdb-private  /root/git/dqsegdb_puppet_repo  /root/git/server  /root/git/dqsegdb-tools  /root/git/web  /root/git/monitors  /root/git/glue  /local/ldbd/git/server"

# determine the dir that any cloned repos should be in, based on hostname
# examples: for seg: /root/git_dqsegdb_server/server/; for detchar-CIT: /home/detchar/dqsegdb/git_dqsegdb_tools/dqsegdb-tools/
#if [ `hostname | grep segments | wc --lines` -gt 0 ]; then home_dir="/root"; else home_dir="~/dqsegdb"; fi
# have to echo "~" into a variable, otherwise it's used as a literal tilde later, not the home dir
#if [ `hostname | grep segments | wc --lines` -gt 0 ]; then home_dir="/root"; else home_dir=`echo ~/dqsegdb`; fi
# if the server is a segments* server, use one list; if not, assume it's a machine running from the detchar account and use the repo that's expected there
if [ `hostname | grep segments | wc --lines` -gt 0 ]; then repo_list=$psb_repo_list; else repo_list="/home/detchar/dqsegdb/git_dqsegdb_tools/dqsegdb-tools"; fi
#if [ -d /root/git ]; then repo_list="client  dqsegdb-private  dqsegdb_puppet_repo  server  dqsegdb-tools  web  monitors  glue"; home_dir="/root/git"; fi
#echo "Using home dir = $home_dir"
echo "repo list = $repo_list"

# loop over the list of repos and run checks on the ones that are present
issue_count=0
for repo in $repo_list
do
  #if [ -d "${home_dir}/${repo}" ]; then
  if [ -d "$repo" ]; then
    #repo_dir=`ls -d ${home_dir}/${repo}/`   # assumes that dir containing repo dir has only the repo dir in it
    #if [ -d "$repo_dir" ]; then echo "### $repo_dir"; fi
    echo "### $repo"
    cd "$repo"
    # check if on master/main branch; if not, report the branch
    #if [ `git branch | grep -e main -e master | wc --lines` -eq 0 ]; then echo "# Not on master/main branch of repo; on branch `git branch`."; fi   # doesn't work - this lists all branches, even if on a branch other than master/main
    # check if more branches than just master/main; if so, list branches
    if [ `git branch | wc --lines` -gt 1 ]; then echo "# More than just 1 branch in repo:"; git branch; let issue_count++; fi
    # check if current branch has modifications that haven't been incorporated into repo
    # the line below only checks if there are *uncommitted* changes to the *current* branch
    if [ ! -z "$(git status --porcelain)" ]; then echo "# There are changes to current branch that haven't been committed!"; let issue_count++; fi
    # figure out if the repo uses master or main, then diff against the corresponding remote repo, to find committed changes that aren't in repo; edge case: changes in other branches that haven't been incorporated into master/main
    if [ "$(git branch -a | grep master | wc --lines)" -eq 0 ]; then
      if [ `git diff HEAD remotes/origin/main | wc --lines` -gt 0 ]; then echo "# There are changes to current branch that haven't been incorporated into main branch on repo!"; let issue_count++; fi
    else
      if [ `git diff HEAD remotes/origin/master | wc --lines` -gt 0 ]; then echo "# There are changes to current branch that haven't been incorporated into master branch on repo!"; let issue_count++; fi
    fi
  fi

done

echo "### Total issue count = $issue_count"
exit $issue_count
