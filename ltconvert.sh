#!/bin/bash
# This is a "lightweight tconvert" (thus, "ltconvert") that is intended to take in a GPS time, then convert it to human-readable format.  If 2 times are given, both are converted and the time between them is calculated.
# Started by Robert Bruntz on 2019.07.28.
# Notes:
# * As of Feb. 2021, the last leap-second was on 2016.12.31, 23:59:60 [sic] UTC (GPS time: 1151366416 (?)).  Any times at or before that point in time must account for leap seconds.  [ https://en.wikipedia.org/wiki/Leap_second ]
# * There have been 18 leap-seconds since GPS epoch (midnight between Jan. 5 and 6, 1980), so that is the max that a GPS-time-to-UTC-time conversion could be off by, up to 23:59:59 UTC on 2021.12.31 (GPS: 1325030417).
# * LIGO data goes back to ~1038100000 (Nov 28 2012 01:06:24 UTC).  There have been 2 leap seconds since 2012.07.01, so that is the max that a LIGO-related GPS-time-to-UTC-time conversion could be off by.
# * From 2017.01.01, 00:00:00 UTC (GPS 1167264018) onward, GPS time can be converted to Unix time as GPS = Unix - 315964782 (and thus Unix = GPS + 315964782).  Before that date and time, leap seconds must be taken into account.
# * GPS <-> UTC here: https://www.gw-openscience.org/gps/ ; GPS <-> UTC (with Unix thrown in for free) here: https://www.andrews.edu/~tzs/timeconv/timeconvert.php (and more info on leap seconds)
# * On leap seconds: GPS labels each second uniquely, including leap seconds; UTC does, as well, but uses an atypical label for leap seconds; Unix re-uses a timestamp for leap seconds:
#     (e.g., for a leap second (occurring at midnight), GPS would progress g, g+1, g+2; UTC would progress 23:59:59, 23:59:60, 00:00:00; Unix time would progress u, u, u+1; more info here: https://en.wikipedia.org/wiki/Unix_time#Leap_seconds)
# To do:
#   * allow entry of more than two GPS times; maybe: order them, then find the difference between each pair
#   * maybe allow arbitrary input and just scan for any 9- or 10-digit numbers and use those for input (check each input "word" for being a possible GPS time; if so, use it and move on; if not, find any/all possible GPS times in that word and move on)
#   * test input times, to make sure that they're both numbers (if not 'now')
#   * decide if we should allow negative GPS times
#   * account for more leap seconds (?) (use https://en.wikipedia.org/wiki/Leap_second plus https://www.andrews.edu/~tzs/timeconv/timeconvert.php )
#   * option to turn off warning messages?
#   * option to return output times in another time zone?

# set some variables
gps_time2=-1
gps_now=$((`date +%s` - 315964782))
#leap_unknown=1261872017   # this is the last time that it is *known* that there is no new leap second (23:59:59 on 31 Dec. 2019) - see https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat (= ftp://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat )
#leap_unknown=1293494417   # this is the last time that it is *known* that there is no new leap second (23:59:59 on 31 Dec. 2020) - see https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat (= ftp://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat )
#leap_unknown=1325030417   # this is the last time that it is *known* that there is no new leap second (23:59:59 on 31 Dec. 2021) - see https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat (= ftp://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat )
#leap_unknown=1356566417   # this is the last time that it is *known* that there is no new leap second (23:59:59 on 31 Dec. 2022) - see https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat (= ftp://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat )
#leap_unknown=1372204817   # this is the last time that it is *known* that there is no new leap second (23:59:59 on 30 June 2023) - see https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat (= ftp://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat )
#leap_unknown=1388102417   # this is the last time that it is *known* that there is no new leap second (23:59:59 on 31 Dec 2023) - see https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat (= ftp://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat )
leap_unknown=1419724817   # this is the last time that it is *known* that there is no new leap second (23:59:59 on 31 Dec 2024) - see https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat (= ftp://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat )
last_safe_date="30 Dec. 2024"
leap_27=1167264017   # = Dec 31 2016 23:59:60 UTC ('tconvert 1167264017')
leap_26=1119744016   # = Jun 30 2015 23:59:60 UTC ('tconvert 1119744016')
leap_25=1025136015   # = Jun 30 2012 23:59:60 UTC ('tconvert 1025136015')
leap_24=914803214    # = Dec 31 2008 23:59:60 UTC ('tconvert 914803214')
leap_23=820108813    # = Dec 31 2005 23:59:60 UTC ('tconvert 820108813')
leap_22=599184012    # = Dec 31 1998 23:59:60 UTC ('tconvert 599184012')
leap_21=551750411    # = Jun 30 1997 23:59:60 UTC ('tconvert 551750411')
leap_20=504489610    # = Dec 31 1995 23:59:60 UTC ('tconvert 504489610')
leap_safe=457056010   # = 1 july 1994 00:00:00 UTC
gps_offset=0
if [ `uname -s` == "Darwin" ]; then OS=Mac; else OS=Linux; fi   ### figure out what OS we're on

# check command line arguments and maybe print usage
if [ "$#" -eq 0 ]; then echo "### INFO ### Usage: '  (script name)  (GPS time)  [GPS time]  '.  Outputs human-readable versions of the GPS time(s); if 2 times given, also gives the duration of time between them."; gps_time1="now"; fi
if [ "$#" -ge 1 ]; then gps_time1=$1; fi
if [ "$gps_time1" == "now" ] || [ "$gps_time1" == "Now" ] || [ "$gps_time1" == "NOW" ]; then gps_time1=$gps_now; echo "### NOTICE ### GPS time 1 converted from 'now' to $gps_now"; fi
if [ `echo -n $gps_time1 | wc -c` -eq 5 ]; then gps_time1_old=$gps_time1; gps_time1=${gps_time1}00000; 
  echo "### NOTICE ### 5-digit GPS times have 5  0's appended (so $gps_time1_old was changed to $gps_time1); if you don't want this, add one or more 0's to the start of your GPS time (e.g., '0${gps_time1_old})"
fi   # this is so that a user can enter just the first 5 digits of a GPS time
if [ "$#" -ge 2 ]; then gps_time2=$2; fi
if [ "$gps_time2" == "now" ] || [ "$gps_time2" == "Now" ] || [ "$gps_time2" == "NOW" ]; then gps_time2=$gps_now; echo "### NOTICE ### GPS time 2 converted from 'now' to $gps_now"; fi
if [ `echo -n $gps_time2 | wc -c` -eq 5 ]; then gps_time2_old=$gps_time2; gps_time2=${gps_time2}00000; 
  echo "### NOTICE ### 5-digit GPS times have 5  0's appended (so $gps_time2_old was changed to $gps_time2); if you don't want this, add one or more 0's to the start of your GPS time (e.g., '0${gps_time2_old})"
fi   # this is so that a user can enter just the first 5 digits of a GPS time
if [ "$#" -ge 3 ]; then echo "### WARNING ### Only the first 2 command-line args are used ('$gps_time1' and '$gps_time2'); remaining args are ignored.  Run without any args for usage info."; fi
# sometimes it's really handy to be able to copy a time from a file that looks like "1368372909,0" (e.g., DQSegDB XML output files); this automatically strips off the ",0" part, rather than producing an error
gps1_comma_count=`echo $gps_time1 | egrep "[[:digit:]]{9},0" | wc --lines`
if [ "$gps1_comma_count" -gt 0 ]; then gps_time1=`echo $gps_time1 | cut -d',' -f 1`; fi
gps2_comma_count=`echo $gps_time2 | egrep "[[:digit:]]{9},0" | wc --lines`
if [ "$gps2_comma_count" -gt 0 ]; then gps_time2=`echo $gps_time2 | cut -d',' -f 1`; fi 

# convert GPS time(s) to UTC time(s)
for gps_time in $gps_time1 $gps_time2
do
  if [ $gps_time -eq -1 ]; then exit -1; fi
  if [ $gps_time -eq $leap_27 ]; then echo "### WARNING ### GPS time $leap_27 is a leap second; thus, it maps to 23:59:60, even though the output says 23:59:59.  (This is an issue with how Unix handles leap seconds.)"; fi
  if [ $gps_time -lt $leap_27 ]; then unix_offset=1; fi
  if [ $gps_time -eq $leap_26 ]; then echo "### WARNING ### GPS time $leap_26 is a leap second; thus, it maps to 23:59:60, even though the output says 23:59:59.  (This is an issue with how Unix handles leap seconds.)"; fi
  if [ $gps_time -lt $leap_26 ]; then unix_offset=2; fi
  if [ $gps_time -eq $leap_25 ]; then echo "### WARNING ### GPS time $leap_25 is a leap second; thus, it maps to 23:59:60, even though the output says 23:59:59.  (This is an issue with how Unix handles leap seconds.)"; fi
  if [ $gps_time -lt $leap_25 ]; then unix_offset=3; fi
  if [ $gps_time -eq $leap_24 ]; then echo "### WARNING ### GPS time $leap_24 is a leap second; thus, it maps to 23:59:60, even though the output says 23:59:59.  (This is an issue with how Unix handles leap seconds.)"; fi
  if [ $gps_time -lt $leap_24 ]; then unix_offset=4; fi
  if [ $gps_time -eq $leap_23 ]; then echo "### WARNING ### GPS time $leap_23 is a leap second; thus, it maps to 23:59:60, even though the output says 23:59:59.  (This is an issue with how Unix handles leap seconds.)"; fi
  if [ $gps_time -lt $leap_23 ]; then unix_offset=5; fi
  if [ $gps_time -eq $leap_22 ]; then echo "### WARNING ### GPS time $leap_22 is a leap second; thus, it maps to 23:59:60, even though the output says 23:59:59.  (This is an issue with how Unix handles leap seconds.)"; fi
  if [ $gps_time -lt $leap_22 ]; then unix_offset=6; fi
  if [ $gps_time -eq $leap_21 ]; then echo "### WARNING ### GPS time $leap_21 is a leap second; thus, it maps to 23:59:60, even though the output says 23:59:59.  (This is an issue with how Unix handles leap seconds.)"; fi
  if [ $gps_time -lt $leap_21 ]; then unix_offset=7; fi
  if [ $gps_time -eq $leap_20 ]; then echo "### WARNING ### GPS time $leap_20 is a leap second; thus, it maps to 23:59:60, even though the output says 23:59:59.  (This is an issue with how Unix handles leap seconds.)"; fi
  if [ $gps_time -lt $leap_20 ]; then unix_offset=8; fi
  if [ $gps_time -gt $leap_unknown ]    # the second before the next possible leap second (as of Nov. 2022)
    then echo "### WARNING ### UTC time after GPS time $leap_unknown should account for leap seconds, but this program doesn't do that yet.  The output time could be off, if there have been any leap seconds since ${last_safe_date}."; fi
  if [ $gps_time -lt $leap_safe ]    # the second after the last leap second this program doesn't account for
    then echo "### WARNING ### UTC time before GPS time $leap_safe should account for leap seconds, but this program doesn't do that yet.  The output time could be off by up to 10 seconds."; fi

  unix_time=$((gps_time + 315964782 + unix_offset))
  if [ $OS == "Mac" ]
  then
    echo "GPS time $gps_time = $(date -u -r $unix_time)"   # -u = output in UTC; -r (seconds) = "Print the date and time represented by seconds, where seconds is the number of seconds since the Epoch (00:00:00 UTC, January 1, 1970; see time(3))" ('man date')
  else
    echo "GPS time $gps_time = $(date -u -d @$unix_time)"   # -u = output in UTC; -d @(seconds) = "Convert seconds since the epoch (1970-01-01 UTC) to a date" ('man date')
  fi
done

# if 2 GPS times given, find the difference in time between them; this section is not affected by leap seconds
if [ $gps_time2 -ne -1 ]
then
  day_count=0
  hour_count=0
  minute_count=0
  sec_count=0
  #echo "### TESTING ### day, hour, min, sec counts = $day_count, $hour_count, $minute_count, $sec_count"
  if [ $gps_time1 -ge $gps_time2 ]; then time_diff=$((gps_time1 - gps_time2)); else time_diff=$((gps_time2 - gps_time1)); fi
  time_diff_tmp=$time_diff
  while [ $time_diff_tmp -ge $((24*60*60)) ]; do   # count up any days in the time difference
    day_count=$((day_count + 1))
    time_diff_tmp=$((time_diff_tmp - 24*60*60))
  done
  while [ $time_diff_tmp -ge $((60*60)) ]; do   # count up any hours in the time difference
    hour_count=$((hour_count + 1))
    time_diff_tmp=$((time_diff_tmp - 60*60))
  done
  while [ $time_diff_tmp -ge $((60)) ]; do   # count up any minutes in the time difference
    minute_count=$((minute_count + 1))
    time_diff_tmp=$((time_diff_tmp - 60))
  done
  sec_count=$time_diff_tmp
  #echo "### TESTING ### day, hour, min, sec counts = $day_count, $hour_count, $minute_count, $sec_count"
  echo "Time difference between $gps_time1 and $gps_time2 = $time_diff seconds  =  ($day_count days  +  $hour_count hours  +  $minute_count minutes  +  $sec_count seconds)."
fi

exit 0

