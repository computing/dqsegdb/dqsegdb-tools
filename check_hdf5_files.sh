#!/bin/bash
# Started by Robert Bruntz on 2024.01.26.
msg1="This is a simple script to check for bad HDF5 files, using h5dump."
msg2='Default files to check are /home/detchar/public_html/omicron/nagios/\*/nagios-latency-\*.h5'
msg3="Run like this: ' (script name)  (verbose level; default = 1)  (optional: file pattern to check, instead of default files) ' "
msg4="  Optional pattern to check can be a single file; patterns with wildcards must be in double quotes."
msg5="Example: ' /home/detchar/bin/check_hdf5_files.sh 1 ' (print each filename in default list, then the check status of the file; message will be OK or FAIL"
msg6="Example: ' /home/detchar/bin/check_hdf5_files.sh 0 ' (same, but only print output for bad files; if all OK, nothing printed) "
msg7="Example: ' /home/detchar/bin/check_hdf5_files.sh 0 \"./*.h5\" ' (check all files ending in '.h5' in current dir, but only print output for bad files; if all OK, nothing printed) "
# You can run a manual version of the check like this:
#   for file in $(ls -1 /home/detchar/public_html/omicron/nagios/*/nagios-latency-*.h5); do echo $file; h5dump $file &> /dev/null; echo $?; done
# There are 2 known-bad files in /home/detchar/dqsegdb/*h5
# to do:
#   check the age of each file before checking it and only check files older than some minimum, to avoid checking files currently being written, which might fail even though they're OK
#   [done] add option to add a pattern to the command line, then check only that and exit

verbose=1
if [ $# -eq 0 ]; then echo $msg1; echo $msg2; echo $msg3; echo $msg4; echo $msg5; echo $msg6; echo $msg7; exit 0; fi
if [ $# -gt 0 ]; then verbose=$1; fi
if [ "$verbose" == "1" ]; then echo "Start: $(date)"; fi

function check_files_in_pattern {
  # check all files that match pattern in file_pattern (global variable)
  if [ "$verbose" == "1" ]; then echo "file pattern = $file_pattern"; fi
  #echo $(ls -1 $file_pattern)
  for file in $(ls -1 $file_pattern); do
    if [ "$verbose" == "1" ]; then echo "Checking $file"; fi
    h5dump $file &> /dev/null
    exit_code=$?
    if [ "$exit_code" == "0" ]; then
      if [ "$verbose" == "1" ]; then echo "OK"; fi
    else
      echo "FAIL ( $file ); exit code = $exit_code"
    fi
  done
}

# input pattern mode - run on only the single pattern input from the command line
if [ $# -gt 1 ]; then
  file_pattern="$2"
  #echo; echo "$file_pattern"; echo; echo $(ls -1 $file_pattern)
  check_files_in_pattern
  if [ "$verbose" == "1" ]; then echo "End: $(date)"; fi
  exit 0
fi

# default mode - check files below
# expand by creating new sets of lines that set 'file_pattern' and then call 'check_files_in_pattern'
# note that a pattern can be a single file
file_pattern="/home/detchar/public_html/omicron/nagios/*/nagios-latency-*.h5"
check_files_in_pattern


if [ "$verbose" == "1" ]; then echo "End: $(date)"; fi
exit 0

#Reference:
# for file in $(ls -1 /home/detchar/public_html/omicron/nagios/*/nagios-latency-*.h5); do echo $file; h5dump $file &> /dev/null; echo $?; done
