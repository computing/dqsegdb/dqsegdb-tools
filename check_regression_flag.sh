#!/bin/bash
# A quick script to run similar commands as are run in the regression tests, but just for one flag.  Mainly for debugging regression test failures.
# Started in 2024.07.29 by Robert Bruntz
msg1="A script to check a single flag, the same way as regression tests do, to hopefully get the same results."
msg2="Usage: ' (script name)  (flag name)  (server to check; default = http://segments-backup.ldas.cit) ' "
msg3="  Note: flag name should be '[IFO_abbreviation]/[flag_name]/[flag_version]', but ':'s will be converted to '/'s; server name should include 'http://' (for *.ldas.cit) or 'https://' (for *ligo.org)"
msg4="Example: ' /root/bin/check_regression_flag.sh ' (uses 'G1/GEO-CALIB_BROKEN/1' as default flag)"
msg5="Example: ' /root/bin/check_regression_flag.sh  V1/VDQ_CAT1/1 ' "
msg6="Example: ' /root/bin/check_regression_flag.sh  H1:GRD-SQZ_OPO_OK:1  http://segments.ldas.cit ' "
# to do:
#   * [done] convert flag names of format "H1:GRD-SQZ_OPO_OK:1" to "H1/GRD-SQZ_OPO_OK/1"
#   * check flag name, that it contains 2 '/'s and no ':'s, and no 'dq' part
#   * check server name, that it has http/https part; error if not (or add it); check that http matches ldas.cit and https matches ligo.org


flag="G1/GEO-CALIB_BROKEN/1"
server=http://segments-backup.ldas.cit
log_file=/tmp/check_regression_flag.sh_log.txt
if [ $# -eq 0 ]; then echo $msg1; echo $msg2; echo $msg3; echo $msg4; echo $msg5; echo $msg6; exit 1; fi
if [ $# -gt 0 -a "$1" != "-" ]; then flag="$1"; fi
if [ $(echo "$flag" | grep "dq/" | wc -l) -gt 0 ]; then echo "### ERROR ### Flag name ($flag) should not have 'dq/' in it."; echo $msg2; echo $msg3; exit 2; fi
# convert a colon-separated flag (e.g., H1:GRD-SQZ_OPO_OK:1) into a slash-separated flag (e.g., H1/GRD-SQZ_OPO_OK/1)
flag=$(echo $flag | tr ':' '/')
if [ $# -gt 1 -a "$2" != "-" ]; then server="$2"; fi
if [ $(echo "$server" | grep "http" | wc -l) -lt 1 ]; then echo "### ERROR ### Server name ($server) should have 'http://' or 'https://' in it."; echo $msg2; echo $msg3; exit 3; fi


echo "Testing flag '$flag' on server $server .  Log file = $log_file ."

echo "# Check active flag-version coverage"
tmp_afvc_report=/tmp/report_coverage_afvc
echo "  Temp file = $tmp_afvc_report"
curl http://segments-backup.ldas.cit/report/coverage -o $tmp_afvc_report 2>> $log_file
afvc_report_count=$(grep -oE "$flag.....total_active_segments...[[:digit:]]*" $tmp_afvc_report | awk '{print $NF}')
#echo "AFVC report count = $afvc_report_count"
temp_afvc_segments=/tmp/flag_active_segments_afvc
echo "  Temp file = $temp_afvc_segments"
curl http://segments-backup.ldas.cit/dq/${flag}?include=active -o $temp_afvc_segments 2>> $log_file
afvc_segments_count=$(echo $(( (`grep -o '[0-9]\{9,10\}' $temp_afvc_segments | wc -l` - 1)/2 )))   # counts all 9-10 digit numbers in the file, subtracts 1 for the "server_timestamp" value, then divides by 2 b/c each segment has a start and end time)
if [ "$afvc_report_count" -ne "$afvc_segments_count" ]
then
  echo "Counts don't match!  $afvc_report_count != $afvc_segments_count"
else
  echo "Counts match.  $afvc_report_count == $afvc_segments_count"
fi

echo "# Check known flag-version coverage"
tmp_kfvc_report=/tmp/report_coverage_kfvc
echo "  Temp file = $tmp_kfvc_report"
curl http://segments-backup.ldas.cit/report/coverage -o $tmp_kfvc_report 2>> $log_file
kfvc_report_count=$(grep -oE "$flag.....total_active_segments...[[:digit:]]*...earliest_active_segment...[[:digit:]]*.0...latest_active_segment...[[:digit:]]*.0...total_known_segments...[[:digit:]]*" $tmp_kfvc_report | awk '{print $NF}')
#$flag.....total_active_segments...[[:digit:]]*" $tmp_afvc_report | awk '{print $NF}')
#echo "AFVC report count = $afvc_report_count"
temp_kfvc_segments=/tmp/flag_known_segments_kfvc
echo "  Temp file = $temp_kfvc_segments"
curl http://segments-backup.ldas.cit/dq/${flag}?include=known -o $temp_kfvc_segments 2>> $log_file
kfvc_segments_count=$(echo $(( (`grep -o '[0-9]\{9,10\}' $temp_kfvc_segments | wc -l` - 1)/2 )))   # counts all 9-10 digit numbers in the file, subtracts 1 for the "server_timestamp" value, then divides by 2 b/c each segment has a start and end time)
if [ "$kfvc_report_count" -ne "$kfvc_segments_count" ]
then
  echo "Counts don't match!  $kfvc_report_count != $kfvc_segments_count"
else
  echo "Counts match.  $kfvc_report_count == $kfvc_segments_count"
fi

echo "# Check flag version active segment boundaries"
tmp_fvasb_report=/tmp/report_coverage_fvasb
echo "  Temp file = $tmp_fvasb_report"
curl http://segments-backup.ldas.cit/report/coverage -o $tmp_fvasb_report 2>> $log_file
fvasb_report_count=$(grep -oE "$flag.....total_active_segments...[[:digit:]]*...earliest_active_segment...[[:digit:]]*" $tmp_fvasb_report | awk '{print $NF}')
temp_fvasb_segments=/tmp/flag_active_segments_fvasb
echo "  Temp file = $temp_fvasb_segments"
curl http://segments-backup.ldas.cit/dq/${flag}?include=active -o $temp_fvasb_segments 2>> $log_file
fvasb_segments_count=$(grep -o "\[[[:digit:]]\{9,10\}.0," $temp_fvasb_segments | grep -o "[[:digit:]]\{9,10\}" | sort -h | head -n 1)
if [ "$fvasb_report_count" -ne "$fvasb_segments_count" ]
then
  echo "Segments don't match!  $fvasb_report_count != $fvasb_segments_count"
else
  echo "Segments match.  $fvasb_report_count == $fvasb_segments_count"
fi

echo "# Check flag version known segment boundaries"
tmp_fvksb_report=/tmp/report_coverage_fvksb
echo "  Temp file = $tmp_fvksb_report"
curl http://segments-backup.ldas.cit/report/coverage -o $tmp_fvksb_report 2>> $log_file
fvksb_report_count=$(grep -oE "$flag.....total_active_segments...[[:digit:]]*...earliest_active_segment...[[:digit:]]*.0...latest_active_segment...[[:digit:]]*.0...total_known_segments...[[:digit:]]*...earliest_known_segment...[[:digit:]]*" $tmp_fvksb_report | awk '{print $NF}')
temp_fvksb_segments=/tmp/flag_known_segments_fvksb
echo "  Temp file = $temp_fvksb_segments"
curl http://segments-backup.ldas.cit/dq/${flag}?include=known -o $temp_fvksb_segments 2>> $log_file
fvksb_segments_count=$(grep -o "\[[[:digit:]]\{9,10\}.0," $temp_fvksb_segments | grep -o "[[:digit:]]\{9,10\}" | sort -h | head -n 1)
if [ "$fvksb_report_count" -ne "$fvksb_segments_count" ]
then
  echo "Segments don't match!  $fvksb_report_count != $fvksb_segments_count"
else
  echo "Segments match.  $fvksb_report_count == $fvksb_segments_count"
fi

echo "Maybe useful:"
echo "display_json.sh  (file)"
echo "python3 -m json.tool  (file)"

exit 0
