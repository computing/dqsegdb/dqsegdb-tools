#!/bin/bash
# This is a script to pretty-print JSON files, and to find any likely GPS times in the file (exactly 10 numerical digits, surrounded by anything else besides other numerical digits),
#   then print the UTC time of those GPS times and the difference between those times and the current time.
# Started by Robert Bruntz, 2022.07.31
# to do:
#   * add a check, to make sure that 'ltconvert' is installed

if [ $# -lt 1 ]; then echo "### Usage: ' (script name)  (file to view) ' "; exit 1; fi
infile=$1
if [ $# -gt 1 ]; then echo "### WARNING ### Only first argument is used.  Arguments 2+ are ignored."; fi

if [ ! -e $infile ]; then echo "### ERROR ### File '$infile' does not exist."; exit 2; fi
if [ ! -s $infile ]; then echo "### ERROR ### File '$infile' is 0 bytes."; ls -l $infile; exit 3; fi

python3 -m json.tool $infile

gps_count=`grep -o '[0-9]\{10\}' $infile | wc --lines`
if [ "$gps_count" -gt 0 ]; then
  echo "### Suspected GPS times in this file:"
#  for gpstime in `grep -o '[0-9]\{10\}' $infile`
  for gpstime in `grep -o '[0-9]\{10\}' $infile | sort -u`
  do
    echo $gpstime
    ltconvert $gpstime now
  done
fi

exit 0

