LGMM
As a workaround for KAGRA members, who are not yet supported by LGMM, we run lgmm_cron_job.sh on segments* machines line this:
00 * * * * /root/bin/lgmm_cron_job.sh &>> /var/log/lgmm/lgmm.log
That script uses /etc/lgmm/lgmm_config_kagra.py, and LGMM itself uses /etc/lgmm/lgmm_config.py
