#!/bin/bash
# 2022.06.01 - modified to add writing output to temp file, then moving that, to avoid race condition of reading new grid-mapfile while it's being written;
#                also, added error-checking code, so that failures that produce obviously-bad LIGO or KAGRA grid-mapfiles don't propagate into the actual grid-mapfile.

gmf=/etc/grid-security/grid-mapfile
ggmf=/etc/grid-security/grid-mapfile.ligo
kgmf=/etc/grid-security/grid-mapfile.kagra
kconf=/etc/lgmm/lgmm_config_kagra.py
tmpmf=/tmp/grid-mapfile

/usr/sbin/lgmm -f -o $ggmf
/usr/sbin/lgmm -f -c $kconf -o $kgmf

#cat $ggmf $kgmf > $gmf
cat $ggmf $kgmf > $tmpmf

if [ `cat $ggmf | wc -l` -gt 1000 ] && [ `cat $kgmf | wc -l` -gt 20 ] && [ `cat $tmpmf | wc -l` -gt 1000 ]
then
  /bin/mv $tmpmf $gmf
else
  echo "### ERROR ### Size of an intermediate grid-mapfile is too small."
  echo "          ### LIGO file is `cat $ggmf | wc -l` lines; should be > 1000."
  echo "          ### KAGRA file is `cat $kgmf | wc -l` lines; should be > 20."
  echo "          ### Combined file (temp file) is `cat $tmpmf | wc -l` lines; should be > 1000."
  exit 1
fi

exit 0
