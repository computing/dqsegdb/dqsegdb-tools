#!/bin/bash
# This is a script to check the free space on /backup/segdb/ and /ifocache/ and report the results in a JSON file, to be read by Nagios/Icinga/Dashboard.
# Started by Robert Bruntz on 2020.04.20
# Usage: ' (script)  (backup_warning_pcent)  (backup_critical_pcent)  (ifocache_warning_pcent)  (ifocache_critical_pcent)  (verbose (default=0))  (ifocache_critical_pcent (default=1)) '
# Example: ' /root/bin/check_backup_and_ifocache_space.sh  90  95  90  95  0  1 '  (runs with all default values)
# Example: ' /root/bin/check_backup_and_ifocache_space.sh  -  -  -  -  1  0 '  (keep default percentage values; print verbose output; don't write out a JSON file for Nagios/Icinga/Dashboard)
# to do:
#   * check that /backup/segdb/ and /ifocache/ are mounted before checking size; if not, return critical error
#   * [done] output message: include used %, used space, total space (human-readable)
#   * separate output message into critica/warning/OK parts (to clarify which one or ones is/are in each (and maybe include percentage thresholds in output?)


# set variable default values
backup_warning_pcent=90
backup_critical_pcent=95
ifocache_warning_pcent=90
ifocache_critical_pcent=95
verbose=0
human_time=$(date)
gps_run_time=$((`date +%s` - 315964782))   # this is the GPS time for the run of this monitor
monitor_dir=/var/www/nagios
output_file=$monitor_dir/nagios_backup_and_ifocache_space.json
write_output_file=1
service_cadence=$((60*60))

# values (maybe) set by command-line arguments
if [ "$#" -gt 0 ] && [ "$1" != "-" ]; then backup_warning_pcent=$1; fi
if [ "$#" -gt 1 ] && [ "$2" != "-" ]; then backup_critical_pcent=$2; fi
if [ "$#" -gt 2 ] && [ "$3" != "-" ]; then ifocache_warning_pcent=$3; fi
if [ "$#" -gt 3 ] && [ "$4" != "-" ]; then ifocache_critical_pcent=$4; fi
if [ "$#" -gt 4 ]; then if [ "$5" -eq 1 ]; then verbose=1; fi; fi   # if the cmd-line arg is there and is 1, set verbose to 1; if no arg or any other value, keep verbose at 0
if [ "$#" -gt 5 ]; then if [ "$6" -eq 0 ]; then write_output_file=0; fi; fi   # if the cmd-line arg is there and is 0, set to 0; if no arg or any other value, keep at 1
#if [ "$#" -ge 2 ]; then output_file=$2; fi
#if [ "$#" -ge 2 ]; then monitor_dir=$2; fi
# print some setup info
if [ $verbose -eq 1 ]; then echo "### INFO ### Run time: $(date) - GPS time: ~$((`date +%s` - 315964782))"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Verbose = $verbose"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### /backup/segdb/ warning at ${backup_warning_pcent}%; /backup/segdb/ critical at ${backup_critical_pcent}%; /ifocach/ warning at ${ifocache_warning_pcent}%; /ifocache/ critical at ${ifocache_critical_pcent}%"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Output file = $output_file"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Write to output file = $write_output_file"; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Results from 'df -h /backup/segdb/ /ifocache/':"; df -h /backup/segdb/ /ifocache/; echo "###"; fi


# check used size percentages
backup_pcent=`df /backup/segdb/ --output=pcent | tail -n 1 | cut -c1-3`
backup_size=`df /backup/segdb/ --output=size -h | tail -n 1`
backup_used=`df /backup/segdb/ --output=used -h | tail -n 1`
ifocache_pcent=`df /ifocache/ --output=pcent | tail -n 1 | cut -c1-3`
ifocache_size=`df /ifocache/ --output=size -h | tail -n 1`
ifocache_used=`df /ifocache/ --output=used -h | tail -n 1`

# Analyze results and write an output file
# we start with a default result of 'OK', then change it if there is a more-serious status anywhere
monitor_status="OK"; monitor_status_code=0
if [ "$backup_pcent" -gt "$backup_warning_pcent" ] || [ "$ifocache_pcent" -gt "$ifocache_warning_pcent" ]; then
  monitor_status="Warning";    monitor_status_code=1;
fi
if [ "$backup_pcent" -gt "$backup_critical_pcent" ] || [ "$ifocache_pcent" -gt "$ifocache_critical_pcent" ]; then
  monitor_status="Critial";    monitor_status_code=2;
fi
if [ $verbose -eq 1 ]; then echo "/backup/segdb/ usage = ${backup_pcent}%  ($backup_used out of $backup_size) ($monitor_status)"; fi
if [ $verbose -eq 1 ]; then echo "/ifocache/     usage = ${ifocache_pcent}%  ($ifocache_used out of $ifocache_size) ($monitor_status)"; fi
main_message="/backup/segdb/ usage is ${backup_pcent}% ($backup_used out of $backup_size); /ifocache/ usage is ${ifocache_pcent}% ($ifocache_used out of $ifocache_size) at GPS time $gps_run_time ($human_time)."
unknown_message="UNKNOWN: No information on space usage of /backup/segdb/ and /ifocache/ after GPS time $gps_run_time ($human_time).\
  (Last values: ${backup_pcent}% ($backup_used out of $backup_size) and ${ifocache_pcent}% ($ifocache_used out of $ifocache_size), respectively.)"

# create the message for the JSON file
output_message="{\"created_gps\": $gps_run_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"${monitor_status}: ${main_message}\", \"num_status\": $monitor_status_code}, \
{\"start_sec\": $((service_cadence + 10)), \"txt_status\": \"${unknown_message}\", \"num_status\": 3}]}"
if [ $verbose -eq 1 ]; then echo "### INFO ### Output message:"; echo $output_message; fi

if [ $write_output_file -eq 1 ]; then
  echo $output_message > $output_file
  if [ $verbose -eq 1 ]; then
    echo "### INFO ### Output file: $output_file"
    echo "### INFO ### Contents of output file:"
    python3 -m json.tool < $output_file
  fi
else
  if [ $verbose -eq 1 ]; then
#    python3 -m json.tool < $output_message   # doesn't work: "line 79: $output_message: ambiguous redirect"
#    python3 -m json.tool < `echo $output_message`   # doesn't work: "line 80: `echo $output_message`: ambiguous redirect"
#    echo $output_message > python3 -m json.tool   # doesn't work - saves output into a file named 'python3' in the pwd
    echo "### INFO ### Reformatted output:"
    echo $output_message | python3 -m json.tool   # this works
  fi
fi

exit 0

### Reference
# typical 'OK' status file:
# {"created_gps": 1246226959, "status_intervals": [{"start_sec": 0, "txt_status": "OK: Service 'httpd' is running on host 'segments-dev2' at GPS time 1246226959 (Wed Jul 3 15:09:01 PDT 2019).", "num_status": 0}, \
{"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if service 'httpd' is running on host 'segments-dev2' after GPS time 1246226959 (Wed Jul 3 15:09:01 PDT 2019).", "num_status": 3}]}
# typical 'Warning' status file:
# {"created_gps": 1246219917, "status_intervals": [{"start_sec": 0, "txt_status": "WARNING: Service 'shib' was not found on host 'segments-dev2' at GPS time 1246219917 (Wed Jul 3 13:11:39 PDT 2019). This might be a configuration issue.", \
"num_status": 1}, {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if service 'shib' is running on host 'segments-dev2' after GPS time 1246219917 (Wed Jul 3 13:11:39 PDT 2019).", "num_status": 3}]}
# typical 'Critical' status file:
# {"created_gps": 1246219862, "status_intervals": [{"start_sec": 0, "txt_status": "CRITICAL: Service 'shibd' is NOT running on host 'segments-dev2' at GPS time 1246219862 (Wed Jul 3 13:10:44 PDT 2019).", "num_status": 2}, \
# {"start_sec": 600, "txt_status": "UNKNOWN: Cannot verify if service 'shibd' is running on host 'segments-dev2' after GPS time 1246219862 (Wed Jul 3 13:10:44 PDT 2019).", "num_status": 3}]}




