#!/bin/bash

# Script started by Robert Bruntz on 2021.10.25.
# This is a script to create backups of crontab contents.
# It defaults to the current user and a related dir (/home/${user}/crontab_backup/), but either/both can be specified.  Script scans for existing crontab files; if none exists, it copies the current crontab to the output dir;
#   if one or more files exist, it compares the newest file to the current crontab; if they match, nothing is done; if they don't match, the current crontab is saved as a new file in the output dir.
#   Crontab filenames must match a specific format ({output dir}/crontab_{user}_YYYY.MM.DD-HH.MM.SS) to be detected by the script.
# Note that if a user has a home dir that is shared across different machines, they should probably create and specify different output dirs for each machine (e.g., /home/detchar/crontab_backup_detchar-LLO/ vs. /home/detchar/crontab_backup_LLO-grid/, etc.);
#   otherwise, each machine might see the other machine's latest backup, notice that it's different from its own crontab, and save a new copy, even if neither machine's crontab has changed.  Sorting which crontab goes with which machine could be problematic, too.
# Note that only user root can view crontab files for user other than self (or maybe a user with sudoer permission).
#
# Usage: ' (script)  [user]  [output dir]  [verbose level] '   # use a dash (-) to use default values without entering anything  # verbose level default = 1 for testing, 0 for running as a cron job
# Example: '/home/detchar/dqsegdb/crontab_backup.sh '   # will use current user and use dir /home/{user}/crontab_backup/ (or notify if that doesn't exist)
# Example: '/home/detchar/dqsegdb/crontab_backup.sh  detchar  /home/detchar/crontab_backup '   # if not user detchar, probably need to be root to run this
# Example: '/home/detchar/dqsegdb/crontab_backup.sh  -  /home/detchar/crontab_backup_detchar-LHO '   # useful for user detchar on a shared filesystem - have one dir for detchar-LHO and another for LHO-grid
# Example: '/root/bin/crontab_backup.sh  detchar  /root/detchar_crontab_backup '   # (untested)

# Original outline (actual script is somewhat modified from this, but close-ish):
#  get user
#  if not root, check if /home/{user}/crontab_backup/ exists
#    if not, report error and exit
#  if root, check if /root/crontab_backup/ exists
#    if not, report error and exit
#  maybe verify that user can read crontab and/or that it exists?
#  echo 'crontab -l' to temp file, with name including formatted date and time
#  list crontab files in backup dir, s.t. newest one would be last; get name of newest one
#  if no newest one matching formatting, copy temp file to backup dir
#  if newest one exists, compare to temp file
#    if same, do nothing
#    if different, copy temp file to backup dir
#  maybe add some override options, such as pick user (probably only useful to root) and pick backup dir

# To do:
#   * [done] write it all, test it
#   * [done] deal with shared filesystem (e.g., different machines that have different crontabs for the same user, so would constantly be copying new versions of crontab after each other)?
#       no - users should pick a specific output dir to match the machine they're backing up (e.g., /home/detchar/crontab_backup_detchar-LLO/ vs. /home/detchar/crontab_backup_LLO-grid/, etc.)
#   * add a test for non-root user wanting to run for user other than themselves (won't work - "must be privileged to use -u")
#   * add an early test for user not being able to write to output dir (that exists)
#   * improve usage documentation printed for --help, etc.
#   * 


verbose=1   # change this to 0 when we're out of testing stage
temp_dir=/tmp/crontab

# check if user wants help
if [ $# -gt 0 ]; then
  if [ "$1" == "--help" ] || [ "$1" == "-help" ] || [ "$1" == "help" ] || [ "$1" == "-h" ]; then
    echo "Help and usage examples given in comments at the start of the script ($0).  Maybe try \" grep -e '^# Usage' -e '^# Example' $0 \" - or just cat the file."
    exit 0
  fi
fi

# set up user ID
user=`id -un`
if [ $# -gt 0 ] && [ "$1" != "-" ]; then user=$1; fi

# set up and check output dir
if [ "$user" == "root" ]
then
  outdir=/root/crontab_backup
else
  outdir=/home/${user}/crontab_backup
fi
if [ $# -gt 1 ] && [ "$2" != "-" ]; then outdir=$2; fi

# maybe change verbose level
if [ $# -gt 2 ]; then verbose=$3; fi

# create/select temp dir
# try to use the temp dir set near the start; if we can't create that, use /tmp/ instead
mkdir -p $temp_dir
if [ ! -d $temp_dir ]; then temp_dir=/tmp; fi

# print values being used
if [ "$verbose" -ne 0 ]; then echo "user = $user ; output directory = $outdir ; temporary directory = $temp_dir ; verbose level = $verbose"; fi

# check if output dir exists
if [ ! -d "$outdir" ]
then
  echo "### ERRROR ### Output dir '$outdir' does not exist.  Please create it or change the dir to be used."
  echo "           ### Usage:  ' (script)  [user]  [output dir]  [verbose level] '   # use a dash (-) to use default for [optional] arguments"
  exit 1
fi

# check if crontab exists and/or is readable
tmp_file=${temp_dir}/crontab_${user}_$(date +%Y.%m.%d-%H.%M.%S)
if [ "$user" != `id -un` ]; then
  crontab -l -u $user &> $tmp_file
  exit_code1=$?
else
  crontab -l > /dev/null &> $tmp_file
  exit_code1=$?
fi
#if [ "$verbose" -ne 0 ]; then echo "tmp_file: $tmp_file"; cat $tmp_file; fi
if [ "$verbose" -ne 0 ]; then echo "tmp_file: $tmp_file"; ls -l $tmp_file; wc $tmp_file; fi

# list crontab files in backup dir, s.t. newest one would be last; get name of newest one
last_file=`ls -1 $outdir/crontab_${user}_????.??.??-??.??.?? 2> /dev/null  |  tail -n 1`

# if no newest file matching formatting, copy temp file to backup dir
if [ "$last_file" == "" ]
then
  cp -p $tmp_file $outdir
  exit_code4=$?
  if [ "$exit_code4" -eq 0 ]
  then
    if [ "$verbose" -ne 0 ]; then echo "No files in $outdir yet.  Copied $tmp_file to $outdir."; fi
    rm $tmp_file
  else
    echo "### ERROR ### Not able to copy new crontab file $tmp_file to output dir $outdir !"
    echo "          ### Note that file $tmp_file should still be available."
    exit 2
  fi
# if newest one exists, compare to temp file
else   # do this if there *is* a recognized last file in outdir
  if [ "$verbose" -ne 0 ]; then echo "Last file in $outdir : $last_file"; fi
  diff $tmp_file $last_file &> /dev/null
  exit_code2=$?   # 0 -> files are identical; 1 -> files differ; 2 -> at least one file doesn't exist
# if same, do nothing
  if [ "$exit_code2" -eq 0 ]
  then
    if [ "$verbose" -ne 0 ]; then echo "Crontab for $user is unchanged.  Nothing done."; fi
    rm $tmp_file
  fi
# if different, copy temp file to backup dir
  if [ "$exit_code2" -eq 1 ]
  then
    cp -p $tmp_file $outdir
    exit_code3=$?
    if [ "$exit_code3" -ne 0 ]
    then
      echo "### ERROR ### Not able to copy new crontab file $tmp_file to output dir $outdir !"
      echo "          ### Note that file $tmp_file should still be available."
      exit 3
    else
     if [ "$verbose" -ne 0 ]; then echo "File $tmp_file copied to $outdir"; fi
     rm $tmp_file
    fi
  fi
  if [ "$exit_code2" -eq 2 ]; then echo "### ERROR ### Couldn't find at least one of these files:  $tmp_file $last_file "; exit 4; fi
  if [ "$exit_code2" -gt 2 ]; then echo "### ERROR ### Exit code from 'diff $tmp_file $last_file &> /dev/null' was $exit_code2.  Not sure what that means, so nothing done."; exit 5; fi
fi


# reference
# files should look like this: /home/detchar/crontab_backup/crontab_detchar_2021.10.25-16.09.20
# or: /home/${user}/crontab_backup/crontab_${user}_????.??.??-??.??.??
