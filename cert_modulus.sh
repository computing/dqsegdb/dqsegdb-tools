#!/bin/bash
# This is just a quick script to check the name and date on an input cert; the idea is that the user doesn't have to remember all of the details of the openssl command, and this program can be run from a script.
# Useful: create an option in this script that searches for all ".cert", ".key", and ".pem" files on a system, excludes files in certain directories (including /usr/share/doc/), and runs the openssl command on everything else.
#   Directories to exclude: /usr/share/doc/; /etc/grid-security/certificates/
# Try these:  (note that ".cer" will match both ".cer" files and ".cert" files (as well as files like ".cert.pem", which will also match the ".pem" version)
#   for fname in `locate .cer | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" `; do check_cert.sh $fname; done
#   for fname in `locate .crt | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" `; do check_cert.sh $fname; done
#   for fname in `locate .key | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" `; do check_cert.sh $fname; done
#   for fname in `locate .pem | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" `; do check_cert.sh $fname; done
# combined:
#   for fname in `locate .cer .crt .key .pem | grep -v "/usr/share/doc/" | grep -v "/etc/grid-security/certificates" | grep -v "ca-bundle" `; do check_cert.sh $fname; done



cert_file=$1
echo "Cert file = $cert_file"
openssl x509 -modulus -noout -in  $cert_file  &> /dev/null
if [ "$?" -eq 0 ]
then
  echo "### INFO ### x509 modulus:"
  openssl x509 -modulus -noout -in  $cert_file
else
  openssl rsa -modulus -noout -in  $cert_file  &>  /dev/null
  if [ "$?" -eq 0 ]
  then
  echo "### INFO ### rsa modulus:"
    openssl rsa -modulus -noout -in  $cert_file
  else
    echo "### INFO ### Could not calculate the x509 or rsa modulus for '$cert_file'."
  fi
fi

exit


