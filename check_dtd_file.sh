#!/bin/bash
# A quick script to check if the ligolw_dtd.txt is available from the link it should be available from.
# For each URL that might have the file, attempts to download the file; if successful, checks its md5sum against the known-good value.
# This script takes advantage of Bash's default use of global variables
# run like this: ' (script name)  (verbose level; default = 1 = verbose; 0 = only print errors) '
# example: ' /root/git_dqsegdb_tools/dqsegdb-tools/check_dtd_file.sh  1 '
# Note that file3 and file4 (ldas-cit URL) are never available, because the server was taken down (Oct. 2023),
#   so those tests are commented out (but sometimes used for testing).
# to do:
#   * add: if a given URL fails, try pinging the host, to see if it's up, and report the results
#   * turn the file, file_out, and file_error parts into a loop, rather than listing them explicitly
#   * start monitoring the backup files: https://ldas-jobs.ligo.caltech.edu/~detchar/dqsegdb/ligolw_dtd.txt , https://ldas-jobs.ligo-wa.caltech.edu/~detchar/dqsegdb/ligolw_dtd.txt , and https://ldas-jobs.ligo-la.caltech.edu/~detchar/dqsegdb/ligolw_dtd.txt 
#   * 

verbose=1
final_exit_code=0
if [ $# -gt 0 ]; then verbose=$1; fi
run_time=$(date +%Y.%m.%d-%H.%M.%S)
file1=http://ldas-sw.ligo.caltech.edu/doc/ligolwAPI/html/ligolw_dtd.txt
file2=https://ldas-sw.ligo.caltech.edu/doc/ligolwAPI/html/ligolw_dtd.txt
file3=http://ldas-cit.ligo.caltech.edu/doc/ligolwAPI/html/ligolw_dtd.txt
file4=https://ldas-cit.ligo.caltech.edu/doc/ligolwAPI/html/ligolw_dtd.txt
file1_out=/tmp/dtd_file1_${run_time}.txt
file2_out=/tmp/dtd_file2_${run_time}.txt
file3_out=/tmp/dtd_file3_${run_time}.txt
file4_out=/tmp/dtd_file4_${run_time}.txt
file1_error=/tmp/dtd_file1_error_${run_time}.txt
file2_error=/tmp/dtd_file2_error_${run_time}.txt
file3_error=/tmp/dtd_file3_error_${run_time}.txt
file4_error=/tmp/dtd_file4_error_${run_time}.txt
dtd_file_md5sum=e327739756ed7eb2707d790f07917057
#dtd_file_md5sum=42   # for testing
if [ "$verbose" -eq 1 ]; then echo "Run start time: $run_time"; fi

get_file () {
# a function to download the desired file
  if [ "$verbose" -eq 1 ]; then echo "-"; echo "Running for $file0 ; output file: $file0_out ; error file: $file0_error "; fi
  wget $file0 -O $file0_out  2>> $file0_error
  exit_code0=$?
  if [ "$exit_code0" -eq 0 ]
  then
    if [ "$verbose" -eq 1 ]; then echo "OK: downloaded file $file0"; fi
  else
    #if [ "$exit_code0" -eq 4 ]; then echo "FAIL: could not download $file; wget reported 'no route to host'"; fi
    echo "FAIL: could not download $file0"
    echo "exit code = $exit_code0"
    echo "output from wget:"
    echo "[quote]"
    cat $file0_error
    echo "[/quote]"
    final_exit_code=$((final_exit_code + 1))
    url0=`echo $file0 | cut -d'/' -f 3`   # cut into fields using delimiter '/'; print the 3rd field (1-based count)
    ping -c 1 $url0 &> /dev/null
    exit_code00=$?
    if [ "$exit_code00" -eq 0 ]; then echo "Server is up, but it isn't serving the file (based on ' ping -c 1 $url0 ')."; fi
    if [ "$exit_code00" -eq 1 ]; then echo "Server is probably not up (based on ' ping -c 1 $url0 ')."; fi
    if [ "$exit_code00" -gt 1 ]; then echo "Server name is probably not recognized (based on ' ping -c 1 $url0 ')."; fi
  fi
}

check_file () {
# a file to check the file that was downloaded
  file0_md5sum=`md5sum $file0_out | awk '{print $1}'`
  if [ "$file0_md5sum" == "$dtd_file_md5sum" ]
  then
    if [ "$verbose" -eq 1 ]; then echo "OK: md5sum for file from $file0 matches reference md5sum"; fi
  else
    echo "FAIL: md5sum for file from $file0 does NOT matches reference md5sum ($dtd_file_md5sum)"
    echo "md5sum for $file0 = $file0_md5sum"
    echo "file info:"
    ls -l $file0_out
    final_exit_code=$((final_exit_code + 1))
  fi
}

run_all () {
# a function to run everything that is appropriate
  get_file
  if [ "$exit_code0" -eq 0 ]
  then
    check_file
  fi
}

# run everything for each URL
file0=$file1; file0_out=$file1_out; file0_error=$file1_error
run_all
file0=$file2; file0_out=$file2_out; file0_error=$file2_error
run_all
#file0=$file3; file0_out=$file3_out; file0_error=$file3_error
#run_all
#file0=$file4; file0_out=$file4_out; file0_error=$file4_error
#run_all

# print end time and exit
end_time=$(date +%Y.%m.%d-%H.%M.%S)
if [ "$verbose" -eq 1 ]; then echo "-"; echo "Run end time: $end_time"; echo "Final exit code: $final_exit_code"; fi
exit $final_exit_code

# reference
# wget exit codes
#   4 = no route to host
