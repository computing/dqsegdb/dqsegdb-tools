#!/bin/bash
# Just a quick script to make it easier to do queries.  Started by Robert Bruntz on 2020.06.08.
# To do:
#   * [done] add option to add 'known', 'active', 'both' to the end of the command, to print out known and/or active segments automatically (to finish it: save output to xml file, then cat those contents and run through the filter once or twice (for both/all))
#   * add option to use https://${server}.ligo.org, rather than http://${server}.ldas.cit
#   * robustify some input - e.g., handle "H" vs. "H1" vs. "LHO", etc.; handle "https://segments.ligo.org" vs "segments"; check input, so that typos aren't run (e.g., 'segmnets')
#   * figure out why printing msg1 et al. doesn't preserve multiple spaces (thus necessitating the multiple echo lines to print a message if no args are given

msg1="Usage: ' (script name)  (segments machine)  (IFO)  (flag name)  (flag version)  (GPS start time)  (GPS end time)  (optional: print 'known' or 'active' or 'both' segments or 'raw' xml output)'; enter a dash (-) anywhere to use default value"
msg2="Example: ' /home/detchar/dqsegdb/query_segments.sh  segments  H1  DMT-ANALYSIS_READY  1  1234500000  1234600000  both ' (these are the default values)"
msg3="Maybe useful for manual commands: append \" | ligolw_print -t segment_summary -c start_time -c end_time -d ' ' \" to print known segments; append \" | ligolw_print -t segment -c start_time -c end_time -d ' ' \" to print active segments"
msg4="Maybe useful for manual commands: append \" | grep segment_def_id:0 | grep segment_summary \" to print known segment lines; append \" | grep segment:segment_id: \" to print active segment lines"
# default values
server=segments
ifo_in=H1
flag=DMT-ANALYSIS_READY
flag_version=1
gps_start=1234500000
gps_end=1234600000
print_type=both
start_time=$(date +%Y.%m.%d-%H.%M.%S) 
output_filename=/tmp/ligolw_query_${start_time}

if [ $# -eq 0 ]; then
  echo "Usage: ' (script name)  (segments machine)  (IFO)  (flag name)  (flag version)  (GPS start time)  (GPS end time)  (optional: print 'known' or 'active' or 'both' segments or 'raw' xml output)'; enter a dash (-) anywhere to use default value"
  echo "Example: ' /home/detchar/dqsegdb/query_segments.sh  segments  H1  DMT-ANALYSIS_READY  1  1234500000  1234600000  both ' (these are the default values)"
  echo "Maybe useful: science-mode flags: H1/L1:DCS-ANALYSIS_READY_C01:1, H1/L1:DMT-ANALYSIS_READY:1, V1:ITF_SCIENCE:1 or :2, G1:GEO-UP:1, K1:GRD-LSC_LOCK_STATE_N_EQ_1000:1 ( https://wiki.ligo.org/Main/O3ScienceModeFlags )"
  echo "Maybe useful for manual commands: append \" | ligolw_print -t segment_summary -c start_time -c end_time -d ' ' \" to print known segments; append \" | ligolw_print -t segment -c start_time -c end_time -d ' ' \" to print active segments"
  echo "Maybe useful for manual commands: append \" | grep segment_def_id:0 | grep segment_summary \" to print known segment lines; append \" | grep segment:segment_id: \" to print active segment lines"
  exit
fi

if [ $# -gt 0 ] && [ "$1" != "-" ]; then server=$1; fi
if [ $# -gt 1 ] && [ "$2" != "-" ]; then ifo_in=$2; fi
if [ $# -gt 2 ] && [ "$3" != "-" ]; then flag=$3; fi
if [ $# -gt 3 ] && [ "$4" != "-" ]; then flag_version=$4; fi
if [ $# -gt 4 ] && [ "$5" != "-" ]; then gps_start=$5; fi
if [ $# -gt 5 ] && [ "$6" != "-" ]; then gps_end=$6; fi
if [ $# -gt 6 ] && [ "$7" != "-" ]; then print_type=$7; fi
if [ "$print_type" == "all" ]; then print_type="both"; fi   # b/c I keep forgetting which it is

# set ifo
# 'ifo' is set to "X" at the start and only changed for a recognized uppercase version of an IFO name
ifo=X
ifo_upper=${ifo_in^^}   ### convert to all uppercase (bash 4+)
if [ "$ifo_upper" == "H" ]  || [ "$ifo_upper" == "H1" ]  || [ "$ifo_upper" == "LHO" ]; then ifo="H1"; fi
if [ "$ifo_upper" == "L" ]  || [ "$ifo_upper" == "L1" ]  || [ "$ifo_upper" == "LLO" ]; then ifo="L1"; fi
if [ "$ifo_upper" == "V" ]  || [ "$ifo_upper" == "V1" ]  || [ "$ifo_upper" == "VGO" ]  || [ "$ifo_upper" == "VIRGO" ]; then ifo="V1"; fi
if [ "$ifo_upper" == "K" ]  || [ "$ifo_upper" == "K1" ]  || [ "$ifo_upper" == "KAG" ]  || [ "$ifo_upper" == "KAGRA" ]; then ifo="K1"; fi
if [ "$ifo_upper" == "G" ]  || [ "$ifo_upper" == "G1" ]  || [ "$ifo_upper" == "GEO" ]; then ifo="G1"; fi
if [ "$ifo" == "X" ]; then echo "### ERROR ### IFO name was not recognized.  Recognized values: (H,L,V,K,G,H1,L1,V1,K1,G1,LHO,LLO,VGO,KAG,GEO,Virgo,Kagra)"; exit; fi

#time ligolw_segment_query_dqsegdb --show-types --gps-start-time=1275000000 --gps-end-time=1276000000 --segment-url https://segments.ligo.org
# --query-segments --include-segments L1:DMT-ETMX_ESD_DAC_OVERFLOW:1 --gps-start-time 1234500000  --gps-end-time 1234600000
query_cmd="/usr/bin/time -f %E -o /tmp/time_${start_time}.txt -- ligolw_segment_query_dqsegdb  --query-segments  --segment-url https://${server}.ligo.org  --include-segments ${ifo}:${flag}:${flag_version} \
 --gps-start-time $gps_start  --gps-end-time $gps_end  > $output_filename"

#echo "### INFO ### Executing: ' time ligolw_segment_query_dqsegdb  --query-segments  --segment-url http://${server}.ldas.cit  --include-segments ${ifo}:${flag}:${flag_version}  --gps-start-time $gps_start  --gps-end-time $gps_end  > $output_filename  ; # then filters' "
echo "### INFO ### Executing: ' $query_cmd ; # then filters' "
#/usr/bin/time -f %E -o /tmp/time_${start_time}.txt -- ligolw_segment_query_dqsegdb  --debug  --query-segments  --segment-url http://${server}.ldas.cit  --include-segments ${ifo}:${flag}:${flag_version}  --gps-start-time $gps_start  --gps-end-time $gps_end  > $output_filename
#/usr/bin/time -f %E -o /tmp/time_${start_time}.txt -- ligolw_segment_query_dqsegdb  --query-segments  --segment-url http://${server}.ldas.cit  --include-segments ${ifo}:${flag}:${flag_version}  --gps-start-time $gps_start  --gps-end-time $gps_end  > $output_filename
#/usr/bin/time -f %E -o /tmp/time_${start_time}.txt -- ligolw_segment_query_dqsegdb  --query-segments  --segment-url https://${server}.ligo.org  --include-segments ${ifo}:${flag}:${flag_version}  --gps-start-time $gps_start  --gps-end-time $gps_end  > $output_filename
eval $query_cmd

echo "### Query run time:  `cat /tmp/time_${start_time}.txt`"
known_count=`grep segment_def_id:0 $output_filename | grep segment_summary | wc --lines`
if [ "$print_type" == "raw" ]; then echo "### xml output file:"; cat $output_filename; fi
if [ "$print_type" == "known" ]; then echo "### known segments:"; cat $output_filename | ligolw_print  -t segment_summary -c start_time -c end_time -d ' ' | head -n $known_count; fi
if [ "$print_type" == "active" ]; then echo "### active segments:"; cat $output_filename | ligolw_print  -t segment -c start_time -c end_time -d ' '; fi
if [ "$print_type" == "both" ]; then echo "### known segments:"; cat $output_filename | ligolw_print  -t segment_summary -c start_time -c end_time -d ' ' | head -n $known_count;
                                       echo "### active segments:"; cat $output_filename | ligolw_print  -t segment -c start_time -c end_time -d ' '; fi

exit 0
